/*
 * levenshtein.h
 *
 * See the README file for copyright information
 *
 */


#ifndef __LEVENSHTEIN_H
#define __LEVENSHTEIN_H

#include "lib/common.h"

using namespace std;

//***************************************************************************
// LV Distance
//***************************************************************************

int lvDistance(const string source, const string target, int maxPer, int& maxDist);

#endif //  __LEVENSHTEIN_H
