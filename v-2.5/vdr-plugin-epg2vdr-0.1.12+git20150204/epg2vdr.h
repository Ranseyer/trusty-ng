/*
 * epg2vdr.h: EPG2VDR plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#ifndef __EPG2VDR_H
#define __EPG2VDR_H

#include <vdr/plugin.h>
#include "lib/config.h"

//***************************************************************************
// Constants
//***************************************************************************

static const char* DESCRIPTION   = trNOOP("epg2vdr plugin");
static const char* MAINMENUENTRY = tr("epg handler");

static const char* VERSION      = "0.1.12";
static const char* VERSION_DATE = "03.02.2015";
#define DB_API  4

//***************************************************************************
// cPluginEPG2VDR
//***************************************************************************

class cPluginEPG2VDR : public cPlugin 
{
   public:

      cPluginEPG2VDR(void);
      virtual ~cPluginEPG2VDR();
      virtual const char* Version(void)          { return VERSION; }
      virtual const char* VersionDate(void)      { return VERSION_DATE; }
      virtual const char* Description(void)      { return tr(DESCRIPTION); }
      virtual const char* CommandLineHelp(void);
      virtual const char** SVDRPHelpPages(void);
      virtual cString SVDRPCommand(const char* Cmd, const char* Option, int& ReplyCode);
      virtual bool Initialize(void);
      virtual bool Start(void);
      virtual cString Active(void);
      virtual const char* MainMenuEntry(void) 
      { return EPG2VDRConfig.mainmenuVisible ? MAINMENUENTRY : 0; }
      virtual cOsdObject* MainMenuAction(void);
      virtual cMenuSetupPage* SetupMenu(void);
      virtual bool SetupParse(const char* Name, const char* Value);
      virtual void Stop();
      virtual void DisplayMessage(const char* s);
      virtual time_t WakeupTime(void);
   
   private:

      // ...
};

//***************************************************************************
#endif // EPG2VDR_H
