#!/bin/sh
#
#   guess-helper
#   Copyright (C) 2004 Tobias Grimm <tg@e-tobi.net>
# 
#   $Id: guess-helper 925 2010-01-24 11:40:38Z tiber $
# 

# prepare to use gettext, if installed:
if [ -x /usr/bin/gettext ]
then
  export TEXTDOMAIN=guess-helper
  echo="gettext -s"
else
  echo="echo"
fi

# clear console window and print header line
ClearScreen ()
{
  clear
  echo
  echo guess-helper
  echo ------------
  echo
}

# AskQuestion "Do you like Debian?" Y n  > Do you like Debian? [Y/n]
AskQuestion ()
{
  if [ -x /usr/bin/gettext ]
  then
    question=`gettext "$1"`
  else
    question=$1    
  fi

  shift

  answers=""
  for answer in $*
  do
    if [ -x /usr/bin/gettext ]
    then
      answer=`gettext "$answer"`
    fi
    answers="$answers $answer"
  done

  choices=""  
  for answer in $answers
  do
    if [ "$choices" = "" ]
    then
      choices="[$answer"
    else
      choices="$choices/$answer"
    fi
  done
  choices="$choices]"

  answers=`$echo $answers | tr '[:lower:]' '[:upper:]'`
  
  result=0
  
  while [ $result -eq 0 ]
  do
    echo -n "$question" "$choices "
    read input
      
    if [ "$input" = "" ]
    then
      result=1
    else
      input=`echo $input | tr '[:lower:]' '[:upper:]'`
      i=1
      for answer in $answers
      do
        if [ "$answer" = "$input" ]
	then
	  result=$i
	fi
	i=`expr $i + 1`
      done
    fi
  done

  return $result
}

# Translate and print text lines
EchoTextLines ()
{
  textLines=""

  while [ "$1" ]
  do
    textLines="$textLines$1"
    shift
  done

  $echo -ne "$textLines"
}

# show next wakeup time settings and ask for reboot
ShowBiosSettings ()
{
  ClearScreen
  EchoTextLines \
    "On the next reboot, please enter the BIOS settings and change\n" \
    "the wakeup time settings to the following values:\n"
  echo
  $echo -n "Day       = " ; echo "$1"
  $echo -n "Hour      = " ; echo "$2"
  $echo -n "Minute    = " ; echo "$3"
  $echo -n "Second    = " ; echo "$4"
  if [ $5 -eq 1 ]
  then
    $echo "Enabled   = Yes"
  else
    $echo "Enabled   = No"
  fi
  echo
  AskQuestion "Restart or Abort?" "A" "r" 
  if [ $? -eq 2 ]
  then
    echo 
    EchoTextLines \
      "After changing the wakeup time in the BIOS settings and rebooting the PC,\n"\
      "please run this script again (PC will reboot in 10 seconds...)\n"
    sleep 10
    shutdown -r now
    exit 0
  else
    echo 
    $echo "Script aborted !!!"
    echo
    exit 0
  fi
}

ReadNvram ()
{
  cat_nvram $catOptions > ~/guess-directisa/$1    2>~/guess-directisa/cat_nvram.log
  [ -c /dev/nvram ] && \
  cat /dev/nvram        > ~/guess-nvram-module/$1 2>~/guess-nvram-module/cat.log
  if [ ! -s ~/guess-directisa/$1 ] && [ ! -s  ~/guess-nvram-module/$1 ] ; then
    EchoTextLines\
      "cat_nvram and cat /dev/nvram failed.\n"
      "Please check the cat_nvram.log for the details, fix the errors and run\n"
      "guess-helper again.\n"
    # if they exist, they are empty. Remove them.
    rm -fr ~/guess-directisa/$1 ~/guess-nvram-module/$1
    exit 1  
  fi 
}

step0file=guess-helper.conf
step1file=31.23.59.59+
step2file=11.12.13.14+
step3file=01.00.00.00+
step4file=01.00.00.00-

# check for needed binaries
if ! (which cat_nvram >/dev/null && \
      which cat       >/dev/null && \
      which guess     >/dev/null    ) ; then
  EchoTextLines \
    "Please check that the tools cat_nvram, cat and guess are installed.\n\n"\
    "guess-helper aborted!\n"
  exit 1
fi

if [ ! -f ~/guess-directisa/$step0file ]
then
  ClearScreen
  EchoTextLines \
    "The aim of this script is to help you figuring out the addresses of the\n"\
    "wakeup time in the NVRAM of your PC. You will have to do this only,\n"\
    "if your mainboard couldn't be detected by nvram-wakeup.\n"\
    "\n"\
    "In several steps it will be necessary to reboot the PC, change the\n"\
    "wakeup time in the BIOS and then run guess-helper again.\n"\
    "\n"\
    "This script will create several temporary files and a config file for\n"\
    "nvram-wakeup. This will be done in your home directory.\n"
  echo
  AskQuestion "Continue with the first step of the nvram-wakeup address detection?" "N" "y"
  [ $? -eq 1 ] && exit
  ClearScreen
  mkdir -p ~/guess-directisa
  EchoTextLines \
     "Please select the chipset of your Board -\n"\
     "  [U] Unknown (i.e, disable access to upper nvram);\n"\
     "  [I] Intel;\n"\
     "  [V] VIA VT82Cxxx (xxx=686A,686B,596,...), nVidia nForce2, ATI RADEON 9100 IGP; \n"\
     "  [3] VIA VT8233/35/37;\n"\
     "  [D] DS1685\n"
     
  AskQuestion "Select one of the above:" "U" "i" "v" "3" "d"
  case $? in
    2) 
      catOptions="INTEL"
      ;;
    3) 
      catOptions="VT82Cxxx"
      ;;
    4) 
      catOptions="VT8235_37"
      ;;
    5) 
      catOptions="DS1685"
      ;;
  esac
  echo catOptions=\"$catOptions\" >~/guess-directisa/$step0file
  ShowBiosSettings 31 23 59 59 1 
else
  . ~/guess-directisa/$step0file
  if [ ! -f ~/guess-directisa/$step1file ]
  then
    [ -c /dev/nvram ] && mkdir -p ~/guess-nvram-module
    ReadNvram $step1file
    ShowBiosSettings 11 12 13 14 1
  else    
    if [ ! -f ~/guess-directisa/$step2file ]
    then
      ReadNvram $step2file
      ShowBiosSettings 01 00 00 00 1
    else    
      if [ ! -f ~/guess-directisa/$step3file ]
      then
        ReadNvram $step3file
        ShowBiosSettings 01 00 00 00 0
      else
        if [ ! -f ~/guess-directisa/$step4file ]
        then
          ReadNvram $step4file
        fi
	
        ClearScreen
	
	cd ~/guess-directisa
	guess > nvram-wakeup.conf 2> guess-error.log
	[ "$catOptions" = "" ] || echo upper_method = $catOptions >> nvram-wakeup.conf
	if [ -d ~/guess-nvram-module ] ; then
	    cd ~/guess-nvram-module
	    guess > nvram-wakeup.conf 2> guess-error.log
	    cd ..
	fi
	
        EchoTextLines \
          "The nvram-wakeup address detection is finished now.\n"\
	  "If everything went well, you will find the detected nvram-wakeup\n"\
	  "settings in the file ~/guess-directisa/nvram-wakeup.conf. This file\n"\
	  "has been generated using the 'directisa' access method.\n" 
	[ -c /dev/nvram ] && EchoTextLines \
	  "In the directory ~/guess-nvram-module you will find the results using\n"\
	  "the 'nvram-kernel-module' access method. It's up to you to decide,\n"\
	  "what access method worked.\n"
	EchoTextLines \
	  "Check the files nvram-wakeup.conf and guess-error.log to see if\n"\
	  "everything went well. In order to use the generated conf file, you\n"\
	  "have to pass it as a command line parameter with the option -C to\n"\
	  "nvram-wakeup. Before doing this, you should copy it to /etc.\n"\
          "If the settings in your nvram-wakup.conf seem to work, please submit\n"\
	  "this file to the mainboard tracker:\n"
        echo
        echo "https://sourceforge.net/tracker/?group_id=35022&atid=412959"
        echo
        $echo "By doing so, you are helping to keep nvram-wakeup support more boards."
      fi
    fi
  fi
fi
