
#include <stdio.h>
#include <errno.h>
#include <signal.h>

#include "epgd.h"

const char* confDir = (char*)confDirDefault;
const char* logPrefix = LOG_PREFIX;

void showUsage()
{
   printf("Usage: epgd [-n][-c <config-dir>][-l <log-level>][-t]\n");
   printf("    -v              show version and exit\n");
   printf("    -n              don't daemonize\n");
   printf("    -t              log to stdout\n");
   printf("    -c <config-dir> use config in <config-dir>\n");
   printf("    -p <plugin-dir> load plugins from <plugin-dir>\n");
   printf("    -l <log-level>  set log level\n");
}

//***************************************************************************
// Main
//***************************************************************************

int main(int argc, char** argv)
{
   cEpgd* job;
   int nofork = no;
   int pid;
   int logstdout = na;
   int loglevel = na;

   // Usage ..

   if (argc > 1 && (argv[1][0] == '?' || (strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0)))
   {
      showUsage();
      return 0;
   }

   // Parse command line

   for (int i = 0; argv[i]; i++)
   {
      if (argv[i][0] != '-' || strlen(argv[i]) != 2)
         continue;

      switch (argv[i][1])
      {
         case 'v': printf("epgd version %s from %s\n", VERSION, VERSION_DATE); return 0;
         case 't': logstdout = yes;                           break;
         case 'n': nofork = yes;                              break;
         case 'l': if (argv[i+1]) loglevel = atoi(argv[++i]); break;
         case 'c': if (argv[i+1]) confDir = argv[++i];        break;
         case 'M': EPG2VDRConfig.maintanance = yes;           break;
         case 'p': if (argv[i+1])
         {
            sstrcpy(EPG2VDRConfig.pluginPath, argv[++i], 
                    sizeof(EPG2VDRConfig.pluginPath)); 
            break;
         }

         default: 
         {
            showUsage(); 
            return 0;
         }
      }
   }

   if (logstdout != na) EPG2VDRConfig.logstdout = logstdout;
   if (loglevel != na)  EPG2VDRConfig.loglevel = loglevel;

   job = new cEpgd();

   if (job->init() != success)
   {
      delete job;
      return 1;
   }

   // fork daemon

   if (!nofork)
   {
      if ((pid = fork()) < 0)
      {
         printf("Can't fork daemon, %s\n", strerror(errno));
         return 1;
      }
      
      if (pid != 0)
         return 0;
   }

   // register SIGINT

   ::signal(SIGINT, cEpgd::downF);
   ::signal(SIGTERM, cEpgd::downF);
   ::signal(SIGHUP, cEpgd::triggerF);

   // do work ...

   job->loop();

   // shutdown

   tell(0, "normal exit");

   delete job;

   return 0;
}
