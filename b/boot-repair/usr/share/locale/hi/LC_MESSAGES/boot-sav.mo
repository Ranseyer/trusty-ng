��          �      <      �     �     �  )   �       '   1  @   Y  g   �       .        D     d  1   �  !   �  &   �  "   �          >  �  \  w      Z   x  ~   �  +   R  �   ~  �   '  -  �  6   
  �   U
  <   �
  S     �   i  o   �  �   o  e   �  K   Y  X   �                                                    	                                  
               ${SYSTEM3} is now without GRUB. Boot successfully repaired. Close this window when you have finished. Create a BootInfo summary GRUB reinstallation has been cancelled. In case you still experience boot problem, indicate this URL to: Please use the file browser that just opened to delete unused files (or transfer them to another disk). Recommended repair Remove any operating system from your computer Repair the boot of the computer This can prevent to start it Which operating system do you want to uninstall ? You can now reboot your computer. e.g. you may get a Power Manager error or to your favorite support forum. repairs most frequent problems to get help by email or forum Project-Id-Version: boot-repair
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-12-08 01:03+0100
PO-Revision-Date: 2012-11-29 21:09+0000
Last-Translator: YannUbuntu <yannubuntu@gmail.com>
Language-Team: Hindi <hi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-12-23 14:41+0000
X-Generator: Launchpad (build 16877)
 ${SYSTEM3} अब भव्य एकीकृत आरंभ स्थापना (GRUB) के बिना है. आरंभ सफलतापूर्वक मरम्मत हो गया है. जब आपका कार्य समाप्त हो जाए, इस विंडो को बंद करें. BootInfo सारांश बनाएँ भव्य एकीकृत आरंभ स्थापना (GRUB) को पुनर्स्थापना रद्द कर दिया गया है. मामले में आप अभी भी बूट समस्या का अनुभव करने के लिए, इस URL के लिए संकेत मिलता है: कृपया अप्रयुक्त फ़ाइलों को हटाने के लिए फ़ाइल ब्राउज़र का उपयोग करें (या उन्हें किसी अन्य डिस्क को हस्तांतरण करें). की सिफारिश की मरम्मत अपने कंप्यूटर से किसी भी ऑपरेटिंग सिस्टम निकालें कंप्यूटर की बूट मरम्मत इसे शुरू करने में यह रोक सकता है. आप कौन सा ऑपरेटिंग सिस्टम की स्थापना रद्द करना चाहते हैं? अब आप अपने कंप्यूटर फिर से आरंभ कर सकते हैं उदाहरणार्थ आपको बिजली प्रबंधक त्रुटि मिल सकती है. अपने पसंदीदा मंच का समर्थन करने के लिए. मरम्मत सबसे लगातार समस्याओं ईमेल या मंच द्वारा मदद पाने के लिए 