/*
 *   NVRAM WakeUp
 *   Copyright (C) 2001-2002, Sergei Haller.
 *
 *   $Id: time.c 727 2004-06-26 07:21:23Z bistr-o-math $
 *
 *   Contributed by Dr. Werner Fink <werner@suse.de>
 *   Added some changes/speedups by Dr. Werner Fink and me.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#define CVSREV_time_c \
     "$Id: time.c 727 2004-06-26 07:21:23Z bistr-o-math $"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(int argc, char **argv) {
     time_t         vdr_abs_time_t;

     if (argc != 2) {
          printf("Usage: %s <time>\nwhere <time> is number of seconds since epoch\n",
                  argv[0]);
          return 1;  
     }

     vdr_abs_time_t = strtol(argv[1], NULL, 10);

     printf("(time_t)     %d\n", (int)vdr_abs_time_t);
     printf("(local time) %s", asctime(localtime(&vdr_abs_time_t)));
     printf("(utc/gmt)    %s", asctime(   gmtime(&vdr_abs_time_t)));
     return 0;
}
