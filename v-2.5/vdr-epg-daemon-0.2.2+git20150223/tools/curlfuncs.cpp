/*
Copyright (c) 2002, Mayukh Bose
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

* Neither the name of Mayukh Bose nor the names of other
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
  Change History:
  11/23/2004 - Removed the #include <unistd.h> line because I didn't 
               need it. Wonder why I had it there in the first place :).
  10/20/2004 - Publicly released this code. 
*/
#include <string>
#include <cstring>
#include <cstdio>
#include <curl/curl.h>
#include <curl/easy.h>
#include "curlfuncs.h"

#ifndef TRUE
#define TRUE 1
#endif

#define EPGD_USERAGENT "libcurl-agent/1.0"

using namespace std;

// Local function prototypes
int CurlDoPost(const char *url, string *sOutput, const string &sReferer,
	       struct curl_httppost *formpost, struct curl_slist *headerlist);

namespace curlfuncs {
   string sBuf;
   bool bInitialized = false;
   CURL *curl = NULL;
}

size_t collect_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  string sTmp;
  register size_t actualsize = size * nmemb;
  if ((FILE *)stream == NULL) {
    sTmp.assign((char *)ptr, actualsize);
    curlfuncs::sBuf += sTmp;
  }
  else {
    size_t xxx = fwrite(ptr, size, nmemb, (FILE *)stream);
  }
  return actualsize;
}

inline void InitCurlLibraryIfNeeded() 
{
  if (!curlfuncs::bInitialized) {
    curl_global_init(CURL_GLOBAL_ALL);
    curlfuncs::curl = curl_easy_init();
    if (!curlfuncs::curl)
      throw string("Could not create new curl instance");
    curl_easy_setopt(curlfuncs::curl, CURLOPT_NOPROGRESS, 1);       // Do not show progress
    curl_easy_setopt(curlfuncs::curl, CURLOPT_WRITEFUNCTION, collect_data);
    curl_easy_setopt(curlfuncs::curl, CURLOPT_WRITEDATA, 0);       // Set option to write to string
    curl_easy_setopt(curlfuncs::curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_easy_setopt(curlfuncs::curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; Mayukh's libcurl wrapper http://www.mayukhbose.com/)");
    curlfuncs::bInitialized = true;
  }
}

void FreeCurlLibrary(void)
{
   if (!curlfuncs::bInitialized)
      return ;

   if (curlfuncs::curl)
      curl_easy_cleanup(curlfuncs::curl);
   
   curl_global_cleanup();
   
   curlfuncs::bInitialized = false;
}

int CurlGetUrl(const char *url, string *sOutput, const string &sReferer) 
{
  InitCurlLibraryIfNeeded();
  CURLcode res;

  curl_easy_setopt(curlfuncs::curl, CURLOPT_URL, url);            // Set the URL to get
  if (sReferer != "")
    curl_easy_setopt(curlfuncs::curl, CURLOPT_REFERER, sReferer.c_str());
  curl_easy_setopt(curlfuncs::curl, CURLOPT_HTTPGET, TRUE);
  curl_easy_setopt(curlfuncs::curl, CURLOPT_FAILONERROR, TRUE);
  curl_easy_setopt(curlfuncs::curl, CURLOPT_WRITEDATA, 0);       // Set option to write to string
  curlfuncs::sBuf = "";
  res = curl_easy_perform(curlfuncs::curl);
  if (res != CURLE_OK) {
    *sOutput = "";
    return 0;
  }
  *sOutput = curlfuncs::sBuf;
  return 1;
  /*
  if (curl_easy_perform(curlfuncs::curl) == 0)
    *sOutput = curlfuncs::sBuf;
  else {
    // We have an error here mate!
    *sOutput = "";
    return 0;
  }
  return 1;
  */
}

int CurlGetUrlFile(const char *url, const char *filename, const string &sReferer)
{
  int nRet = 0;
  InitCurlLibraryIfNeeded();
  
  // Point the output to a file
  FILE *fp;
  if ((fp = fopen(filename, "w")) == NULL)
    return 0;

  curl_easy_setopt(curlfuncs::curl, CURLOPT_WRITEDATA, fp);       // Set option to write to file
  curl_easy_setopt(curlfuncs::curl, CURLOPT_URL, url);            // Set the URL to get
  if (sReferer != "")
    curl_easy_setopt(curlfuncs::curl, CURLOPT_REFERER, sReferer.c_str());
  curl_easy_setopt(curlfuncs::curl, CURLOPT_HTTPGET, TRUE);
  if (curl_easy_perform(curlfuncs::curl) == 0)
    nRet = 1;
  else
    nRet = 0;

  curl_easy_setopt(curlfuncs::curl, CURLOPT_WRITEDATA, NULL);     // Set option back to default (string)
  fclose(fp);
  return nRet;
}

int CurlPostUrl(const char *url, const string &sPost, string *sOutput, const string &sReferer)
{
  InitCurlLibraryIfNeeded();

  int retval = 1;
  string::size_type nStart = 0, nEnd, nPos;
  string sTmp, sName, sValue;
  struct curl_httppost *formpost=NULL;
  struct curl_httppost *lastptr=NULL;
  struct curl_slist *headerlist=NULL;

  // Add the POST variables here
  while ((nEnd = sPost.find("##", nStart)) != string::npos) {
    sTmp = sPost.substr(nStart, nEnd - nStart);
    if ((nPos = sTmp.find("=")) == string::npos)
      return 0;
    sName = sTmp.substr(0, nPos);
    sValue = sTmp.substr(nPos+1);
    curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, sName.c_str(), CURLFORM_COPYCONTENTS, sValue.c_str(), CURLFORM_END);
    nStart = nEnd + 2;
  }
  sTmp = sPost.substr(nStart);
  if ((nPos = sTmp.find("=")) == string::npos)
    return 0;
  sName = sTmp.substr(0, nPos);
  sValue = sTmp.substr(nPos+1);
  curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, sName.c_str(), CURLFORM_COPYCONTENTS, sValue.c_str(), CURLFORM_END);

  retval = CurlDoPost(url, sOutput, sReferer, formpost, headerlist);

  curl_formfree(formpost);
  curl_slist_free_all(headerlist);
  return retval;
}

int CurlPostRaw(const char *url, const string &sPost, string *sOutput, const string &sReferer)
{
  InitCurlLibraryIfNeeded();

  int retval;
  struct curl_httppost *formpost=NULL;
  struct curl_slist *headerlist=NULL;

  curl_easy_setopt(curlfuncs::curl, CURLOPT_POSTFIELDS, sPost.c_str());
  curl_easy_setopt(curlfuncs::curl, CURLOPT_POSTFIELDSIZE, 0); //FIXME: Should this be the size instead, in case this is binary string?

  retval = CurlDoPost(url, sOutput, sReferer, formpost, headerlist);

  curl_formfree(formpost);
  curl_slist_free_all(headerlist);
  return retval;  
}

int CurlDoPost(const char *url, string *sOutput, const string &sReferer,
		struct curl_httppost *formpost, struct curl_slist *headerlist) 
{
  headerlist = curl_slist_append(headerlist, "Expect:");

  // Now do the form post
  curl_easy_setopt(curlfuncs::curl, CURLOPT_URL, url);
  if (sReferer != "")
    curl_easy_setopt(curlfuncs::curl, CURLOPT_REFERER, sReferer.c_str());
  curl_easy_setopt(curlfuncs::curl, CURLOPT_HTTPPOST, formpost);

  curl_easy_setopt(curlfuncs::curl, CURLOPT_WRITEDATA, 0); // Set option to write to string
  curlfuncs::sBuf = "";
  if (curl_easy_perform(curlfuncs::curl) == 0) {
    *sOutput = curlfuncs::sBuf;
    return 1;
  }
  else {
    // We have an error here mate!
    *sOutput = "";
    return 0;
  }
}

int CurlSetCookieFile(char *filename)
{
  InitCurlLibraryIfNeeded();
  if (curl_easy_setopt(curlfuncs::curl, CURLOPT_COOKIEFILE, filename) != 0)
    return 0;
  if (curl_easy_setopt(curlfuncs::curl, CURLOPT_COOKIEJAR, filename) != 0)
    return 0;
  return 1;
}

char *CurlEscape(const char *url) {
    InitCurlLibraryIfNeeded();
    return curl_escape(url , strlen(url));
}

//***************************************************************************
// Callbacks
//***************************************************************************

size_t CurlWriteMemoryCallback(void* ptr, size_t size, size_t nmemb, void* data)
{
   size_t realsize = size * nmemb;
   struct MemoryStruct* mem = (struct MemoryStruct*)data;
   
   if (mem->memory)
      mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
   else
      mem->memory = (char*)malloc(mem->size + realsize + 1);
   
   if (mem->memory)
   {
      memcpy (&(mem->memory[mem->size]), ptr, realsize);
      mem->size += realsize;
      mem->memory[mem->size] = 0;
   }

   return realsize;
}

size_t CurlWriteHeaderCallback(void* ptr, size_t size, size_t nmemb, void* data)
{
   size_t realsize = size * nmemb;
   struct MemoryStruct* mem = (struct MemoryStruct*)data;
   char* p;

   if (ptr)
   {
      // get filename
      {
         // Content-Disposition: attachment; filename="20140103_20140103_de_qy.zip"

         const char* attribute = "Content-disposition: ";
         
         if ((p = strcasestr((char*)ptr, attribute)))
         {
            if (p = strcasestr(p, "filename="))
            {
               p += strlen("filename=");

               tell(4, "found filename at [%s]", p);

               if (*p == '"')
                  p++;

               sprintf(mem->name, "%s", p);
               
               if ((p = strchr(mem->name, '\n')))
                  *p = 0;
               
               if ((p = strchr(mem->name, '\r')))
                  *p = 0;
               
               if ((p = strchr(mem->name, '"')))
                  *p = 0;

               tell(4, "set name to '%s'", mem->name);
            }
         }
      }
      
      // since some sources update "ETag" an "Last-Modified:" without changing the contents
      //  we have to use "Content-Length:" to check for updates :(
      {
         const char* attribute = "Content-Length: ";
         
         if ((p = strcasestr((char*)ptr, attribute)))
         {
            sprintf(mem->tag, "%s", p+strlen(attribute));
            
            if ((p = strchr(mem->tag, '\n')))
               *p = 0;
            
            if ((p = strchr(mem->tag, '\r')))
               *p = 0;
            
            if ((p = strchr(mem->tag, '"')))
               *p = 0;
         }
      }
   }

   return realsize;
}


int CurlGetUrlMem(const char* url, int& size, MemoryStruct* data, int timeout) {
   CURL* curl_handle;
   long code;
   CURLcode res = CURLE_OK;

   size = 0;

   // init curl

   if (curl_global_init(CURL_GLOBAL_ALL) != 0)
   {
      tell(0, "Error, something went wrong with curl_global_init()");
      
      return fail;
   }
   
   curl_handle = curl_easy_init();
   
   if (!curl_handle)
   {
      tell(0, "Error, unable to get handle from curl_easy_init()");
      
      return fail;
   }
  
   if (EPG2VDRConfig.useproxy)
   {
      curl_easy_setopt(curl_handle, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
      curl_easy_setopt(curl_handle, CURLOPT_PROXY, EPG2VDRConfig.httpproxy);   // Specify HTTP proxy
   }

   curl_easy_setopt(curl_handle, CURLOPT_URL, url);                            // Specify URL to get   
   curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 0);                   // don't follow redirects
   curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, CurlWriteMemoryCallback);  // Send all data to this function
   curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void*)data);              // Pass our 'data' struct to the callback function
   curl_easy_setopt(curl_handle, CURLOPT_HEADERFUNCTION, CurlWriteHeaderCallback); // Send header to this function
   curl_easy_setopt(curl_handle, CURLOPT_WRITEHEADER, (void*)data);            // Pass some header details to this struct
   curl_easy_setopt(curl_handle, CURLOPT_MAXFILESIZE, 100*1024*1024);          // Set maximum file size to get (bytes)
   curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1);                       // No progress meter
   curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);                         // No signaling
   curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, timeout);                    // Set timeout
   curl_easy_setopt(curl_handle, CURLOPT_NOBODY, data->headerOnly ? 1 : 0);    // 
   curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, EPGD_USERAGENT);           // Some servers don't like requests 
                                                                               // that are made without a user-agent field
   // perform http-get

   if ((res = curl_easy_perform(curl_handle)) != 0)
   {
      data->clear();

      tell(1, "Error, download failed; %s (%d)",
           curl_easy_strerror(res), res);
      curl_easy_cleanup(curl_handle);  // Cleanup curl stuff

      return fail;
   }

   curl_easy_getinfo(curl_handle, CURLINFO_HTTP_CODE, &code); 
   curl_easy_cleanup(curl_handle);     // cleanup curl stuff

   if (code == 404)
   {
      data->clear();
      return fail;
   }

   size = data->size;

   return success;
}
