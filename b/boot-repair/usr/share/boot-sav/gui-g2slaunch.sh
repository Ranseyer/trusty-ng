#! /bin/bash
# Copyright 2013 Yann MRN
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.


determine_g2s() {
G2S=""
if [[ "$(ls /usr/share/boot-sav | grep 'x-')" ]];then
	if [[ "$(type -p glade2script)" ]];then
		G2S=glade2script
	elif hash glade2script-gtk2;then
		G2S=glade2script-gtk2
	fi
fi
}


g2slaunch() {
local G2S PACK PACKNEW PACKOLD vvv ppa PPADEB removdeb
# Ask root privileges
if [[ $EUID -ne 0 ]];then
	if hash gksudo;then
		gksudo $APPNAME  #gksu and su dont work in Kubuntu
	elif hash gksu;then
		gksu $APPNAME  #TODO PolicyKit http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=492493
	elif hash sudo && [ "$(grep -E '(boot=casper)|(boot=live)' /proc/cmdline)" ];then
		sudo $APPNAME
	elif hash su;then
		su -c $APPNAME
	else
		echo "Please install gksu or su"
	fi
	exit
fi
# Launch the Glade window via glade2script
determine_g2s
if [[ "$G2S" ]];then
	$G2S $1 -g ./$PACK_NAME.glade -s ./$APPNAME.sh \
	--combobox="@@_combobox_format_partition@@col" \
	--combobox="@@_combobox_bootflag@@col" \
	--combobox="@@_combobox_ostoboot_bydefault@@col" \
	--combobox="@@_combobox_purge_grub@@col" \
	--combobox="@@_combobox_separateboot@@col" \
	--combobox="@@_combobox_efi@@col" \
	--combobox="@@_combobox_sepusr@@col" \
	--combobox="@@_combobox_place_grub@@col" \
	--combobox="@@_combobox_add_kernel_option@@col" \
	--combobox="@@_combobox_restore_mbrof@@col" \
	--combobox="@@_combobox_partition_booted_bymbr@@col"
fi
}

echo_g2sversion() {
determine_g2s
G2S_VERSION=$($PACKVERSION $G2S )
echo "$G2S version : $G2S_VERSION"
echo "boot-sav-extra version : $($PACKVERSION boot-sav-extra )"
}

echoversion_or_g2slaunch() {
PACK_NAME=boot-sav
cd /usr/share/$PACK_NAME
if [[ "$1" = "-v" ]];then
	. bs-init.sh
	echo_version
	echo_g2sversion
	check_if_live_session
	check_efi_dmesg
else
	g2slaunch $1
fi
exit
}

########################## CHECK IF LIVE-SESSION #######################
check_if_live_session() {
local DR ra=/home/usr
hash lsb_release && DISTRIB_DESCRIPTION="$(lsb_release -ds)" || DISTRIB_DESCRIPTION=Unknown-name
DR="$(df / | grep /dev/ )"; DR="${DR%% *}"; DR="${DR#*v/}"
if [ "$(grep -E '(boot=casper)|(boot=live)' /proc/cmdline)" ] || [[ "$DR" =~ loop ]];then #http://paste.ubuntu.com/949845
	LIVESESSION=live
else 
	LIVESESSION=installed
	CURRENTSESSIONNAME="$The_system_now_in_use - $DISTRIB_DESCRIPTION"
	CURRENTSESSIONPARTITION="$DR"
	if [[ "$TMP_FOLDER_TO_BE_CLEARED" ]];then
		#Add CurrentSession at the beginning of OSPROBER (so that GRUB reinstall of CurrentSession is selected by default)
		echo "/dev/${CURRENTSESSIONPARTITION}:$CURRENTSESSIONNAME CurrentSession:linux" >$TMP_FOLDER_TO_BE_CLEARED/osprober_with_currentsession
		echo "$OSPROBER" >> $TMP_FOLDER_TO_BE_CLEARED/osprober_with_currentsession
		OSPROBER=$(< $TMP_FOLDER_TO_BE_CLEARED/osprober_with_currentsession)
	fi
fi
[[ -d /usr/share/ubuntu-defaults-french ]] && echo "$APPNAME est exécuté en session $LIVESESSION ($DISTRIB_DESCRIPTION, $(lsb_release -cs), $(lsb_release -is)-fr, $(uname -m))" \
|| echo "$APPNAME is executed in $LIVESESSION-session ($DISTRIB_DESCRIPTION, $(lsb_release -cs), $(lsb_release -is), $(uname -m))"
[ "$(grep -E '(boot=casper)|(boot=live)' /proc/cmdline)" ] && [[ "$(ls $ra/.config)" =~ os ]] && OSBKP=y
LANGUAGE=C LC_ALL=C lscpu | grep bit
cat /proc/cmdline
}

################################### CHECK EFI DMSG #####################
check_efi_dmesg() {
#http://forum.ubuntu-fr.org/viewtopic.php?id=742721
local ue="$(dmesg | grep EFI | grep -v Variables )"
SECUREBOOT='maybe enabled'
if [[ -d /sys/firmware/efi ]];then #http://paste.ubuntu.com/1176988
	EFIDMESG="BIOS is EFI-compatible, and is setup in EFI-mode for this $LIVESESSION-session."
	[[ "$(lsb_release -cs | grep -v squeeze | grep -v oneiric | grep -v precise | grep -v quantal | grep -v raring | grep -v saucy | grep -v nadia)" ]] \
	|| [[ "$(uname -m)" != x86_64 ]] || [[ -d /usr/share/ubuntu-defaults-french ]] \
	&& EFIDMESG="Unusual EFI: $PLEASECONTACT
$EFIDMESG"
	[[ ! "$ue" ]] && EFIDMESG="No EFI in dmseg. $PLEASECONTACT
$EFIDMESG"
	#SecureBoot http://launchpadlibrarian.net/119223180/ubiquity_2.12.8_2.12.9.diff.gz
	local efi_vars sb_var
	efi_vars=/sys/firmware/efi/vars
	sb_var="$efi_vars/SecureBoot-8be4df61-93ca-11d2-aa0d-00e098032b8c/data"
	sb_var2="$efi_vars/SecureBoot-a8be4df61-93ca-11d2-aa0d-00e098032b8c/data"
	if [[ ! -d $efi_vars ]];then
		SECUREBOOT=disabled
	elif [ -e "$sb_var" ];then
		[ "$(printf %x \'"$(cat "$sb_var")")" = 1 ] && SECUREBOOT=enabled || SECUREBOOT=disabled
	elif [ -e "$sb_var2" ];then #http://paste.ubuntu.com/1643471
		[ "$(printf %x \'"$(cat "$sb_var2")")" = 1 ] && SECUREBOOT=enabled || SECUREBOOT=disabled
	else
		[[ -f "$sb_var" ]] || [[ -f "$sb_var2" ]] && echo "Warning: sbvar. $PLEASECONTACT"
		tst="$(ls $efi_vars | grep SecureBoot )"
		if [[ "$tst" ]];then
			if [ -e "$efi_vars/$tst/data" ];then
				[ "$(printf %x \'"$(cat "$efi_vars/$tst/data")")" = 1 ] && SECUREBOOT=enabled || SECUREBOOT=disabled
			elif [[ -f "$efi_vars/$tst/data" ]];then
				echo "Warning: $tst/data . $PLEASECONTACT"
			fi
		fi
		a=""; for b in $(ls $efi_vars);do a="$b,$a";done
		echo "ls $efi_vars : $a
$PLEASECONTACT" #eg http://paste.ubuntu.com/1398454 , http://paste.ubuntu.com/1460548
	fi
	if [[ "$SECUREBOOT" = 'maybe enabled' ]] && [[ "$(grep signed /proc/cmdline)" ]];then
		SECUREBOOT=enabled
		a=""; for b in $(ls $efi_vars);do a="$b,$a";done
		echo "ls $efi_vars : $a
Special SecureBoot. $PLEASECONTACT"
	fi
	if [[ "$(type -p efibootmgr)" ]];then
		echo "
efibootmgr -v"
		LANGUAGE=C LC_ALL=C efibootmgr -v
	fi
else
	[[ "$LIVESESSION" = installed ]] && SECUREBOOT=disabled
	if [[ "$EFIFILEPRESENCE" ]];then
		EFIDMESG="BIOS is EFI-compatible, but it is not setup in EFI-mode for this $LIVESESSION-session."
		#ex of efi win with no efi dmsg: http://paste.ubuntu.com/1079434 , http://paste.ubuntu.com/1088771
	elif [[ "$(uname -m)" != x86_64 ]] || [[ "$(lsb_release -is)" = Debian ]] || [[ "$(lsb_release -cs)" = lucid ]];then
		EFIDMESG="This $LIVESESSION-session is not EFI-compatible."
	elif [[ -d /usr/share/ubuntu-defaults-french ]] && [[ "$LIVESESSION" = live ]];then
		EFIDMESG="Le disque Ubuntu Edition Francophone ne peut pas être démarré en mode EFI."
	else # http://paste.ubuntu.com/1001831 , http://paste.ubuntu.com/966239 , http://paste.ubuntu.com/934497
		EFIDMESG="This $LIVESESSION-session is not in EFI-mode."
	fi
	[[ "$ue" ]] && EFIDMESG="$EFIDMESG
EFI in dmesg.
$ue" #ex: http://paste.ubuntu.com/1354258
fi
EFIDMESG="$EFIDMESG
SecureBoot $SECUREBOOT."
#Not sure if SecureBoot disabled
[[ -d /sys/firmware/efi ]] && [[ -d $efi_vars ]] && [[ "$SECUREBOOT" != enabled ]] \
&& EFIDMESG="$EFIDMESG (maybe sec-boot, $PLEASECONTACT)"
echo "$DASH UEFI/Legacy mode:
$EFIDMESG
"
#Ex of OS with EFI activated (http://paste.ubuntu.com/995665) / deactivated (http://paste.ubuntu.com/1003660)
}

