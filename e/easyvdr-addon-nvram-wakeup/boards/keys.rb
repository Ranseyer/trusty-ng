#
# NVRAM WakeUp - Code generator for mainboard "database"
#
# Copyright (c) 2008 Tobias Grimm
#
# $Id$
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

class Hash
    def slice(args)
        ret = {}
        args.each {|key| ret[key] = self[key] if self[key]}
        ret
    end

    def method_missing(meth,*args)
        if /=$/=~(meth=meth.id2name) then
            self[meth[0...-1]] = (args.length<2 ? args[0] : args)
        else
            self[meth]
        end
    end
end

@dmi_keys = [
     'vendor',
     'type',
     'version',
     'biosvendor',
     'biosversion',
     'biosrelease']

@biosinfo_keys = [
     'need_reboot',
     'addr_chk_h',
     'addr_chk_l',
     'addr_chk_h2',
     'addr_chk_l2',
     'addr_stat',
     'addr_mon',
     'addr_day',
     'addr_wdays',
     'addr_hour',
     'addr_min',
     'addr_sec',
     'shift_stat',
     'shift_mon',
     'shift_day',
     'shift_wdays',
     'shift_hour',
     'shift_min',
     'shift_sec',
     'rtc_time',
     'rtc_day',
     'rtc_mon',
     'rtc_day_0_is_c0',
     'rtc_mon_0_is_c0',
     'reset_day',
     'reset_mon',
     'nr_stat',
     'nr_mon',
     'nr_day',
     'nr_hour',
     'nr_min',
     'nr_sec',
     'nr_rtc_day',
     'nr_rtc_mon',
     'nr_wdays',
     'bcd',
     'day_no_bcd',
     'day_hack',
     'upper_method',
     'chk_method']
