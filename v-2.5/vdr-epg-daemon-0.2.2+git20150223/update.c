/*
 * update.c
 *
 * See the README file for copyright information
 *
 */

#include <dirent.h>
#include <unistd.h>

#include <locale.h>
#include <stdio.h>

#include "epgd.h"

int cEpgd::shutdown = no;
int cEpgd::trigger = no;

//***************************************************************************
// Class Update
//***************************************************************************

cEpgd::cEpgd()
{
   const char* lang;

   chkDoubleMap = 0;   
   selectAllMap = 0;
   selectByCompTitle = 0;
   selectMaxUpdSp = 0;
   selectDistCompname = 0;
   selectByCompName = 0;
   selectByCompNames = 0;
   selectMaxMapOrd = 0;
   updateEpisodeAtEvents = 0;
   selectNewRecordings = 0;
   countNewRecordings = 0;
   selectRecordingEvent = 0;
   selectRecOtherClient = 0;
   countDvbChanges = 0;
   selectActiveVdrs = 0;
      
   procMergeEpg = 0;
   procUser = 0;

   eventsDb = 0;
   fileDb = 0;
   imageRefDb = 0;
   imageDb = 0;
   episodeDb = 0;
   mapDb = 0;
   vdrDb = 0;
   compDb = 0;
   parameterDb = 0;
   recordingsDb = 0;
   connection = 0;

   // thread / update control

   connection = 0;
   fullupdate = no;
   fullreload = no;
   nextUpdateAt = time(0) + 10;
   lastUpdateAt = 0;
   lastMergeAt = 0;

   // ..

   xmlSubstituteEntitiesDefault(1);
   xmlLoadExtDtdDefaultValue = 1;
   withutf8 = no;

   // scraper stuff

   tvdbManager = 0;
   movieDbManager = 0;

   // ..

   exsltRegisterAll();

   setlocale(LC_CTYPE, "");
   lang = setlocale(LC_CTYPE, 0);
   
   if (lang)
   {
      tell(0, "Set locale to '%s'", lang);
      
      if ((strcasestr(lang, "UTF-8") != 0) || (strcasestr(lang, "UTF8") != 0))
      {
         tell(0, "detected UTF-8");
         withutf8 = yes;
      }
   }
   else
   {
      tell(0, "Reseting locale for LC_CTYPE failed.");
   }
}

cEpgd::~cEpgd()
{
   xsltCleanupGlobals();
   xmlCleanupParser();
   
   exitScrapers();
   exitDb();

   cDbConnection::exit();

   vector<PluginLoader*>::iterator it;

   for (it = plugins.begin(); it < plugins.end(); it++)
      delete *it;

   plugins.clear();
}

//***************************************************************************
// Init
//***************************************************************************

int cEpgd::init()
{
   int status;

   // first - prepare my uuid
   
   initUuid();
   
   // load plugins

   loadPlugins();

   // read configuration ..

   if (readConfig() != success)
      return fail;

   // init database ...

   cDbConnection::init();
   cDbConnection::setEncoding(withutf8 ? "utf8": "latin1"); // mysql uses latin1 for ISO8851-1
   cDbConnection::setHost(EPG2VDRConfig.dbHost);
   cDbConnection::setPort(EPG2VDRConfig.dbPort);
   cDbConnection::setName(EPG2VDRConfig.dbName);
   cDbConnection::setUser(EPG2VDRConfig.dbUser);
   cDbConnection::setPass(EPG2VDRConfig.dbPass);

   cDbStatement::explain = EPG2VDRConfig.loglevel >= 4;

   cDbTable::setConfPath(confDir);

   chkDir(EPG2VDRConfig.cachePath);

   initPlugins();

   // open tables ..

   if ((status = initDb()) != success)
   {
      exitDb();

      if (status == abrt)
         return status;
   }

   getParameter("lastMergeAt", lastMergeAt);
   
   if (EPG2VDRConfig.scrapEpg || EPG2VDRConfig.scrapRecordings) 
   {
      if (initScrapers() != success)
         tell(0, "Error connecting scrapers, no scrap process will be initiated");
   }

   return success;
}

//***************************************************************************
// Init Uuid
//***************************************************************************

int cEpgd::initUuid()
{
   MemoryStruct data;
   char* uuidFile;

   asprintf(&uuidFile, "%s/uuid", confDir);

   if (fileExists(uuidFile))
   {
      if (loadFromFile(uuidFile, &data) == success)
      {
         memset(EPG2VDRConfig.uuid, 0, sizeof(EPG2VDRConfig.uuid));
         memcpy(EPG2VDRConfig.uuid, data.memory, min(data.size, sizeof(EPG2VDRConfig.uuid)));
         tell(1, "Loading uuid from '%s' succeeded [%s]", uuidFile, EPG2VDRConfig.uuid);
      }
      else
      {
         tell(0, "Error: Load of uuid from '%s' failed, using '<unknown>' instead", uuidFile);
         sstrcpy(EPG2VDRConfig.uuid, "<unknown>", sizeof(EPG2VDRConfig.uuid));
      }
   }
   else
   {
      tell(1, "Initially creating uuid, storing to '%s'", uuidFile);
      sstrcpy(EPG2VDRConfig.uuid, getUniqueId(), sizeof(EPG2VDRConfig.uuid));
      storeToFile(uuidFile, EPG2VDRConfig.uuid, strlen(EPG2VDRConfig.uuid));
   }

   free(uuidFile);

   return done;
}

//***************************************************************************
// Configuration
//***************************************************************************

int cEpgd::atConfigItem(const char* Name, const char* Value)
{
   char* par;
   char* plug = strdup(Name);

   // config for plugin ?

   if (par = strchr(plug, '.'))
   {
      vector<PluginLoader*>::iterator it;

      *par++ = 0;
      
      for (it = plugins.begin(); it < plugins.end(); it++)
      {
         Plugin* p = (*it)->getPlugin();
         
         if (p->hasSource(plug))
         {
            int status = p->atConfigItem(par, Value);
            free(plug);
            return status;
         }
      }
      
      free(plug);

      return success;
   }

   free(plug);

   // Parse setup parameters and store values.
   
   if      (!strcasecmp(Name, "DbHost"))             sstrcpy(EPG2VDRConfig.dbHost, Value, sizeof(EPG2VDRConfig.dbHost));
   else if (!strcasecmp(Name, "DbPort"))             EPG2VDRConfig.dbPort = atoi(Value);
   else if (!strcasecmp(Name, "DbName"))             sstrcpy(EPG2VDRConfig.dbName, Value, sizeof(EPG2VDRConfig.dbName));
   else if (!strcasecmp(Name, "DbUser"))             sstrcpy(EPG2VDRConfig.dbUser, Value, sizeof(EPG2VDRConfig.dbUser));
   else if (!strcasecmp(Name, "DbPass"))             sstrcpy(EPG2VDRConfig.dbPass, Value, sizeof(EPG2VDRConfig.dbPass));

   else if (!strcasecmp(Name, "EpgView"))            sstrcpy(EPG2VDRConfig.epgView, Value, sizeof(EPG2VDRConfig.epgView));
   else if (!strcasecmp(Name, "TheTvDBView"))        sstrcpy(EPG2VDRConfig.theTvDBView, Value, sizeof(EPG2VDRConfig.theTvDBView));

   else if (!strcasecmp(Name, "CheckInitial"))       EPG2VDRConfig.checkInitial = atoi(Value);
   else if (!strcasecmp(Name, "DaysInAdvance"))      EPG2VDRConfig.days = atoi(Value);
   else if (!strcasecmp(Name, "DaysToUpdate"))       EPG2VDRConfig.upddays = atoi(Value);
   else if (!strcasecmp(Name, "UpdateTime"))         EPG2VDRConfig.updatetime = atoi(Value);
   else if (!strcasecmp(Name, "XmlStoreToFs"))       EPG2VDRConfig.storeXmlToFs = atoi(Value);
   else if (!strcasecmp(Name, "UpdateThreshold"))    EPG2VDRConfig.updateThreshold = atoi(Value);
   
   else if (!strcasecmp(Name, "GetEPGImages"))       EPG2VDRConfig.getepgimages = atoi(Value);
   else if (!strcasecmp(Name, "MaxImagesPerEvent"))  EPG2VDRConfig.maximagesperevent = atoi(Value);
   else if (!strcasecmp(Name, "EpgImageSize"))       EPG2VDRConfig.epgImageSize = atoi(Value);
   
   else if (!strcasecmp(Name, "SeriesUrl"))          sstrcpy(EPG2VDRConfig.seriesUrl, Value, sizeof(EPG2VDRConfig.seriesUrl));
   else if (!strcasecmp(Name, "SeriesEnabled"))      EPG2VDRConfig.seriesEnabled = atoi(Value);
   else if (!strcasecmp(Name, "SeriesPort"))         EPG2VDRConfig.seriesPort = atoi(Value);
   else if (!strcasecmp(Name, "SeriesStoreToFs"))    EPG2VDRConfig.storeSeriesToFs = atoi(Value);

   else if (!strcasecmp(Name, "CachePath"))          sstrcpy(EPG2VDRConfig.cachePath, Value, sizeof(EPG2VDRConfig.cachePath));

   else if (!strcasecmp(Name, "UseProxy"))           EPG2VDRConfig.useproxy = atoi(Value);
   else if (!strcasecmp(Name, "HTTPProxy"))          sstrcpy(EPG2VDRConfig.httpproxy, Value, sizeof(EPG2VDRConfig.httpproxy));
   else if (!strcasecmp(Name, "UserName"))           sstrcpy(EPG2VDRConfig.username, Value, sizeof(EPG2VDRConfig.username));
   else if (!strcasecmp(Name, "Password"))           sstrcpy(EPG2VDRConfig.password, Value, sizeof(EPG2VDRConfig.password));
   
   else if (!strcasecmp(Name, "LogLevel"))           EPG2VDRConfig.loglevel = EPG2VDRConfig.loglevel == 1 ? atoi(Value) : EPG2VDRConfig.loglevel;
   
   else if (!strcasecmp(Name, "ScrapEpg"))           EPG2VDRConfig.scrapEpg = atoi(Value);
   else if (!strcasecmp(Name, "ScrapRecordings"))    EPG2VDRConfig.scrapRecordings = atoi(Value);

   else
      return fail;
   
   return success;
}

//***************************************************************************
// Init/Exit Database Connections
//***************************************************************************

cDBS::FieldDef changeCountDef = { "count(1)", cDBS::ffUInt,  0, 999, cDBS::ftData };

int cEpgd::initDb()
{
   int status = success;
   int count = 0;

   if (!connection)
      connection = new cDbConnection();

   // -------------------------

   static int initial = yes;

   vdrDb = new cTableVdrs(connection);
   if (vdrDb->open() != success) return fail;

   if (initial)
   {
      if (registerMe() != success)
         return fail;

      initial = no;
   }

   // ------------------------
   // create/open other tables 
   // ------------------------

   mapDb = new cTableChannelMap(connection);
   if (mapDb->open() != success) return fail;

   fileDb = new cTableFileRefs(connection);
   if (fileDb->open() != success) return fail;

   imageDb = new cTableImages(connection);
   if (imageDb->open() != success) return fail;

   imageRefDb = new cTableImageRefs(connection);
   if (imageRefDb->open() != success) return fail;

   episodeDb = new cTableEpisodes(connection);
   if (episodeDb->open() != success) return fail;

   eventsDb = new cTableEvents(connection);
   if (eventsDb->open() != success) return fail;

   compDb = new cTableComponents(connection);
   if (compDb->open() != success) return fail;

   parameterDb = new cTableParameters(connection);
   if (parameterDb->open() != success) return fail;

   recordingsDb = new cTableRecordings(connection);
   if (recordingsDb->open() != success) return fail;

   // ------------------------------------
   // check if epglv/epglvr are installed

   status += connection->query("%s", "select epglv('123', '123')");
   connection->queryReset();
   status += connection->query("%s", "select epglvr('123', '123')");
   connection->queryReset();

   if (status != success)
   {
      tell(0, "Error: Missing functions epglv/epglvr, please install first!");
      return abrt;
   }

   // ------------------
   // create tables used by procedure
   // ------------------
 
   cDbTable* db = new cTableAnalyse(connection);
   status += db->createTable();
   status += db->createIndices();
   delete db;

   db = new cTableSnapshot(connection);
   status += db->createTable();
   status += db->createIndices();
   delete db;

   if (status != success)
      return fail;

   // ---------------------------
   // prepare statements
   // ---------------------------

   // ----------
   // select ip, svdrp from vdrs
   //   where state = 'attached' 

//    selectActiveVdrs = new cDbStatement(vdrDb);

//    selectActiveVdrs->build("select ");
//    selectActiveVdrs->bind(cTableVdrs::fiIp, cDBS::bndOut);
//    selectActiveVdrs->bind(cTableVdrs::fiSvdrp, cDBS::bndOut, ", ");
//    selectActiveVdrs->build(" from %s where state = 'attached'", vdrDb->TableName());

//    status += selectActiveVdrs->prepare();

   // ----------
   // select extid from channelmap
   //   where channelid = ? 
   //     and source <> ? 
   //     and updflg <> 'D'

   chkDoubleMap = new cDbStatement(mapDb);

   chkDoubleMap->build("select extid from %s where ", mapDb->TableName());
   chkDoubleMap->bind(cTableChannelMap::fiChannelId, cDBS::bndIn | cDBS::bndSet);
   chkDoubleMap->bindCmp(0, cTableChannelMap::fiSource, 0, "<>", " and ");
   chkDoubleMap->build(" and updflg <> 'D'");

   status += chkDoubleMap->prepare();

//    // --------------------
//    // select max(ord) from channelmap

//    selectMaxMapOrd = new cDbStatement(mapDb);

//    selectMaxMapOrd->build("select ");
//    selectMaxMapOrd->bind(cTableChannelMap::fiOrder, cDBS::bndOut, "max(");
//    selectMaxMapOrd->build(") from %s", mapDb->TableName());

//    status += selectMaxMapOrd->prepare();

   // ----------
   // select extid, source, updflg
   //   from channelmap

   selectAllMap = new cDbStatement(mapDb);

   selectAllMap->build("select ");
   selectAllMap->bind(cTableChannelMap::fiExternalId, cDBS::bndOut);
   selectAllMap->bind(cTableChannelMap::fiChannelId, cDBS::bndOut, ", ");
   selectAllMap->bind(cTableChannelMap::fiSource, cDBS::bndOut, ", ");
   selectAllMap->bind(cTableChannelMap::fiUpdFlg, cDBS::bndOut, ", ");
   selectAllMap->build(" from %s", mapDb->TableName());

   status += selectAllMap->prepare();

   // ----------
   // update events set episode = ?, episodepart = ?, episodelang = ? 
   //     where eventid = ? and channelid = ?

   updateEpisodeAtEvents = new cDbStatement(eventsDb);

   updateEpisodeAtEvents->build("update %s set ", eventsDb->TableName());
   updateEpisodeAtEvents->bind(cTableEvents::fiUpdSp, cDBS::bndIn | cDBS::bndSet);
   updateEpisodeAtEvents->bind(cTableEvents::fiEpisode, cDBS::bndIn | cDBS::bndSet, ", ");
   updateEpisodeAtEvents->bind(cTableEvents::fiEpisodePart, cDBS::bndIn | cDBS::bndSet, ", ");
   updateEpisodeAtEvents->bind(cTableEvents::fiEpisodeLang, cDBS::bndIn | cDBS::bndSet, ", ");
   updateEpisodeAtEvents->build(" where ");
   updateEpisodeAtEvents->bind(cTableEvents::fiEventId, cDBS::bndIn | cDBS::bndSet);
   updateEpisodeAtEvents->bind(cTableEvents::fiChannelId, cDBS::bndIn | cDBS::bndSet, " and ");

   status += updateEpisodeAtEvents->prepare();

   // ----------
   // select eventid, compshorttext, episodepart, episodelang 
   //   from events 
   //     where comptitle = ?

   selectByCompTitle = new cDbStatement(eventsDb);

   selectByCompTitle->build("select ");
   selectByCompTitle->bind(cTableEvents::fiEventId, cDBS::bndOut);
   selectByCompTitle->bind(cTableEvents::fiChannelId, cDBS::bndOut, ", ");
   selectByCompTitle->bind(cTableEvents::fiCompShortText, cDBS::bndOut, ", ");
   selectByCompTitle->bind(cTableEvents::fiEpisodePart, cDBS::bndOut, ", ");
   selectByCompTitle->bind(cTableEvents::fiEpisodeLang, cDBS::bndOut, ", ");
   selectByCompTitle->build(" from %s where ", eventsDb->TableName());
   selectByCompTitle->bind(cTableEvents::fiCompTitle, cDBS::bndIn | cDBS::bndSet);

   status += selectByCompTitle->prepare();

   //---------------------
   // select count(1) from events 
   //  where source = ?
   //    and updsp > ?

   changeCount.setField(&changeCountDef);

   countDvbChanges = new cDbStatement(eventsDb);

   countDvbChanges->build("select ");
   countDvbChanges->bind(&changeCount, cDBS::bndOut);
   countDvbChanges->build(" from %s where ", eventsDb->TableName());
   countDvbChanges->bind(cTableEvents::fiSource, cDBS::bndIn | cDBS::bndSet);
   countDvbChanges->bindCmp(0, cTableEvents::fiUpdSp, 0, ">", " and ");
   
   status += countDvbChanges->prepare();

   // --------------------
   // select max(updsp) from episodes

   selectMaxUpdSp = new cDbStatement(episodeDb);

   selectMaxUpdSp->build("select ");
   selectMaxUpdSp->bind(cTableEpisodes::fiUpdSp, cDBS::bndOut, "max(");
   selectMaxUpdSp->build(") from %s", episodeDb->TableName());

   status += selectMaxUpdSp->prepare();

   // --------------------
   // select distinct compname from episodes

   selectDistCompname = new cDbStatement(episodeDb);

   selectDistCompname->build("select ");
   selectDistCompname->bind(cTableEpisodes::fiCompName, cDBS::bndOut, "distinct ");
   selectDistCompname->build(" from %s", episodeDb->TableName());

   status += selectDistCompname->prepare();

   // --------------------
   // select comppartname, lang 
   //   from episodes
   //     where compname = ?

   selectByCompName = new cDbStatement(episodeDb);

   selectByCompName->build("select ");
   selectByCompName->bind(cTableEpisodes::fiCompPartName, cDBS::bndOut);
   selectByCompName->bind(cTableEpisodes::fiLang, cDBS::bndOut, ", ");
   selectByCompName->build(" from %s where ", episodeDb->TableName());
   selectByCompName->bind(cTableEpisodes::fiCompName, cDBS::bndIn | cDBS::bndSet);

   status += selectByCompName->prepare();

   // --------------------
   // select episodename, partname, lang 
   //   from episodes
   //     where compname = ? and comppartname = ?

   selectByCompNames = new cDbStatement(episodeDb);

   selectByCompNames->build("select ");
   selectByCompNames->bind(cTableEpisodes::fiEpisodeName, cDBS::bndOut);
   selectByCompNames->bind(cTableEpisodes::fiPartName, cDBS::bndOut, ", ");
   selectByCompNames->bind(cTableEpisodes::fiLang, cDBS::bndOut, ", ");
   selectByCompNames->build(" from %s where ", episodeDb->TableName());
   selectByCompNames->bind(cTableEpisodes::fiCompName, cDBS::bndIn | cDBS::bndSet);
   selectByCompNames->bind(cTableEpisodes::fiCompPartName, cDBS::bndIn | cDBS::bndSet, " and ");

   status += selectByCompNames->prepare();

   // --------------------
   // select count(1)
   //   from recordings
   //     where scrap_new = ?

   newRecCount.setField(&changeCountDef);

   countNewRecordings = new cDbStatement(recordingsDb);
   countNewRecordings->build("select ");
   countNewRecordings->bind(&newRecCount, cDBS::bndOut);
   countNewRecordings->build(" from %s where ", recordingsDb->TableName());
   countNewRecordings->bind(cTableRecordings::fiScrapNew, cDBS::bndIn | cDBS::bndSet);

   status += countNewRecordings->prepare();

   // --------------------
   // select event_id, channel_id, scrapinfo_movie_id, scrapinfo_series_id, scrapinfo_episode_id,
   //        rec_title, rec_subtitle, rec_duration
   //   from recordings
   //     where scrap_new = ?

   selectNewRecordings = new cDbStatement(recordingsDb);
   selectNewRecordings->build("select ");
   selectNewRecordings->bind(cTableRecordings::fiUuid, cDBS::bndOut);
   selectNewRecordings->bind(cTableRecordings::fiRecPath, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiRecStart, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiEventId, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiChannelId, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiScrapInfoMovieId, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiScrapInfoSeriesId, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiScrapInfoEpisodeId, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiRecTitle, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiRecSubTitle, cDBS::bndOut, ", ");
   selectNewRecordings->bind(cTableRecordings::fiRecDuration, cDBS::bndOut, ", ");
   selectNewRecordings->build(" from %s where ", recordingsDb->TableName());
   selectNewRecordings->bind(cTableRecordings::fiScrapNew, cDBS::bndIn | cDBS::bndSet);

   status += selectNewRecordings->prepare();

   // --------------------
   // select scrseriesid, scrseriesepisode, scrmovieid 
   //   from events 
   //     where useid = ? and channelid = ? and (series_id is not null or movie_id is not null);

   selectRecordingEvent = new cDbStatement(eventsDb);
   selectRecordingEvent->build("select ");
   selectRecordingEvent->bind(cTableEvents::fiScrSeriesId, cDBS::bndOut);
   selectRecordingEvent->bind(cTableEvents::fiScrSeriesEpisode, cDBS::bndOut, ", ");
   selectRecordingEvent->bind(cTableEvents::fiScrMovieId, cDBS::bndOut, ", ");
   selectRecordingEvent->build(" from %s where ", eventsDb->TableName());
   selectRecordingEvent->bind(cTableEvents::fiMasterId, cDBS::bndIn | cDBS::bndSet);
   selectRecordingEvent->bind(cTableEvents::fiChannelId, cDBS::bndIn | cDBS::bndSet, " and ");
   selectRecordingEvent->build(" and (%s is not null or %s is not null)", eventsDb->getField(cTableEvents::fiScrSeriesId)->name,
                               eventsDb->getField(cTableEvents::fiScrMovieId)->name );

   status += selectRecordingEvent->prepare();

   // --------------------
   // select series_id, episode_id, movie_id
   //   from recordings
   //     where uuid != ? and rec_path = ? and rec_start > ? and rec_start < ? and (series_id > 0 or movie_id > 0);

   selectRecOtherClient = new cDbStatement(recordingsDb);
   selectRecOtherClient->build("select ");
   selectRecOtherClient->bind(cTableRecordings::fiSeriesId, cDBS::bndOut);
   selectRecOtherClient->bind(cTableRecordings::fiEpisodeId, cDBS::bndOut, ", ");
   selectRecOtherClient->bind(cTableRecordings::fiMovieId, cDBS::bndOut, ", ");
   selectRecOtherClient->build(" from %s where ", recordingsDb->TableName());
   selectRecOtherClient->bindCmp(0, cTableRecordings::fiUuid, 0, "!=");
   selectRecOtherClient->bindCmp(0, cTableRecordings::fiRecPath, 0, " like ", " and ");
   selectRecOtherClient->bind(cTableRecordings::fiRecStart, cDBS::bndIn | cDBS::bndSet, " and ");
   selectRecOtherClient->build(" and (%s > 0 or %s > 0)", recordingsDb->getField(cTableRecordings::fiSeriesId)->name,
                               recordingsDb->getField(cTableRecordings::fiMovieId)->name );

   status += selectRecOtherClient->prepare();

   // --------------------
   // update Channel Map

   loadChannelmap();
   applyChannelmapChanges();

   initPluginDb();

   // --------------------------
   // procedures / views

   if (!status)
   {
      procMergeEpg = new cDbProcedure(connection, "mergeepg");
      status += checkProcedure("mergeepg", cDbProcedure::ptProcedure, procMergeEpg);

      status += checkProcedure("reverseepg", cDbProcedure::ptProcedure);
      status += checkProcedure("getupdflg", cDbProcedure::ptFunction);
      status += checkProcedure("getcrosslvr", cDbProcedure::ptFunction);
      status += checkProcedure("getlvrmin", cDbProcedure::ptFunction);

      // optional create user procedure ...

      if (cDbProcedure::existOnFs(confDir, "userexit"))
      {
         procUser = new cDbProcedure(connection, "userexit");
         status += checkProcedure("userexit", cDbProcedure::ptProcedure, procUser);
      }
      
      // ------------------
      // views ...

      status += checkView("eventsview", EPG2VDRConfig.epgView);
      status += checkView("eventsviewplain", "eventsviewplain.sql");
      status += checkView("thetvdbview", EPG2VDRConfig.theTvDBView);
   }

   // force initial check on start with empty tables

   eventsDb->countWhere("source != 'vdr'", count);

   if (!count)
   {
      tell(1, "No external events on database, force initial check!");
      EPG2VDRConfig.checkInitial = yes;
   }

   // ------------------
   // Maintanance Mode

   if (status == success && EPG2VDRConfig.maintanance)
   {
      time_t start = time(0);
      int count = 0;

      cDbStatement sel(eventsDb);
      sel.build("select ");
      sel.bind(cTableEvents::fiEventId, cDBS::bndOut);
      sel.bind(cTableEvents::fiChannelId, cDBS::bndOut, ", ");
      sel.bind(cTableEvents::fiTitle, cDBS::bndOut, ", ");
      sel.bind(cTableEvents::fiShortText, cDBS::bndOut, ", ");
      sel.build(" from events");
      sel.prepare();

      cDbStatement upd(eventsDb);
      upd.build("update events set ");
      upd.bind(cTableEvents::fiCompTitle, cDBS::bndIn | cDBS::bndSet);
      upd.bind(cTableEvents::fiCompShortText, cDBS::bndIn | cDBS::bndSet, ", ");
      upd.build(" where ");
      upd.bind(cTableEvents::fiEventId, cDBS::bndIn | cDBS::bndSet);
      upd.bind(cTableEvents::fiChannelId, cDBS::bndIn | cDBS::bndSet, " and ");
      upd.prepare();

      connection->startTransaction();

      for (int f = sel.find(); f; f = sel.fetch())
      {
         string comp;

         tell(0, "update comp of %ld", eventsDb->getIntValue(cTableEvents::fiEventId));
         
         if (!eventsDb->isNull(cTableEvents::fiTitle))
         {
            comp = eventsDb->getStrValue(cTableEvents::fiTitle);
            prepareCompressed(comp);
            eventsDb->setValue(cTableEvents::fiCompTitle, comp.c_str());
         }
         
         if (!eventsDb->isNull(cTableEvents::fiShortText))
         {
            comp = eventsDb->getStrValue(cTableEvents::fiShortText);
            prepareCompressed(comp);
            eventsDb->setValue(cTableEvents::fiCompShortText, comp.c_str());
         }
         
         upd.execute();
         count++;
      }

      connection->commit();

      tell(0, "updated %d events in %ld seconds", count, time(0) - start);
      upd.freeResult();
      sel.freeResult();
      exitDb();
      exit(1);
   }

   return status;
}

int cEpgd::exitDb()
{
   exitPluginDb();

   delete chkDoubleMap;           chkDoubleMap = 0;
   delete selectMaxMapOrd;        selectMaxMapOrd = 0;

   delete selectAllMap;           selectAllMap = 0;
   delete selectByCompTitle;      selectByCompTitle = 0;
   delete selectMaxUpdSp;         selectMaxUpdSp = 0;
   delete selectDistCompname;     selectDistCompname = 0;
   delete selectByCompName;       selectByCompName = 0;
   delete selectByCompNames;      selectByCompNames = 0;
   delete updateEpisodeAtEvents;  updateEpisodeAtEvents = 0;
   delete countDvbChanges;        countDvbChanges = 0;
   delete selectNewRecordings;    selectNewRecordings = 0;
   delete countNewRecordings;     countNewRecordings = 0;
   delete selectRecordingEvent;   selectRecordingEvent = 0;
   delete selectRecOtherClient;   selectRecOtherClient = 0;
   delete selectActiveVdrs;       selectActiveVdrs = 0;

   delete procMergeEpg;  procMergeEpg = 0;
   delete procUser;      procUser = 0;

   delete eventsDb;      eventsDb = 0;
   delete fileDb;        fileDb = 0;
   delete imageRefDb;    imageRefDb = 0;
   delete imageDb;       imageDb = 0;
   delete episodeDb;     episodeDb = 0;
   delete mapDb;         mapDb = 0;
   delete vdrDb;         vdrDb = 0;
   delete compDb;        compDb = 0;
   delete parameterDb;   parameterDb = 0;
   delete recordingsDb;  recordingsDb = 0;

   delete connection;    connection = 0;

   return done;
}

//***************************************************************************
// Check Function
//***************************************************************************

int cEpgd::checkProcedure(const char* name, cDBS::ProcType type, cDbProcedure* fp)
{
   char* file = 0;
   char* param = 0;
   int status = success;
   char md5[100+TB]; *md5 = 0;
   md5Buf md5New; md5New[0] = 0;
   cDbProcedure* p = 0;
  
   asprintf(&file, "%s.sql", name);

   if (createMd5OfFile(confDir, file, md5New) != success)
   {
      tell(0, "Error: Can't access procedure '%s/%s'", confDir, file);
      free(file);
      return fail;
   }

   asprintf(&param, "%s.md5", name);
   p = fp ? fp : new cDbProcedure(connection, name, type);

   if (p->created())
   {
      getParameter(param, md5);

      if (strcmp(md5, md5New) != 0)          // drop if changed
         p->drop();
   }
   
   if (!p->created())
   {
      status = p->create(confDir);
      setParameter(param, md5New);
   }
   
   free(file);
   free(param);

   if (!fp)
      delete p;

   return status;
}

//***************************************************************************
// Check View
//***************************************************************************

int cEpgd::checkView(const char* name, const char* file)
{
   int status = success;
   char md5[100+TB]; *md5 = 0;
   md5Buf md5New; md5New[0] = 0;
   char* param = 0;

   // create/check view

   cDbView view(connection, name);

   asprintf(&param, "%s.md5", name);

   if (createMd5OfFile(confDir, file, md5New) != success)
   {
      tell(0, "Error: Can't access view '%s/%s'", confDir, file);
      status = fail;
   }
   
   if (status == success)
   {
      if (view.exist())
      {
         getParameter(param, md5);
         
         if (strcmp(md5, md5New) != 0)          // drop if changed
            view.drop();
      }
      
      if (!view.exist())
      {
         status += view.create(confDir, file);
         setParameter(param, md5New);
      }
   }

   free(param);

   return status;
}

//***************************************************************************
// Register Me 
//***************************************************************************

int cEpgd::registerMe()
{
   char* v = 0;

   if (!dbConnected())
      return fail;

   // -------------------------------------------
   // register me to the clients table

   asprintf(&v, "epgd %s (%s)", VERSION, VERSION_DATE);

   vdrDb->clear();
   vdrDb->setValue(cTableVdrs::fiUuid, "epgd");
   vdrDb->find();

   if (!vdrDb->isNull(cTableVdrs::fiDbApi) &&
       vdrDb->getIntValue(cTableVdrs::fiDbApi) != DB_API)
   {
      tell(0, "Found dbapi %ld, expected %d, please alter the tables first! Aborting now.",  
           vdrDb->getIntValue(cTableVdrs::fiDbApi), DB_API);
      free(v);

      return fail;
   }

   vdrDb->setValue(cTableVdrs::fiIp, getFirstIp());
   vdrDb->setValue(cTableVdrs::fiName, getHostName());
   vdrDb->setValue(cTableVdrs::fiDbApi, DB_API);
   vdrDb->setValue(cTableVdrs::fiVersion, v);
   vdrDb->setValue(cTableVdrs::fiMaster, "-");
   vdrDb->store();

   setState(Es::esInit);

   free(v);

   return dbConnected() ? success : fail;
}

//***************************************************************************
// Set State
//***************************************************************************

void cEpgd::setState(Es::State state, time_t lastUpdate, int silent)
{
   static Es::State actualState = Es::esUnknown;

   if (!dbConnected())
      return;

   if (actualState != state)
   {
//      Es::State lastState = actualState;

      actualState = state;

      if (!silent)
         tell(0, "State now '%s'", Es::toName(actualState));
      
      vdrDb->clear();
      vdrDb->setValue(cTableVdrs::fiUuid, "epgd");
      vdrDb->find();
      vdrDb->setValue(cTableVdrs::fiState, Es::toName(actualState));
      vdrDb->setValue(cTableVdrs::fiNextUpdate, nextUpdateAt);

      if (lastUpdate) 
         vdrDb->setValue(cTableVdrs::fiLastUpdate, lastUpdate);

      vdrDb->store();

//       if (actualState == cEpgdState::esStandby)
//          triggerVdrs(Es::toName(lastState));
   }
}

//***************************************************************************
// Load Plugins
//***************************************************************************

int cEpgd::loadPlugins()
{
   DIR* dir;
   dirent* dp;

   if (!(dir = opendir(EPG2VDRConfig.pluginPath)))
   {
      tell(0, "Error: Opening plugin directory '%s' failed, %s", 
           EPG2VDRConfig.pluginPath, strerror(errno));
      return fail;
   }

   while (dp = readdir(dir))
   {
      if (strncmp(dp->d_name, "libepgd-", 8) == 0 && strstr(dp->d_name, ".so"))
      {
         char* path;

         asprintf(&path, "%s/%s", EPG2VDRConfig.pluginPath, dp->d_name);
               
         PluginLoader* pl = new PluginLoader(path);

         free(path);

         if (pl->load() != success)
         {
            delete pl;
            continue;
         }
 
         plugins.push_back(pl);
      }
   }
   
   closedir(dir);

   return success;
}

int cEpgd::initPlugins()
{
   vector<PluginLoader*>::iterator it;

   for (it = plugins.begin(); it < plugins.end(); it++)
      (*it)->getPlugin()->init(this, withutf8);
   
   return done;
}

int cEpgd::initPluginDb()
{
   vector<PluginLoader*>::iterator it;

   for (it = plugins.begin(); it < plugins.end(); it++)
      (*it)->getPlugin()->initDb();
   
   return done;
}

int cEpgd::exitPluginDb()
{
   vector<PluginLoader*>::iterator it;

   for (it = plugins.begin(); it < plugins.end(); it++)
      (*it)->getPlugin()->exitDb();
   
   return done;
}

//***************************************************************************
// Schedule Auto Update
//***************************************************************************

void cEpgd::scheduleAutoUpdate(int wait)
{
   if (wait)
      nextUpdateAt = time(0) + wait;
   else
      nextUpdateAt = time(0) + EPG2VDRConfig.updatetime * 60 * 60;

   if (nextUpdateAt && EPG2VDRConfig.loglevel > 0)
   {
      if ((nextUpdateAt-time(0))/60 < 1)
         tell(0, "Scheduled next update in %ld second(s)", 
              nextUpdateAt-time(0));
      else if ((nextUpdateAt-time(0))/60/60 < 1)
         tell(0, "Scheduled next update in %ld minute(s)", 
              (nextUpdateAt-time(0))/60);
      else
         tell(0, "Scheduled next update in %ld hour(s)", 
              (nextUpdateAt-time(0))/60/60);
   }
}

//***************************************************************************
// Loop
//***************************************************************************

void cEpgd::loop()
{
   time_t lastCheckAt = 0;
   time_t lastUpdCheckAt = 0;
   time_t lastRecCheckAt = 0;

   shutdown = no;

   // first run 10 seconds after start (if configured)

   scheduleAutoUpdate(EPG2VDRConfig.checkInitial ? 10 : 0);

   while (!doShutDown())
   {      
      setState(Es::esStandby);

      // wait for update and perform meanwhile actions

      while (!doTrigger() && !doShutDown() && nextUpdateAt > time(0))
      {
         sleep(1);

         if (lastCheckAt < time(0) - 60)
         {
            // check active vdrs every 60 seconds
            //   and reset if they are silent over 5 minutes
          
            lastCheckAt = time(0);

            if (checkConnection() != success)
               continue;
  
            connection->query("%s", "update vdrs set"
                              " state = 'crashed', master = 'n'" 
                              " where state = 'attached'"
                              " and from_unixtime(updsp) < (now() - interval 5 minute);");
         }

         if (!dbConnected())
            continue;
         
         // check all 30 seconds if new recordings are in db
         
         if (!doShutDown() && lastRecCheckAt < time(0) - 30 && nextUpdateAt > time(0) + 30 && EPG2VDRConfig.scrapRecordings)
         {
            lastRecCheckAt = time(0);

            // check if we need a recording scrap

            recordingsDb->clear();
            recordingsDb->setValue(cTableRecordings::fiScrapNew, 1);
            countNewRecordings->execute();

            // if more than x DVB updates pending call mergeepg

            if (newRecCount.getIntValue() >= 1)
            {
               setState(Es::esBusyScraping, 0, no);
               sleep(11);                       // wait 11 seconds until vdrs detecting the busy state
               
               if (!doShutDown())
                  scrapNewRecordings(newRecCount.getIntValue());
               
               setState(Es::esStandby, 0, no);
            }

            countNewRecordings->freeResult();
         }
         
         // check all 30 seconds if we need a mergeepg call

         if (!doShutDown() && lastUpdCheckAt < time(0) - 30 && nextUpdateAt > time(0) + 30)
         {
            lastUpdCheckAt = time(0);

            // check if we need a merge 

            eventsDb->clear();
            eventsDb->setValue(cTableEvents::fiSource, "vdr");
            eventsDb->setValue(cTableEvents::fiUpdSp, lastMergeAt);
            countDvbChanges->execute();

            // if more than x DVB updates pending call mergeepg

            if (changeCount.getIntValue() > EPG2VDRConfig.updateThreshold)
            {
               setState(Es::esBusyMatch, 0, no);
               sleep(11);                       // wait 11 seconds until vdrs detecting the busy state
               
               if (!doShutDown())
               {
                  uint64_t start = cTimeMs::Now();

                  connection->startTransaction();
                  procMergeEpg->call(2);
                  connection->commit();
                  
                  tell(1, "%ld DVB pending, mergeepg done after %s", 
                       changeCount.getIntValue(), ms2Dur(cTimeMs::Now()-start).c_str());
                  
                  lastMergeAt = time(0);
                  setParameter("lastMergeAt", lastMergeAt);
               }

               setState(Es::esStandby, 0, no);
            }

            countDvbChanges->freeResult();
         }

         if (EPG2VDRConfig.loglevel > 2)
            connection->showStat("merge/rec-scrap");
      }

      trigger = no;                             // reset SIGHUB trigger

      if (doShutDown())
         break;

      // database connection established ?

      if (checkConnection() != success)
      {
         nextUpdateAt = time(0) + 10;
         continue;
      }
      // work

      setState(Es::esBusyEvents);

      if (!doShutDown())
         if (cleanupEvents() != success)        // cleanup event and fileref table
            continue;

      if (!doShutDown())
         if (cleanupBefore() != success)        // cleanup plugins
            continue;

      if (!doShutDown())
         if (update() != success)               // update epg data
            continue;


      if (!doShutDown())
         downloadEpisodes();                    // download and store optionally on local fs

      if (!doShutDown() && procUser)            // call user procedure if defined
         procUser->call();
      
      if (!doShutDown())
         evaluateEpisodes();                    // try series match

      if (!doShutDown())
         procMergeEpg->call();

      lastUpdateAt = time(0);
      setState(Es::esBusyImages, lastUpdateAt);

      if (!dbConnected())
         continue;

      if (!doShutDown())      
         if (cleanupPictures() != success)      // at last cleanup pictures
            continue;

      if (!doShutDown())
         if (getPictures() != success)          // get pictures
            continue;

      if (!doShutDown())
         if (cleanupAfter() != success)         // cleanup plugins
            continue;

      setState(Es::esBusyScraping);
      sleep(11);                                // wait 11 seconds until vdrs detecting the busy state

      if (!doShutDown() && EPG2VDRConfig.scrapEpg)
         if (scrapNewEvents() != success)       // scrap new events
            continue;

      if (!doShutDown() && (EPG2VDRConfig.scrapEpg || EPG2VDRConfig.scrapRecordings))
         if (cleanupSeriesAndMovies() != success)   // cleanup scraped movies and series 
            continue;

      if (dbConnected())
         scheduleAutoUpdate();

      setState(Es::esStandby, time(0));
      
      if (EPG2VDRConfig.loglevel > 2)
         connection->showStat("main loop");
   } 

   setState(Es::esStopped);
}

//***************************************************************************
// Check Connection
//***************************************************************************

int cEpgd::checkConnection()
{
   static int retry = 0;

   // check connection
   
   if (!dbConnected())
   {
      // try to connect
      
      tell(0, "Trying to re-connect to database!");
      retry++;
      
      if (initDb() != success)
      {
         tell(0, "Retry #%d failed, retrying in 60 seconds!", retry);
         exitDb();

         return fail;
      }
      
      retry = 0;         
      tell(0, "Connection established successfull!");
   }
   
   return success;
}

//***************************************************************************
// Download File
//***************************************************************************

int cEpgd::downloadFile(const char* url, int& size, MemoryStruct* data, int timeout, const char* userAgent)
{
   return ::downloadFile(url, size, data, timeout, userAgent);
}

//***************************************************************************
// Update
//***************************************************************************

int cEpgd::update()
{
   Statistic stat;

   memset(&stat, 0, sizeof(Statistic));

   tell(0, "EPG %s started", fullupdate ? "Full-Update" : fullreload ? "Reload" : "Update");

   // loop from today over configured range ..

   for (int day = 0; day < EPG2VDRConfig.days && !doShutDown(); day++)
   {
      processDay(day, fullupdate, &stat);

      if (!dbConnected())
         return fail;
   }

   fullupdate = no;
   double mb = (double)stat.bytes / 1024.0 / 1024.0;

   tell(0, "EPG Update finished, loaded %d files (%.3f %cB), %d non-updates "
           "skipped, %d rejected due to format error.", 
           stat.files, mb > 2 ? mb : (double)stat.bytes/1024.0, mb > 2 ? 'M' : 'K',
           stat.nonUpdates, stat.rejected);
  
   return success;
}

//***************************************************************************
// Transform Xml
//***************************************************************************

xmlDocPtr cEpgd::transformXml(const char* buffer, int size,
                              xsltStylesheetPtr stylesheet, 
                              const char* fileRef)
{
   xmlDocPtr doc, transformedDoc = 0;
   int readOptions = 0;

#if LIBXML_VERSION >= 20900
   readOptions |=  XML_PARSE_HUGE;
#endif

      if ((doc = xmlReadMemory(buffer, size, "tmp.xml.gz", 0, readOptions)))
   {
      if ((transformedDoc = xsltApplyStylesheet(stylesheet, doc, 0)) == 0)
         tell(1, "Error applying XSLT stylesheet");
      
      xmlFreeDoc(doc);
   }
   else
   {
      tell(2, "Error parsing XML File '%s'", fileRef);
   }
   
   return transformedDoc;
}

//***************************************************************************
// Parse XML Event
//***************************************************************************

int cEpgd::parseEvent(cDbRow* event, xmlNode* node)
{
   const char* name;
   char* content;
   char* images = 0;
   char* imagetype = 0;
   int imgCnt = 0;

   for (xmlNodePtr n = node->xmlChildrenNode; n; n = n->next)
   {
      if (n->type != XML_ELEMENT_NODE)
         continue;
      
      name = (const char*)n->name;
      content = (char*)xmlNodeGetContent(n);
      
      if (strcmp(name, "images") == 0)
         images = strdup(content);
      else if (strcmp(name, "imagetype") == 0)
         imagetype = strdup(content);
      else if (cDbService::FieldDef* f = cTableEvents::toField(name))
      {
         if (f->format == cDbService::ffAscii || f->format == cDbService::ffText)
            event->setValue(f->index, content);
         else
            event->setValue(f->index, (long)atoi(content));
      }
      else
         tell(1, "Ignoring unexpected element <%s>\n", name);

      xmlFree(content);
   }
   
   if (images && imagetype)
      imgCnt = storeImageRefs(event->getIntValue(cTableEvents::fiEventId),
                              event->getStrValue(cTableEvents::fiSource),
                              images, imagetype, 
                              event->getStrValue(cTableEvents::fiFileRef));

   else
      tell(4, "no images for event %ld in %s", 
           event->getIntValue(cTableEvents::fiEventId),
           event->getStrValue(cTableEvents::fiFileRef));

   event->setValue(cTableEvents::fiImageCount, imgCnt);

   free(images);
   free(imagetype);

   return success;
}

//***************************************************************************
// Store Images
//***************************************************************************

int cEpgd::storeImageRefs(long evtId, const char* source, const char* images, 
                          const char* ext, const char* fileRef)
{
   char* next;
   char* image;
   int lfn = 0;
   char* imagesCsv = strdup(images);
   int count = 0;

   for (char* p = imagesCsv; p && *p; p = next, lfn++)
   {
      if (next = strchr(p, ','))
      {
         *next = 0;     // terminate
         next++;
      }

      asprintf(&image, "%s.%s", p, ext);

      imageRefDb->clear();
      
      imageRefDb->setValue(cTableImageRefs::fiEventId, evtId);
      imageRefDb->setValue(cTableImageRefs::fiLfn, lfn);
      imageRefDb->setValue(cTableImageRefs::fiImgName, image);
      imageRefDb->setValue(cTableImageRefs::fiSource, source);
      imageRefDb->setValue(cTableImageRefs::fiFileRef, fileRef);
      
      imageRefDb->store();
      count++;

      free(image);
   }

   free(imagesCsv);

   tell(3, "There are %d images for event %ld", count, evtId);

   return count;
}

//***************************************************************************
// Get Pictures
//***************************************************************************

int cEpgd::getPictures()
{
   time_t start = time(0);
   int count = 0;
   int notFound = 0;
   int total = 0;
   unsigned int bytes = 0;
   MemoryStruct data;
   int rows = -1;

   if (!EPG2VDRConfig.getepgimages)
      return done;

   // fetch all images

   tell(0, "Start download of new images");

   char* where;
   asprintf(&where, "lfn < %d", EPG2VDRConfig.maximagesperevent);
   imageRefDb->countWhere(where, rows, "count(distinct imagename)");
   free(where);

   cDbStatement* stmt = new cDbStatement(imageRefDb);

   stmt->build("select ");
   stmt->bind(cTableImageRefs::fiImgName, cDBS::bndOut);
   stmt->bind(cTableImageRefs::fiSource, cDBS::bndOut,", ");
   stmt->bind(cTableImageRefs::fiFileRef, cDBS::bndOut, ", ");
   stmt->build(" from %s where ", imageRefDb->TableName());
   stmt->bindCmp(0, cTableImageRefs::fiLfn, 0, "<");
   stmt->build(" group by imagename");

   if (stmt->prepare() != success)
   {
      delete stmt;
      return fail;
   }

   imageRefDb->clear();
   imageRefDb->setValue(cTableImageRefs::fiLfn, EPG2VDRConfig.maximagesperevent);  // limit to config

   connection->startTransaction();

   for (int f = stmt->find(); f && !doShutDown(); f = stmt->fetch())
   {
      const char* imagename = imageRefDb->getStrValue(cTableImageRefs::fiImgName);

      if (!(++total % 500))
      {
         connection->commit();

         double mb = (double)bytes / 1024.0 / 1024.0;
         tell(0, "Still updating images, now %d of %d checked and %d loaded (%.3f %cB)", 
              total, rows, count, mb > 2 ? mb : (double)bytes/1024.0, mb > 2 ? 'M' : 'K');

         connection->startTransaction();
      }

      // get image if missing
      
      imageDb->clear();
      imageDb->setValue(cTableImages::fiImgName, imagename);

      int found = imageDb->find();
      
      if (!found || imageDb->isNull(cTableImages::fiImage))
      {
         int fileSize = getPicture(imageRefDb->getStrValue(cTableImageRefs::fiSource), imagename, 
                                   imageRefDb->getStrValue(cTableImageRefs::fiFileRef), &data);

         if (fileSize > 0)
         {
            int maxSize = imageDb->getField(cTableImages::fiImage)->size;

            bytes += fileSize;
            count++;
            
            tell(2, "Downloaded image '%s' with (%d) bytes", imagename, fileSize);
            
            if (fileSize < maxSize)
            {
               imageDb->setValue(cTableImages::fiImage, data.memory, data.size);
               imageDb->store();
            }
            else
            {
               tell(0, "Warning, skipping storage of image due to size "
                    "limit of %d byte, got image with %d bytes", maxSize, fileSize);
            }

            data.clear();
         }
         else
         {
            notFound++;
         }
      }

      imageDb->reset();
   } 

   stmt->freeResult();
   delete stmt;

   connection->commit();
   
   double mb = (double)bytes / 1024.0 / 1024.0;

   tell(0, "Loaded %d images (%.3f %cB), checked %d; %d failed to load in %ld seconds", 
        count, mb > 2 ? mb : (double)bytes/1024.0, mb > 2 ? 'M' : 'K', 
        total, notFound, time(0)-start);

   return dbConnected() ? success : fail;
}

//***************************************************************************
// Remove Old Files
//***************************************************************************

int cEpgd::cleanupEvents()
{
   char* where;
   struct tm tm;
   time_t historyFrom;

   tell(1, "Starting cleanup of events");
   
   // detete all fileref entrys older than 24 hours 

   historyFrom = time(0) - tmeSecondsPerDay;
   localtime_r(&historyFrom, &tm);
   
   asprintf(&where, "substr(name,1,8) <= '%4d%02d%02d'", tm.tm_year + 1900, tm.tm_mon+1, tm.tm_mday);
   tell(1, "Delete fileref [%s]", where);
   fileDb->deleteWhere(where);
   free(where);

//    // delete all external events with old fileref

//    asprintf(&where, "source <> 'vdr' and substr(fileref,1,8) <= '%4d%02d%02d'", 
//             tm.tm_year + 1900, tm.tm_mon+1, tm.tm_mday);
//    tell(1, "Delete events [%s]", where);
//    eventsDb->deleteWhere(where);
//    free(where);

//    // delete VDR events older than 6 hours 

//    asprintf(&where, "source = 'vdr' and starttime+duration < %ld", time(0) - 6 * tmeSecondsPerHour);
//    tell(1, "Delete events [%s]", where);
//    eventsDb->deleteWhere(where);
//    free(where);

   // delete events older than 6 hours 

   asprintf(&where, "starttime+duration < %ld", time(0) - 6 * tmeSecondsPerHour);
   tell(1, "Delete events [%s]", where);
   eventsDb->deleteWhere(where);
   free(where);

   // cleanup components 

   compDb->deleteWhere("eventid not in (select eventid from events where source = 'vdr');");
  
   tell(1, "Cleanup of events finished");

   return dbConnected() ? success : fail;
}

//***************************************************************************
// Remove Pictures
//***************************************************************************

int cEpgd::cleanupPictures()
{
   if (EPG2VDRConfig.getepgimages)
   {
      // remove unused images
      
      tell(1, "Starting cleanup of imagerefs");
      
      imageRefDb->deleteWhere("eventid not in (select eventid from events)");
      tell(1, "Starting cleanup of images");
      imageDb->deleteWhere("imagename not in (select imagename from imagerefs)");
      
      tell(1, "Image cleanup finished");
   }

   return dbConnected() ? success : fail;
}

//***************************************************************************
// Store to FS
//***************************************************************************

int cEpgd::storeToFs(MemoryStruct* data, const char* filename, const char* subPath)
{
   char* path = 0;
   char* outfile = 0;

   asprintf(&path, "%s/%s", EPG2VDRConfig.cachePath, subPath);
   chkDir(path);
   asprintf(&outfile, "%s/%s", path, filename);
   free(path);
   
   storeToFile(outfile, data->memory, data->size);
   
   free(outfile);

   return success;
}

//***************************************************************************
// Load from FS
//***************************************************************************

int cEpgd::loadFromFs(MemoryStruct* data, const char* filename, const char* subPath)
{
   char* path = 0;
   char* infile = 0;

   asprintf(&path, "%s/%s", EPG2VDRConfig.cachePath, subPath);
   chkDir(path);

   asprintf(&infile, "%s/%s", path, filename);
   free(path);

   loadFromFile(infile, data);

   free(infile);

   return success;
}

//***************************************************************************
// Stored Parameters
//***************************************************************************

int cEpgd::getParameter(const char* name, char* value)
{
   *value = 0;

   parameterDb->clear();
   parameterDb->setValue(cTableParameters::fiOwner, "epgd");
   parameterDb->setValue(cTableParameters::fiName, name);

   if (parameterDb->find())
      sprintf(value, "%s", parameterDb->getStrValue(cTableParameters::fiValue));

   parameterDb->reset();

   return success;
}

int cEpgd::getParameter(const char* name, long int& value, long int def)
{
   char txt[100];
   
   getParameter(name, txt);

   if (!isEmpty(txt))
      value = atol(txt);
   else if (isEmpty(txt) && def != na)
      value = def;
   else
      value = 0;

   return success;
}

int cEpgd::setParameter(const char* name, const char* value)
{
   tell(2, "Storing '%s' with value '%s' (%d)", name, value, sizeMd5);
   parameterDb->clear();
   parameterDb->setValue(cTableParameters::fiOwner, "epgd");
   parameterDb->setValue(cTableParameters::fiName, name);
   parameterDb->setValue(cTableParameters::fiValue, value);

   return parameterDb->store();
}

int cEpgd::setParameter(const char* name, long int value)
{
   char txt[16];

   snprintf(txt, sizeof(txt), "%ld", value);

   return setParameter(name, txt);
}

//***************************************************************************
// Plugin Interface
//***************************************************************************
//***************************************************************************
// Cleanup Before
//***************************************************************************

int cEpgd::cleanupBefore()
{      
   vector<PluginLoader*>::iterator it;
   
   for (it = plugins.begin(); it < plugins.end(); it++)
   {
      Plugin* p = (*it)->getPlugin();
      
      if (p->ready())
         p->cleanupBefore();
   }
   
   return dbConnected() ? success : fail;
}

//***************************************************************************
// Cleanup After
//***************************************************************************

int cEpgd::cleanupAfter()
{      
   vector<PluginLoader*>::iterator it;
   
   for (it = plugins.begin(); it < plugins.end(); it++)
   {
      Plugin* p = (*it)->getPlugin();
      
      if (p->ready())
         p->cleanupAfter();
   }
   
   return dbConnected() ? success : fail;
}

//***************************************************************************
// Get Picture
//***************************************************************************

int cEpgd::getPicture(const char* source, const char* imagename, 
                      const char* fileRef, MemoryStruct* data)
{
   vector<PluginLoader*>::iterator it;
   
   for (it = plugins.begin(); it < plugins.end(); it++)
   {
      Plugin* p = (*it)->getPlugin();
      
      if (p->ready() && p->hasSource(source))
         return p->getPicture(imagename, fileRef, data);
   }
   
   return 0;
}

//***************************************************************************
// Process Day
//***************************************************************************

int cEpgd::processDay(int day, int fullupdate, Statistic* stat)
{      
   vector<PluginLoader*>::iterator it;
   
   for (it = plugins.begin(); it < plugins.end(); it++)
   {
      Plugin* p = (*it)->getPlugin();
      
      if (p->ready())
      {
         tell(1, "Updating '%s' day today+%d now", p->getSource(), day);
         
         p->processDay(day, fullupdate, stat);
      }
   }
   
   return dbConnected() ? success : fail;
}

//***************************************************************************
// Init Scrapers
//***************************************************************************

int cEpgd::initScrapers()
{
   tvdbManager = new cTVDBManager();

   if (!tvdbManager->ConnectScraper()) 
   {
      delete tvdbManager;
      tvdbManager = 0;
      tell(0, "Error while connecting tvdb scraper");
      return fail;
   }

   if (!tvdbManager->ConnectDatabase(connection)) 
   {
      delete tvdbManager;
      tvdbManager = 0;
      tell(0, "Error while connecting to series database");
      return fail;
   }

   tell(0, "tvdb scraper connected");
   movieDbManager = new cMovieDBManager();

   if (!movieDbManager->ConnectScraper()) 
   {
      delete movieDbManager;
      movieDbManager = 0;
      tell(0, "Error while connecting movieDb scraper");
      return fail;
   }

   if (!movieDbManager->ConnectDatabase(connection)) 
   {
      delete movieDbManager;
      movieDbManager = 0;
      tell(0, "Error while connecting to movies database");
      return fail;
   }

   tell(0, "moviedb scraper connected");

   return success;
}

//***************************************************************************
// Exit Scrapers
//***************************************************************************

void cEpgd::exitScrapers() 
{
   if (tvdbManager) 
   {
      delete tvdbManager;
      tvdbManager = 0;
   }

   if (movieDbManager) 
   {
      delete movieDbManager;
      movieDbManager = 0;
   }
}

//***************************************************************************
// Scrap New Events
//***************************************************************************

int cEpgd::scrapNewEvents()
{
   tell(0, "Starting tvscraper scrap process");

   if (tvdbManager) 
   {
      tell(0, "Scraping new series and episodes");
      tvdbManager->SetServerTime();

      //update existing series with new data from thetvdb.com

      time_t start = time(0);
      tvdbManager->ResetBytesDownloaded();

      tvdbManager->UpdateSeries();    

      int bytes = tvdbManager->GetBytesDownloaded();
      double mb = (double)bytes / 1024.0 / 1024.0;
      
      tell(0, "Update of series and episodes done in %ld s, downloaded %.3f %cB", time(0) - start, mb > 2 ? mb : (double)bytes/1024.0, mb > 2 ? 'M' : 'K');
      
      //scrap new series in EPG

      vector<sSeriesResult> seriesToScrap;

      if (!tvdbManager->GetSeriesWithEpisodesFromEPG(&seriesToScrap))
         return fail;

      start = time(0);
      tvdbManager->ResetBytesDownloaded();
      int seriesTotal = seriesToScrap.size();
      tell(0, "%d new series events to scrap in db", seriesTotal);
      int seriesCur = 1;

      for (vector<sSeriesResult>::iterator it = seriesToScrap.begin() ; it != seriesToScrap.end(); ++it) 
      {
         if (seriesCur%10 == 0)
            tell(0, "series episode %d / %d scraped...continuing scraping", seriesCur, seriesTotal);
         
         tvdbManager->ProcessSeries(*it);
         seriesCur++;

         if (doShutDown()) 
         {
            tell(0, "%d series episodes scraped in %ld s", seriesCur, time(0) - start);
            return success;
         }
      }

      bytes = tvdbManager->GetBytesDownloaded();
      mb = (double)bytes / 1024.0 / 1024.0;

      tell(0, "%d series episodes scraped in %ld s, downloaded %.3f %cB", seriesTotal, time(0) - start, mb > 2 ? mb : (double)bytes/1024.0, mb > 2 ? 'M' : 'K');
   }

   if (movieDbManager && !doShutDown()) 
   {
      time_t start = time(0);
      movieDbManager->ResetBytesDownloaded();
      tell(0, "Scraping new movies");
      vector<sMovieResult> moviesToScrap;

      if (!movieDbManager->GetMoviesFromEPG(&moviesToScrap))
         return fail;

      int moviesTotal = moviesToScrap.size();
      int movieCur = 1;
      tell(0, "%d new movies to scrap in db", moviesTotal);

      for (vector<sMovieResult>::iterator it = moviesToScrap.begin() ; it != moviesToScrap.end(); ++it) 
      {
         if (movieCur%10 == 0)
            tell(0, "movie %d / %d scraped...continuing scraping", movieCur, moviesTotal);
         
         movieDbManager->ProcessMovie(*it);
         movieCur++;

         if (doShutDown()) 
         {
            tell(0, "%d movies scraped in %ld s", movieCur, time(0) - start);
            return success;
         }
      }

      int bytes = movieDbManager->GetBytesDownloaded();
      double mb = (double)bytes / 1024.0 / 1024.0;

      tell(0, "%d movies scraped in %ld s, downloaded %.3f %cB", moviesTotal, time(0) - start, mb > 2 ? mb : (double)bytes/1024.0, mb > 2 ? 'M' : 'K');
   }

   return success;
}

//***************************************************************************
// Cleanup Series and Movies
//***************************************************************************

int cEpgd::cleanupSeriesAndMovies()
{
   if (tvdbManager) 
   {
      tell(0, "cleaning up series...");
      int numDeleted = tvdbManager->CleanupSeries();
      tell(0, "%d outdated series deleted", numDeleted);
   }

   if (movieDbManager) 
   {
      tell(0, "cleaning up movies...");
      int numDeleted = movieDbManager->CleanupMovies();
      tell(0, "%d outdated movies deleted", numDeleted);
   }

   return success;
}

//***************************************************************************
// Check new Recordings
//***************************************************************************

void cEpgd::scrapNewRecordings(int count)
{
   recordingsDb->clear();
   recordingsDb->setValue(cTableRecordings::fiScrapNew, 1);

   tell(1, "scraper: scrap new recordings %d pending", count);

   connection->startTransaction();

   for (int res = selectNewRecordings->find(); res; res = selectNewRecordings->fetch()) 
   {
      int seriesId = 0;
      int episodeId = 0;
      int movieId = 0;
      string uuid =  recordingsDb->getStrValue(cTableRecordings::fiUuid);
      string recPath = recordingsDb->getStrValue(cTableRecordings::fiRecPath);
      int recStart = recordingsDb->getIntValue(cTableRecordings::fiRecStart);
      string recTitle = recordingsDb->getStrValue(cTableRecordings::fiRecTitle);
      string recSubtitle = recordingsDb->getStrValue(cTableRecordings::fiRecSubTitle);
      int eventId = recordingsDb->getIntValue(cTableRecordings::fiEventId);
      string channelId = recordingsDb->getStrValue(cTableRecordings::fiChannelId);
      int scrapInfoMovieId = recordingsDb->getIntValue(cTableRecordings::fiScrapInfoMovieId);
      int scrapInfoSeriesId = recordingsDb->getIntValue(cTableRecordings::fiScrapInfoSeriesId);
      int scrapInfoEpisodeId = recordingsDb->getIntValue(cTableRecordings::fiScrapInfoEpisodeId);
      int recDuration = recordingsDb->getIntValue(cTableRecordings::fiRecDuration);
      bool found = false;

      tell(0, "-------------------------------------------------------");
      tell(0, "found new recording \"%s\"", recTitle.c_str());

      /**************************************************************************************
      * first check if current event with scrap info is available
      **************************************************************************************/

      if (eventId > 0 && channelId.size() > 0) 
         found = checkEventsForRecording(eventId, channelId, seriesId, episodeId, movieId);

      if (found) 
      {
         tell(0, "found active event for recording \"%s\"", recTitle.c_str());
         recordingsDb->setValue(cTableRecordings::fiSeriesId, seriesId);
         recordingsDb->setValue(cTableRecordings::fiEpisodeId, episodeId);
         recordingsDb->setValue(cTableRecordings::fiMovieId, movieId);
         recordingsDb->setValue(cTableRecordings::fiScrapNew, 0);
         recordingsDb->setValue(cTableRecordings::fiScrSp, time(0));
         recordingsDb->update();
         continue;
      }

      /**************************************************************************************
      * maybe another client got the event earlier...
      **************************************************************************************/      

      found = checkRecOtherClients(uuid, recPath, recStart);

      // primary key was changed through function call

      recordingsDb->setValue(cTableRecordings::fiUuid, uuid.c_str());
      recordingsDb->setValue(cTableRecordings::fiRecPath, recPath.c_str());
      recordingsDb->setValue(cTableRecordings::fiRecStart, recStart);

      if (found) 
      {
         tell(0, "found same recording from other client for recording \"%s\"", recTitle.c_str());
         recordingsDb->setValue(cTableRecordings::fiScrapNew, 0);
         recordingsDb->setValue(cTableRecordings::fiScrSp, time(0));
         recordingsDb->update();
         continue;
      }

      /**************************************************************************************
      * now check scrapInfo if available
      **************************************************************************************/

      if (scrapInfoMovieId > 0) 
      {
         found = movieDbManager->CheckScrapInfoDB(scrapInfoMovieId);

         if (!found)
            found = movieDbManager->CheckScrapInfoOnline(scrapInfoMovieId);
      } 
      else if (scrapInfoSeriesId > 0) 
      {
         found = tvdbManager->CheckScrapInfoDB(scrapInfoSeriesId, scrapInfoEpisodeId);

         if (!found) 
            found = tvdbManager->CheckScrapInfoOnline(scrapInfoSeriesId, scrapInfoEpisodeId);
      }

      if (found) 
      {
         tell(0, "scrapinfo successfully checked for recording \"%s\"", recTitle.c_str());
         recordingsDb->setValue(cTableRecordings::fiScrapNew, 0);
         recordingsDb->setValue(cTableRecordings::fiSeriesId, scrapInfoSeriesId);
         recordingsDb->setValue(cTableRecordings::fiEpisodeId, scrapInfoEpisodeId);
         recordingsDb->setValue(cTableRecordings::fiMovieId, scrapInfoMovieId);
         recordingsDb->setValue(cTableRecordings::fiScrSp, time(0));
         recordingsDb->update();
         continue;
      }

      /**************************************************************************************
      * now the last chance...scrap with recording title and subtitle
      * series < 70 min, movies >= 70 min
      **************************************************************************************/

      seriesId = 0;
      episodeId = 0;
      movieId = 0;         

      tell(0, "searching \"%s\" as series in db", recTitle.c_str());
      found = tvdbManager->SearchRecordingDB(recTitle, recSubtitle, seriesId, episodeId);

      if (!found)
      {
         tell(0, "searching \"%s\" as movie in db", recTitle.c_str());
         found = movieDbManager->SearchRecordingDB(recTitle, movieId);
      }

      if (found)
         tell(0, "found \"%s\" in Database", recTitle.c_str());

      if (!found && recDuration < 70) 
      {
         tell(0, "nothing found in db, searching \"%s\" as series online", recTitle.c_str());
         found = tvdbManager->SearchRecordingOnline(recTitle, recSubtitle, seriesId, episodeId);

         if (found)
            tell(0, "found \"%s\" as series online, seriesId %d, episodeId %d", recTitle.c_str(), seriesId, episodeId);
      } 
      else if (!found)
      {

         tell(0, "nothing found in db, searching \"%s\" as movie online", recTitle.c_str());
         found = movieDbManager->SearchRecordingOnline(recTitle, movieId);

         if (found)
            tell(0, "found \"%s\" as movie online, movieId %d", recTitle.c_str(), movieId);
      }

      if (found) 
      {
         tell(0, "successfully scraped recording \"%s\"", recTitle.c_str());
         recordingsDb->setValue(cTableRecordings::fiScrapNew, 0);
         recordingsDb->setValue(cTableRecordings::fiSeriesId, seriesId);
         recordingsDb->setValue(cTableRecordings::fiEpisodeId, episodeId);
         recordingsDb->setValue(cTableRecordings::fiMovieId, movieId);
         recordingsDb->setValue(cTableRecordings::fiScrSp, time(0));
         recordingsDb->update();
         continue;
      }

      tell(0, "recording %s NOT successfully scraped", recTitle.c_str());
      recordingsDb->setValue(cTableRecordings::fiScrapNew, 0);
      recordingsDb->setValue(cTableRecordings::fiSeriesId, 0);
      recordingsDb->setValue(cTableRecordings::fiEpisodeId, 0);
      recordingsDb->setValue(cTableRecordings::fiMovieId, 0);
      recordingsDb->setValue(cTableRecordings::fiScrSp, time(0));
      recordingsDb->update();
   }

   connection->commit();

   tell(1, "scraper: scrap new recordings done");
}

bool cEpgd::checkEventsForRecording(int eventId, string channelId, int &seriesId, int &episodeId, int &movieId)
{
   eventsDb->clear();
   eventsDb->setValue(cTableEvents::fiMasterId, eventId);
   eventsDb->setValue(cTableEvents::fiChannelId, channelId.c_str());
   int found = selectRecordingEvent->find();

   if (!found)
      return false;

   seriesId = eventsDb->getIntValue(cTableEvents::fiScrSeriesId);
   episodeId = eventsDb->getIntValue(cTableEvents::fiScrSeriesEpisode);
   movieId = eventsDb->getIntValue(cTableEvents::fiScrMovieId);

   if (seriesId > 0 || movieId > 0)
      return true;

   return false;
}

bool cEpgd::checkRecOtherClients(string uuid, string recPath, int recStart)
{
   // take only last two directorys from path for comparison

   size_t firstSlash = recPath.find_last_of('/');

   if (firstSlash == string::npos)
      return false;

   size_t secondSlash = recPath.find_last_of('/', firstSlash-1);

   if (secondSlash == string::npos)
      return false;

   string compPath = recPath.substr(secondSlash);
   compPath = '%' + compPath;
   recordingsDb->setValue(cTableRecordings::fiRecPath, compPath.c_str());
   recordingsDb->setValue(cTableRecordings::fiUuid, uuid.c_str());
   recordingsDb->setValue(cTableRecordings::fiRecStart, recStart);
   int found = selectRecOtherClient->find();

   if (!found)
      return false;

   int seriesId = 0;
   int movieId = 0;
   seriesId = recordingsDb->getIntValue(cTableRecordings::fiSeriesId);
   movieId = recordingsDb->getIntValue(cTableRecordings::fiMovieId);

   if (seriesId > 0 || movieId > 0)
      return true;

   return false;
}

//***************************************************************************
// Trigger VDRs
//***************************************************************************
/*
int cEpgd::triggerVdrs(const char* trg)
{
   const char* plgs[] = { "epg2vdr", 0 };

   vdrDb->clear();

   for (int f = selectActiveVdrs->find(); f; f = selectActiveVdrs->fetch())
   {
      const char* ip = vdrDb->getStrValue(cTableVdrs::fiIp);
      unsigned int port = vdrDb->getIntValue(cTableVdrs::fiSvdrp);

      cSvdrpClient cl(ip, port);
         
      // open tcp connection
      
      if (cl.open() == success)
      {
         for (int i = 0; plgs[i]; i++)
         {
            char* command = 0;
            cList<cLine> result;
            
            asprintf(&command, "PLUG %s TRIGGER %s", plgs[i], trg);

            tell(0, "Send '%s' to '%s' at '%s:%d'", 
                 command, plgs[i], ip, port);
            
            if (!cl.send(command))
               tell(0, "Error: Send '%s' to '%s' at '%s:%d' failed!", 
                    command, plgs[i], ip, port);
            else
               cl.receive(&result);

            free(command);
         }

         cl.close(no);
      }
   }

   selectActiveVdrs->freeResult();

   return success;
}
*/
