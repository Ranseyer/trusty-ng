��          �      �        3   	  J   =  p   �     �  Q     5   g  &   �     �     �     �  !         "     6     V  J   g  .   �  -   �  F     >   V  1   �  &   �  !   �  �    _   �  �     �   �  K   �	  w   "
  d   �
  �   �
  5   �  C   �  m   �  >   k  V   �  D     7   F  �   ~  �   '  z   �  �   $  �   �  �   �  �   I  m   �                                                           	              
                               ${OS_TO_DELETE_NAME} has been successfully removed. (the Linux distribution installed from this Windows via Wubi will be lost) (the Linux distribution installed into this Windows via Wubi on ${WUBI_TO_DELETE_PARTITION} will also be erased) Boot successfully repaired. Do you really want to uninstall ${OS_TO_DELETE_NAME} (${OS_TO_DELETE_PARTITION})? Format the partition ${OS_TO_DELETE_PARTITION} into : No OS has been found on this computer. OS-Uninstaller Operation aborted Please wait few seconds... Removing ${OS_TO_DELETE_NAME} ... Repair file systems Repair the boot of the computer Scanning systems See https://wiki.ubuntu.com/WubiGuide#Uninstallation for more information. The ${THISPARTITION} partition is nearly full. The ${THISPARTITION} partition is still full. To finish the removal, please do not forget to update your bootloader! We hope you enjoyed it and look forward to read your feedback. Which operating system do you want to uninstall ? Wubi must be uninstalled from Windows. You can now reboot your computer. Project-Id-Version: boot-repair
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-08 01:03+0100
PO-Revision-Date: 2013-05-06 04:54+0000
Last-Translator: Praveen <prvnpa4@gmail.com>
Language-Team: Tamil <ubuntu-tam@lists.ubuntu.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-12-23 14:41+0000
X-Generator: Launchpad (build 16877)
Language: ta
 ${OS_TO_DELETE_NAME} வெற்றிகரமாக நீக்கப்பட்டது. (இந்த விண்டோஸ் இலிருந்து வூபி வழியாக நிறுவிய லீனக்ஸ் வினியோகமும் நீக்கப்படும்) (வூபி வழியாக இந்த விண்டோஸ் உள் ${WUBI_TO_DELETE_PARTITION} இல் நிறுவிய லீனக்ஸ் வினியோகமும் நீக்கப்படும்) துவக்கம்  சரி செய்யப்பட்டது ${OS_TO_DELETE_NAME} (${OS_TO_DELETE_PARTITION}) ஐ நிச்சயம் நீக்க வேண்டுமா? ${OS_TO_DELETE_PARTITION}  ஐ இப்படி ஒழுங்கு செய்யவும்: இந்த கணினியில் எந்த இயங்குதளமும் காணப்படவில்லை. ஓஎஸ் -நிறுவல்நீக்கி செயல்பாடு கைவிடப்பட்டது தயை செய்து சில நொடிகள் காத்திருக்கவும்... ${OS_TO_DELETE_NAME} ஐ நீக்குகிறது ... கோப்புத் தொகுதிகளைத் திருத்துக கணினி ஏற்றி பழுதுநீக்கம் கணினிகளை வருடுகிறது தயை செய்து பார்க்கவும்: https://wiki.ubuntu.com/WubiGuide#Uninstallation மேலும் தகவல்களுக்கு பாகம்  ${THISPARTITION} ஆனது தற்போது ஓரளவு நிரம்பி விட்டது. பாகம்  ${THISPARTITION} ஆனது இப்போதும் நிரம்பியே உள்ளது. நீக்கலை முடிக்க தயை செய்து உங்கள் பூட் துவக்கியை மேம்படுத்த மறவாதீர்! இதை நீங்கள் ரசித்தீர்கள் என நினைக்கிறோம். உங்கள் விமர்சனங்களை மின்னஞ்சல் மூலம். நீங்கள் எந்த இயங்குதளத்தை நீக்க விரும்புகிறீர்கள்? விண்டோஸ் இலிருந்து வுபி நிறுவல்நீக்கப்பட வேண்டும். நீங்கள் இப்போது கணனியை மீளத் தொடக்கலாம் 