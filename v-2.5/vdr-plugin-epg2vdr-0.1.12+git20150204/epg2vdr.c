/*
 * epg2vdr.c: EPG2VDR plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#include "lib/config.h"

#include "update.h"

#if defined (APIVERSNUM) && (APIVERSNUM < 10600)
# error VDR API versions < 1.6.0 are not supported !
#endif

cUpdate* oUpdate;
const char* logPrefix = LOG_PREFIX;

//***************************************************************************
// Plugin Main Menu
//***************************************************************************

class cEpgPluginMenu : public cOsdMenu
{
   public:

      cEpgPluginMenu(const char* title, cPluginEPG2VDR* aPlugin);
      virtual ~cEpgPluginMenu() { };
      
      virtual eOSState ProcessKey(eKeys key);

   protected:

      cPluginEPG2VDR* plugin;
};

cEpgPluginMenu::cEpgPluginMenu(const char* title, cPluginEPG2VDR* aPlugin)
   : cOsdMenu(title)
{
   plugin = aPlugin;

   Clear();

   cOsdMenu::Add(new cOsdItem(tr("Reload")));
   cOsdMenu::Add(new cOsdItem(tr("Update")));

   SetHelp(0, 0, 0,0);

   Display();
}

//***************************************************************************
// Process Key
//***************************************************************************

eOSState cEpgPluginMenu::ProcessKey(eKeys key)
{
   eOSState state = cOsdMenu::ProcessKey(key);

   if (state != osUnknown)
      return state;

   switch (key)
   {
      case kOk:
      {
         if (Current() == 0)
         {
            Skins.Message(mtInfo, tr("Reload EPG"));
            oUpdate->triggerUpdate(true);
         }
         
         else if (Current() == 1)
         {
            Skins.Message(mtInfo, tr("Update EPG"));
            oUpdate->triggerUpdate();
         }

         return osEnd;
      }

      default:
         break;
   }

   return state;
}

//***************************************************************************
// Plugin Setup Menu
//***************************************************************************

class cMenuSetupEPG2VDR : public cMenuSetupPage 
{
   private:

      cEPG2VDRConfig data; 
      virtual void Setup();

   protected:

      virtual eOSState ProcessKey(eKeys Key);
      virtual void Store();

   public:

      cMenuSetupEPG2VDR();
};

cMenuSetupEPG2VDR::cMenuSetupEPG2VDR() 
{
  data = EPG2VDRConfig;

  Setup();
}

void cMenuSetupEPG2VDR::Setup()
{
   static const char* masterModes[] =
   {
      "auto",
      "yes",
      "no",

      0
   };

   char* buf;
   int current = Current();
   
   Clear();

   asprintf(&buf, "-------------------- %s ----------------------------------", tr("EPG Update"));
   Add(new cOsdItem(buf));
   free(buf);
   cList<cOsdItem>::Last()->SetSelectable(false);

   Add(new cMenuEditBoolItem(tr("Show In Main Menu"), &data.mainmenuVisible));
   cOsdMenu::Add(new cMenuEditStraItem(tr("Update DVB EPG Database"), (int*)&data.masterMode, cUpdate::mmCount, masterModes));
   Add(new cMenuEditBoolItem(tr("Load Images"), &data.getepgimages));
   Add(new cMenuEditBoolItem(tr("Prohibit Shutdown On Busy 'epgd'"), &data.activeOnEpgd));
   Add(new cMenuEditBoolItem(tr("Schedule Boot For Update"), &data.scheduleBoot));

   asprintf(&buf, "--------------------- %s ---------------------------------", tr("MySQL"));   
   Add(new cOsdItem(buf));
   free(buf);
   cList<cOsdItem>::Last()->SetSelectable(false);
   Add(new cMenuEditStrItem(tr("Host"), data.dbHost, sizeof(data.dbHost), tr(FileNameChars)));
   Add(new cMenuEditIntItem(tr("Port"), &data.dbPort, 1, 99999));
   Add(new cMenuEditStrItem(tr("Database Name"), data.dbName, sizeof(data.dbName), tr(FileNameChars)));
   Add(new cMenuEditStrItem(tr("User"), data.dbUser, sizeof(data.dbUser), tr(FileNameChars)));
   Add(new cMenuEditStrItem(tr("Password"), data.dbPass, sizeof(data.dbPass), tr(FileNameChars)));

   asprintf(&buf, "--------------------- %s ---------------------------------", tr("NO EPG"));   
   Add(new cOsdItem(buf));
   free(buf);
   Add(new cMenuEditBoolItem(tr("Blacklist not configured Channels"), &data.blacklist));

   asprintf(&buf, "--------------------- %s ---------------------------------", tr("Technical Stuff"));
   Add(new cOsdItem(buf));
   free(buf);
   cList<cOsdItem>::Last()->SetSelectable(false);
   Add(new cMenuEditIntItem(tr("Log level"), &data.loglevel, 0, 4));

   SetCurrent(Get(current));
   Display();
}

eOSState cMenuSetupEPG2VDR::ProcessKey(eKeys Key) 
{
   eOSState state = cMenuSetupPage::ProcessKey(Key);
   
   switch (state) 
   {
      case osContinue:
      {
         if (NORMALKEY(Key) == kUp || NORMALKEY(Key) == kDown) 
         {
            cOsdItem* item = Get(Current());

            if (item)
               item->ProcessKey(kNone);
         }

         break;
      }
         
      default: break;
   }

   return state;
}

void cMenuSetupEPG2VDR::Store(void)
{
   // int updatetime = EPG2VDRConfig.updatetime;

   EPG2VDRConfig = data;

   SetupStore("LogLevel", EPG2VDRConfig.loglevel);
   SetupStore("ShowInMainMenu", EPG2VDRConfig.mainmenuVisible);
   SetupStore("Blacklist", EPG2VDRConfig.blacklist);
   SetupStore("DbHost", EPG2VDRConfig.dbHost);
   SetupStore("DbPort", EPG2VDRConfig.dbPort);
   SetupStore("DbName", EPG2VDRConfig.dbName);
   SetupStore("DbUser", EPG2VDRConfig.dbUser);
   SetupStore("DbPass", EPG2VDRConfig.dbPass);
   SetupStore("MasterMode", EPG2VDRConfig.masterMode);
   SetupStore("LoadImages", EPG2VDRConfig.getepgimages);
   SetupStore("ActiveOnEpgd", EPG2VDRConfig.activeOnEpgd);
   SetupStore("ScheduleBoot", EPG2VDRConfig.scheduleBoot);
}

//***************************************************************************
// Plugin - EPG2VDR
//***************************************************************************

cPluginEPG2VDR::cPluginEPG2VDR(void)
{
   oUpdate = 0;

   cDbConnection::init();
}

cPluginEPG2VDR::~cPluginEPG2VDR()
{
   delete oUpdate;

   cDbConnection::exit();
}

void cPluginEPG2VDR::DisplayMessage(const char *s) 
{
   tell(0, "%s", s);

   Skins.Message(mtInfo, tr(s));
   sleep(Setup.OSDMessageTime);
}

const char *cPluginEPG2VDR::CommandLineHelp(void)
{
   return 0;
}

const char **cPluginEPG2VDR::SVDRPHelpPages(void) 
{
   static const char *HelpPages[] = 
   {
      "UPDATE\n"
      "    Load new/changed events database.",
      "RELOAD\n"
      "    Reload all events from database to EPG",
      0
   };

   return HelpPages;
}

cString cPluginEPG2VDR::SVDRPCommand(const char *Cmd, const char *Option, int &ReplyCode) 
{
   if (strcasecmp(Cmd, "UPDATE") == 0) 
   {
      oUpdate->triggerUpdate();
      return "EPG2VDR update started.";
   }
   
   else if (strcasecmp(Cmd, "RELOAD") == 0) 
   {
      oUpdate->triggerUpdate(true);
      return "EPG2VDR full reload of events from database.";
   }
   
   return 0;
}

//***************************************************************************
// Initialize
//***************************************************************************

bool cPluginEPG2VDR::Initialize()
{
   return true;
}

//***************************************************************************
// Start
//***************************************************************************

bool cPluginEPG2VDR::Start()
{
   oUpdate = new cUpdate(this);
   oUpdate->Start();   // start thread
   
   return true;
}

cString cPluginEPG2VDR::Active()
{
//   time_t timeoutAt = time(0) + 10;

   if (EPG2VDRConfig.activeOnEpgd && oUpdate->isEpgdUpdating())
      return tr("EPG2VDR Waiting on epgd");
   
//    while (oUpdate->isUpdateActive())
//    {
//       tell(0, "EPG2VDR EPG update running, wating up to 10 seconds ..");

//       if (time(0) > timeoutAt)
//       {
//          tell(0, "EPG2VDR EPG update running, shutdown timed out, aborting");
//          return 0;
//       }
      
//       usleep(500000);
//    }
   
   return 0;
}

time_t cPluginEPG2VDR::WakeupTime()
{
   // return custom wakeup time for shutdown script

   if (EPG2VDRConfig.scheduleBoot && oUpdate->getNextEpgdUpdateAt())
      return oUpdate->getNextEpgdUpdateAt();

   return 0;
}

cOsdObject* cPluginEPG2VDR::MainMenuAction()
{
   return new cEpgPluginMenu(MAINMENUENTRY, this);
}

cMenuSetupPage* cPluginEPG2VDR::SetupMenu(void)
{
  return new cMenuSetupEPG2VDR;
}

bool cPluginEPG2VDR::SetupParse(const char *Name, const char *Value)
{
  // Parse your own setup parameters and store their values.

  if      (!strcasecmp(Name, "LogLevel"))        EPG2VDRConfig.loglevel = atoi(Value);
  else if (!strcasecmp(Name, "ShowInMainMenu"))  EPG2VDRConfig.mainmenuVisible = atoi(Value);
  else if (!strcasecmp(Name, "Blacklist"))       EPG2VDRConfig.blacklist = atoi(Value);
  else if (!strcasecmp(Name, "DbHost"))          sstrcpy(EPG2VDRConfig.dbHost, Value, sizeof(EPG2VDRConfig.dbHost));
  else if (!strcasecmp(Name, "DbPort"))          EPG2VDRConfig.dbPort = atoi(Value);
  else if (!strcasecmp(Name, "DbName"))          sstrcpy(EPG2VDRConfig.dbName, Value, sizeof(EPG2VDRConfig.dbName));
  else if (!strcasecmp(Name, "DbUser"))          sstrcpy(EPG2VDRConfig.dbUser, Value, sizeof(EPG2VDRConfig.dbUser));
  else if (!strcasecmp(Name, "DbPass"))          sstrcpy(EPG2VDRConfig.dbPass, Value, sizeof(EPG2VDRConfig.dbPass));
  else if (!strcasecmp(Name, "MasterMode"))      EPG2VDRConfig.masterMode = atoi(Value);
  else if (!strcasecmp(Name, "LoadImages"))      EPG2VDRConfig.getepgimages = atoi(Value);
  else if (!strcasecmp(Name, "ActiveOnEpgd"))    EPG2VDRConfig.activeOnEpgd = atoi(Value);
  else if (!strcasecmp(Name, "ScheduleBoot"))    EPG2VDRConfig.scheduleBoot = atoi(Value);
  else if (!strcasecmp(Name, "Uuid"))            sstrcpy(EPG2VDRConfig.uuid, Value, sizeof(EPG2VDRConfig.uuid));

  else
     return false;

  return true;
}

void cPluginEPG2VDR::Stop()
{
   oUpdate->Stop();
}

//***************************************************************************

VDRPLUGINCREATOR(cPluginEPG2VDR);
