/*
 * update.c
 *
 * See the README file for copyright information 
 *
 */

#include "epgd.h"

//***************************************************************************
// Evaluate
//***************************************************************************

int cEpgd::evaluateEpisodes()
{
   int ec = 0, pp = 0, plv = 0;  // statistics
   long lStart = time(0);
   int updated = 0;

   if (!EPG2VDRConfig.seriesEnabled)
      return done;

   tell(1, "Starting episode lookup ...");

   // first read all events into list ..

   episodeDb->getConnection()->startTransaction();

   // loop over all episodes ..

   for (int f = selectDistCompname->find(); f && !doShutDown(); f = selectDistCompname->fetch())
   {
      // const int maxTitleDist = (((double)strlen(episodeCompName)) / 100.0 * 20.0);
      char* episodeCompName = strdup(episodeDb->getStrValue(cTableEpisodes::fiCompName));
      ec++;

      eventsDb->clear();
      eventsDb->setValue(cTableEvents::fiCompTitle, episodeCompName);
      
      // loop over all events matching this episodes 1:1
      
      for (int f = selectByCompTitle->find(); f; f = selectByCompTitle->fetch())
      {
         if (strlen(eventsDb->getStrValue(cTableEvents::fiCompShortText)) == 0)
            continue;

         const char* evtCompShortText = eventsDb->getStrValue(cTableEvents::fiCompShortText);

         eventsDb->setValue(cTableEvents::fiEpisode, episodeCompName);

         episodeDb->clear();
         episodeDb->setValue(cTableEpisodes::fiCompName, episodeCompName);
         episodeDb->setValue(cTableEpisodes::fiCompPartName, evtCompShortText);

         // search episode part 1:1

         if (selectByCompNames->find())
         {
            pp++;

            if (!eventsDb->hasValue(cTableEvents::fiEpisodePart, evtCompShortText)
                || !eventsDb->hasValue(cTableEvents::fiEpisodeLang, episodeDb->getStrValue(cTableEpisodes::fiLang)))
            {
               // store reference
               
               eventsDb->setValue(cTableEvents::fiUpdSp, time(0));
               eventsDb->setValue(cTableEvents::fiEpisodePart, evtCompShortText);
               eventsDb->setValue(cTableEvents::fiEpisodeLang, episodeDb->getStrValue(cTableEpisodes::fiLang));
               updateEpisodeAtEvents->execute();
               updated++;
            }
         }

         else  // if not found try via lv
         {
            const int maxDist = (((double)strlen(evtCompShortText)) / 100.0 * 20.0);
            int d;
            int dMin = maxDist+1;
            int tmp;
            char* bestCompPart = 0;
            char* bestCompPartLang = 0;

            for (int f = selectByCompName->find(); f; f = selectByCompName->fetch())
            {
               if (::abs(strlen(evtCompShortText) - strlen(episodeDb->getStrValue(cTableEpisodes::fiCompPartName))) >= dMin)
                  continue;

               if ((d = lvDistance(evtCompShortText, episodeDb->getStrValue(cTableEpisodes::fiCompPartName), 20, tmp)) < dMin)
               {
                  free(bestCompPart);
                  free(bestCompPartLang);
                  bestCompPart = strdup(episodeDb->getStrValue(cTableEpisodes::fiCompPartName));
                  bestCompPartLang = strdup(episodeDb->getStrValue(cTableEpisodes::fiLang));
                  dMin = d;
               }
            }
            
            if (bestCompPart)
            {
               plv++;
               
               if (!eventsDb->hasValue(cTableEvents::fiEpisodePart, bestCompPart)
                   || !eventsDb->hasValue(cTableEvents::fiEpisodeLang, bestCompPartLang))
               {
                  // store reference
                  
                  eventsDb->setValue(cTableEvents::fiUpdSp, time(0));
                  eventsDb->setValue(cTableEvents::fiEpisodePart, bestCompPart);
                  eventsDb->setValue(cTableEvents::fiEpisodeLang, bestCompPartLang);
                  updateEpisodeAtEvents->execute();
                  updated++;
               }
            }

            free(bestCompPart);     bestCompPart = 0;
            free(bestCompPartLang); bestCompPartLang = 0;
            
            selectByCompName->freeResult();
         }

         selectByCompNames->freeResult();
      }

      selectByCompTitle->freeResult();

      free(episodeCompName);
   }

   selectDistCompname->freeResult();
   episodeDb->getConnection()->commit();

   tell(1, "Lookup done for "
        "%d series, matched %d parts by compare and %d parts by lv in %ld seconds; Updated %d",
        ec, pp, plv, time(0)-lStart, updated);

   return success;
}

//***************************************************************************
// Download Episodes and store to filesystem
//***************************************************************************

int cEpgd::downloadEpisodes()
{
   if (!EPG2VDRConfig.seriesEnabled)
      return done;

   cSvdrpClient cl(EPG2VDRConfig.seriesUrl, EPG2VDRConfig.seriesPort);
   string fileName;
   string linkName;
   int isLink = 0;
   cEpisodeFiles files;
   int code;
   int abort = 0;
   char command[200];

   tell(0, "Starting episode download ...");

   int minutes = na;

   if (selectMaxUpdSp->find() && episodeDb->getIntValue(cTableEpisodes::fiUpdSp) > 0)
      minutes = (time(0) - episodeDb->getIntValue(cTableEpisodes::fiUpdSp)) / 60;

   selectMaxUpdSp->freeResult();

   if (!minutes && !fullupdate)
   {
      tell(0, "Nothing to be done, all episodes are up-to-date");
      return done;
   }

   // open tcp connection

   if (cl.open() != 0)
   {
      tell(0, "Open connection to '%s' failed, aborting transfer!", EPG2VDRConfig.seriesUrl);
      return fail;
   }

   // select characterset

   if (!cl.send(withutf8 ? "CHARSET utf-8": "CHARSET iso-8859-1"))
   {
      tell(0, "Send '%s' failed, aborting transfer!", command);
      cl.close();
      
      return fail;
   }

   // check for characterset confirmation
   
   cList<cLine> csconf;
   
   if (cl.receive(&csconf) != 225) 
   {
      tell(0, "SVDRPCL: did not receive charset confirmation. Closing...");
      cl.abort();
      
      return fail;
   }
   
   if (csconf.First() && csconf.First()->Text())
      tell(0, "Got '%s'", csconf.First()->Text());

   // identify myself

   sprintf(command, "HELLO %s v%s (%s), ID=%s", TARGET, VERSION, VERSION_DATE, EPG2VDRConfig.uuid);
   cl.send(command);
   cl.receive();

   // build GET command for the files

   *command = 0;

   if (fullupdate || minutes == na)
   {
      tell(1, "Requesting all episodes due to '%s'", minutes != na ? "fullupdate" : "empty table");
      sprintf(command, "GET all");

      // truncate table!

      episodeDb->truncate();
   }

   else if (minutes > 0)
   {
      minutes += 5;      // request 5 minutes more to compensate time diffs to constabel.net
      minutes += 90;     // request 90 minutes more to compensate TC etc. :o
      tell(0, "Requesting episode changes of last %d minutes", minutes);
      sprintf(command, "TGET newer than %d minutes", minutes);
   }

   if (!cl.send(command))
   {
      tell(0, "Send '%s' failed, aborting transfer!", command);
      cl.close();

      return fail;
   }

   cList<cLine>* result = new cList<cLine>;
      
   while (!abort && (code = cl.receive(result)) != codeCommunicationEnd)
   {
      switch (code)
      {
         case codeFileInfo:
         {
            if (result->Count() < 2)
            {
               tell(2, "Protocol violation, aborting!");

               abort = 1;
            }
            else
            {
               linkName = "";
               fileName = result->Next(result->First())->Text();
               isLink = fileName != "not a link";
                  
               if (!isLink)
                  fileName = result->First()->Text();
               else
                  linkName = result->First()->Text();
            }
               
            break;
         }
         case codeFileContent:
         {
            if (isLink)
            {
               files.Add(new cEpisodeFile(fileName, linkName));
            }
            else
            {
               for (cLine* l = result->First(); l; l = result->Next(l))
               {
                  if (strcmp(l->Text(), "End of data") == 0)
                  {
                     result->Del(l);
                     break;
                  }
                     
                  tell(3, "Got line '%s'", l->Text());
               }

               if (result->Count())
               {
                  // create episode file and adopt the result
                  
                  files.Add(new cEpisodeFile(fileName, "", result));
                  
                  // create new result object since cEpisodeFile adopted the current
                  
                  result = new cList<cLine>;
               }
            }
               
            break;
         }
            
         case codeTransferEnd:
         {
            abort = 1;
            break;
         }
      }
         
      result->Clear();
   }

   cl.close();

   tell(0, "Received %d episode files", files.Count());

   // insert / update series into database ...

   episodeDb->getConnection()->startTransaction();
   files.storeToTable(episodeDb);
   episodeDb->getConnection()->commit();

   // optional store to filesystem ...

   if (EPG2VDRConfig.storeSeriesToFs)
   {
      char* path = 0;

      asprintf(&path, "%s/eplist", EPG2VDRConfig.cachePath);

      chkDir(path);
      files.storeToFile(path);

      free(path);
   }

   delete result;

   return 0;
}
