CREATE VIEW eventsviewplain
as
select
 sub.masterid as useid,
 cnt.eventid,
 cnt.channelid,
 sub.eventid as imageid,
 cnt.source,
 GREATEST(cnt.updsp,sub.updsp,IFNULL(epi.updsp,0)) updsp,
 cnt.updflg,
 cnt.delflg,
 cnt.fileref,
 cnt.tableid,
 cnt.version,
 sub.title,
 case
  when sub.shorttext is null or ( substring(sub.shorttext,1,1)='S' and  substring(sub.shorttext,2,1) REGEXP ('[0-9]') ) then
   case when sub.genre is null then '' else concat(sub.genre,' (', sub.country, ' ',sub.year, ')') end
  else sub.shorttext
 end shorttext,
 cnt.starttime,
 cnt.duration,
 cnt.parentalrating,
 cnt.vps,
 sub.imagecount,
 sub.genre,
 sub.category,
 sub.country,
 sub.year,
 sub.shortdescription,
 sub.shortreview,
 sub.tipp,
 sub.rating,
 sub.topic,
 sub.longdescription,
 sub.info,
 sub.moderator,
 sub.guest,
 sub.fsk,
 sub.actor,
 sub.producer,
 sub.other,
 sub.director,
 sub.screenplay,
 sub.camera,
 sub.music,
 sub.audio,
 sub.flags,
 epi. episodename,
 epi.shortname,
 epi.partname,
 epi.extracol1,
 epi.extracol2,
 epi.extracol3,
 epi.season,
 epi.part,
 epi.parts,
 epi.number,
 case when cnt.source <> sub.source then concat(upper(replace(cnt.source,'vdr','dvb')),'/',upper(sub.source)) else upper(replace(cnt.source,'vdr','dvb')) end merge
from
 events cnt
 inner join events sub on (case when cnt.useid = 0 then cnt.masterid else cnt.useid end = sub.masterid)
 left outer join episodes epi on (sub.episode = epi.compname and sub.episodepart = epi.comppartname and sub.episodelang = epi.lang);
