/*
 * update.c: EPG2VDR plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#include <locale.h>

#include <vdr/videodir.h>
#include <vdr/tools.h>

#include "epg2vdr.h"
#include "update.h"
#include "handler.h"

//***************************************************************************
// Class Update
//***************************************************************************

cUpdate::cUpdate(cPluginEPG2VDR* plugin)
   : cThread("update thread started")
{
   char* pdir;
   char* lang;

   // thread / update control

   connection = 0;
   loopActive = no;
   updateTriggered = no;
   timerUpdateTriggered = yes;
   fullreload = no;
   nextEpgdUpdateAt = 0;
   lastUpdateAt = 0;
   lastEventsUpdateAt = 0;
   epgdBusy = yes;
   epgdUpdating = no;
   eventRefreshOnly = no;
   epgdState = cEpgdState::esUnknown;

   // 

   compDb = 0;
   eventsDb = 0;
   fileDb = 0;
   imageDb = 0;
   imageRefDb = 0;
   episodeDb = 0;
   vdrDb = 0;
   mapDb = 0;
   timerDb = 0;

   selectMasterVdr = 0;
   selectAllImages = 0;
   selectUpdEvents = 0;
   selectAllChannels = 0;
   selectAllVdrChannels = 0;
   selectComponentsOf = 0;
   deleteTimer = 0;
   selectMyTimer = 0;

   // 

   epgimagedir = 0;
   withutf8 = no;
   epgHandler = 0;

   lang = setlocale(LC_CTYPE, 0);

   if (lang)
   {
      tell(0, "Set locale to '%s'", lang);

      if ((strcasestr(lang, "UTF-8") != 0) || (strcasestr(lang, "UTF8") != 0))
      {
         tell(0, "detected UTF-8");
         withutf8 = yes;
      }
   }
   else
   {
      tell(0, "Reseting locale for LC_CTYPE failed.");
   }
  
   strcpy(imageExtension, "jpg");
   asprintf(&epgimagedir, "%s/epgimages", EPG2VDR_DATA_DIR);
   asprintf(&pdir, "%s/images", epgimagedir);

   if (!(DirectoryOk(pdir) || MakeDirs(pdir, true)))
      tell(0, "could not access or create Directory %s", pdir);
  
   free(pdir);

   // check uuid

   if (isEmpty(EPG2VDRConfig.uuid))
   {
      sstrcpy(EPG2VDRConfig.uuid, getUniqueId(), sizeof(EPG2VDRConfig.uuid));
      plugin->SetupStore("Uuid", EPG2VDRConfig.uuid);
      Setup.Save();

      tell(0, "Initially created uuid '%s'", EPG2VDRConfig.uuid);
   }

   // init database ...

   cDbConnection::setEncoding(withutf8 ? "utf8": "latin1"); // mysql uses latin1 for ISO8851-1
   cDbConnection::setHost(EPG2VDRConfig.dbHost);
   cDbConnection::setPort(EPG2VDRConfig.dbPort);
   cDbConnection::setName(EPG2VDRConfig.dbName);
   cDbConnection::setUser(EPG2VDRConfig.dbUser);
   cDbConnection::setPass(EPG2VDRConfig.dbPass);

   cDbTable::setConfPath(cPlugin::ConfigDirectory("epg2vdr/"));

   // init epg handler

   epgHandler = new cEpg2VdrEpgHandler(this);

   // open tables ..

   if (initDb() != success)
      exitDb();
}

//***************************************************************************
// 
//***************************************************************************

cUpdate::~cUpdate()
{
   if (loopActive)
      Stop();
   
   free(epgimagedir);
   
   exitDb();
}

//***************************************************************************
// Init/Exit Database Connections
//***************************************************************************

int cUpdate::initDb()
{
   int status = success;

   if (!connection)
      connection = new cDbConnection();

   vdrDb = new cTableVdrs(connection);
   if (vdrDb->open() != success) return fail;
      
   // DB-API check 

   vdrDb->clear();
   vdrDb->setValue(cTableVdrs::fiUuid, EPGDNAME);

   if (!vdrDb->find())
   {
      tell(0, "Can't lookup epgd information, start epgd to create the tables first! Aborting now.");
      return fail;
   }

   vdrDb->reset();

   if (vdrDb->getIntValue(cTableVdrs::fiDbApi) != DB_API)
   {
      tell(0, "Found dbapi %d, expected %d, please alter the tables first! Aborting now.",  
           (int)vdrDb->getIntValue(cTableVdrs::fiDbApi), DB_API);
      return fail;
   }

   // open tables ..

   mapDb = new cTableChannelMap(connection);
   if (mapDb->open() != success) return fail;

   fileDb = new cTableFileRefs(connection);
   if (fileDb->open() != success) return fail;

   imageDb = new cTableImages(connection);
   if (imageDb->open() != success) return fail;

   imageRefDb = new cTableImageRefs(connection);
   if (imageRefDb->open() != success) return fail;

   episodeDb = new cTableEpisodes(connection);
   if (episodeDb->open() != success) return fail;

   eventsDb = new cTableEvents(connection);
   if (eventsDb->open() != success) return fail;

   compDb = new cTableComponents(connection);
   if (compDb->open() != success) return fail;

   timerDb = new cTableTimers(connection);
   if (timerDb->open() != success) return fail;

   // -------------------------------------------
   // init statements

   selectMasterVdr = new cDbStatement(vdrDb);

   // select uuid, name from vdrs
   //    where upper(master) = 'Y' and uuid <>  ?;

   selectMasterVdr->build("select ");
   selectMasterVdr->bind(cTableVdrs::fiUuid, cDBS::bndOut);
   selectMasterVdr->bind(cTableVdrs::fiName, cDBS::bndOut, ", ");
   selectMasterVdr->build(" from %s where upper(master) = 'Y' and ", vdrDb->TableName());
   selectMasterVdr->bindCmp(0, cTableVdrs::fiUuid, 0, "<>");

   status += selectMasterVdr->prepare();

   // all images

   selectAllImages = new cDbStatement(imageRefDb);

   // prepare fields

   cDBS::FieldDef imageSizeDef = { "image", cDBS::ffUInt,  0, 999, cDBS::ftData };
   imageSize.setField(&imageSizeDef);
   imageUpdSp.setField(imageDb->getField(cTableImages::fiUpdSp));
   masterId.setField(eventsDb->getField(cTableEvents::fiMasterId));

   // select e.masterid, r.imagename, r.eventid, r.lfn, length(i.image)
   //      from imagerefs r, images i, events e 
   //      where i.imagename = r.imagename 
   //         and e.eventid = r.eventid
   //         and (i.updsp > ? or r.updsp > ?)

   selectAllImages->build("select ");
   selectAllImages->setBindPrefix("e.");
   selectAllImages->bind(&masterId, cDBS::bndOut);
   selectAllImages->setBindPrefix("r.");
   selectAllImages->bind(cTableImageRefs::fiImgName, cDBS::bndOut, ", ");
   selectAllImages->bind(cTableImageRefs::fiEventId, cDBS::bndOut, ", ");
   selectAllImages->bind(cTableImageRefs::fiLfn, cDBS::bndOut, ", ");
   selectAllImages->setBindPrefix("i.");
   selectAllImages->build(", length(");
   selectAllImages->bind(&imageSize, cDBS::bndOut);
   selectAllImages->build(")");
   selectAllImages->clrBindPrefix();
   selectAllImages->build(" from %s r, %s i, %s e where ", 
                          imageRefDb->TableName(), imageDb->TableName(), eventsDb->TableName());
   selectAllImages->build("e.%s = r.%s and i.%s = r.%s and (",
                          eventsDb->getField(cTableEvents::fiEventId)->name, 
                          imageRefDb->getField(cTableImageRefs::fiEventId)->name,
                          imageDb->getField(cTableImages::fiImgName)->name,
                          imageRefDb->getField(cTableImageRefs::fiImgName)->name);
   selectAllImages->bindCmp("i", &imageUpdSp, ">");
   selectAllImages->build(" or ");
   selectAllImages->bindCmp("r", cTableImageRefs::fiUpdSp, 0, ">");
   selectAllImages->build(")");

   status += selectAllImages->prepare();

   // select distinct channelid, channelname 
   //   from channelmap;

   selectAllChannels = new cDbStatement(mapDb);

   selectAllChannels->build("select distinct ");
   selectAllChannels->bind(cTableChannelMap::fiChannelId, cDBS::bndOut);
   selectAllChannels->bind(cTableChannelMap::fiChannelName, cDBS::bndOut, ", ");
   selectAllChannels->build(" from %s;", mapDb->TableName());

   status += selectAllChannels->prepare();

   // select channelid, source 
   //   from channlemap 
   //    where source = ?;

   selectAllVdrChannels = new cDbStatement(mapDb);

   selectAllVdrChannels->build("select ");
   selectAllVdrChannels->bind(cTableChannelMap::fiChannelId, cDBS::bndOut);
   selectAllVdrChannels->bind(cTableChannelMap::fiChannelName, cDBS::bndOut, ", ");
   selectAllVdrChannels->bind(cTableChannelMap::fiSource, cDBS::bndOut, ", ");
   selectAllVdrChannels->build(" from %s where ", mapDb->TableName());
   selectAllVdrChannels->bind(cTableChannelMap::fiSource, cDBS::bndIn | cDBS::bndSet);

   status += selectAllVdrChannels->prepare();

   // select changed events

   selectUpdEvents = new cDbStatement(eventsDb);

   // select useid, eventid, source, delflg, updflg, fileref, 
   //        tableid, version, title, shorttext, starttime, 
   //        duration, parentalrating, vps, description
   //    from eventsview
   //      where 
   //        channelid = ?
   //        and updsp > ?
   //        and updflg in (.....)

   selectUpdEvents->build("select ");
   selectUpdEvents->bind(cTableEvents::fiUseId, cDBS::bndOut);
//   selectUpdEvents->bind(cTableEvents::fiMasterId, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiEventId, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiSource, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiDelFlg, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiUpdFlg, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiFileRef, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiTableId, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiVersion, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiTitle, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiShortText, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiStartTime, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiDuration, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiParentalRating, cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiVps,  cDBS::bndOut, ", ");
   selectUpdEvents->bind(cTableEvents::fiDescription, cDBS::bndOut, ", ");
   selectUpdEvents->build(" from eventsview where ");
   selectUpdEvents->bind(cTableEvents::fiChannelId, cDBS::bndIn | cDBS::bndSet);
   selectUpdEvents->bindCmp(0, cTableEvents::fiUpdSp, 0, ">", " and ");
   selectUpdEvents->build(" and updflg in (%s)", Us::getNeeded());

   status += selectUpdEvents->prepare();

   // ...
  
   // select stream, type, lang, description 
   //  from components where 
   // eventid = ?;
   // channelid = ?;

   selectComponentsOf = new cDbStatement(compDb);
   
   selectComponentsOf->build("select ");
   selectComponentsOf->bind(cTableComponents::fiStream, cDBS::bndOut);
   selectComponentsOf->bind(cTableComponents::fiType, cDBS::bndOut, ",");
   selectComponentsOf->bind(cTableComponents::fiLang, cDBS::bndOut, ",");
   selectComponentsOf->bind(cTableComponents::fiDescription, cDBS::bndOut, ",");
   selectComponentsOf->build(" from %s where ", compDb->TableName());
   selectComponentsOf->bind(cTableComponents::fiEventId, cDBS::bndIn |cDBS::bndSet);
   selectComponentsOf->bind(cTableComponents::fiChannelId, cDBS::bndIn |cDBS::bndSet, " and ");
   
   status += selectComponentsOf->prepare();
   
   // select eventid, channelid, starttime, state, endtime
   //   from timers where 
   //     vdruuid = ?

   selectMyTimer = new cDbStatement(timerDb);
   
   selectMyTimer->build("select ");
   selectMyTimer->bind(cTableTimers::fiEventId, cDBS::bndOut);
   selectMyTimer->bind(cTableTimers::fiChannelId, cDBS::bndOut, ",");
   selectMyTimer->bind(cTableTimers::fiStartTime, cDBS::bndOut, ",");
   selectMyTimer->bind(cTableTimers::fiState, cDBS::bndOut, ",");
   selectMyTimer->bind(cTableTimers::fiEndTime, cDBS::bndOut, ",");
   selectMyTimer->build(" from %s where ", timerDb->TableName());
   selectMyTimer->bind(cTableTimers::fiVdrUuid, cDBS::bndIn |cDBS::bndSet);
   
   status += selectMyTimer->prepare();

   // delete from timers where 

   deleteTimer = new cDbStatement(timerDb);

   deleteTimer->build("delete from %s where ", timerDb->TableName());
   deleteTimer->bind(cTableTimers::fiEventId, cDBS::bndIn |cDBS::bndSet);
   deleteTimer->bind(cTableTimers::fiChannelId, cDBS::bndIn |cDBS::bndSet, " and ");
   deleteTimer->bind(cTableTimers::fiVdrUuid, cDBS::bndIn |cDBS::bndSet, " and ");

   status += deleteTimer->prepare();

   if (status == success)
   {
      // -------------------------------------------
      // lookback -> get last stamp
      
      vdrDb->clear();
      vdrDb->setValue(cTableVdrs::fiUuid, EPG2VDRConfig.uuid);
      
      if (vdrDb->find())
      {
         char buf[50+TB];

         lastUpdateAt = vdrDb->getIntValue(cTableVdrs::fiLastUpdate);
         lastEventsUpdateAt = lastUpdateAt;

         strftime(buf, 50, "%y.%m.%d %H:%M:%S", localtime(&lastUpdateAt));
         tell(0, "Info: Last update was at '%s'", buf);
      }
      
      // register me to the vdrs table
      
      char* v;

      asprintf(&v, "vdr %s epg2vdr %s (%s)", VDRVERSION, VERSION, VERSION_DATE);
      vdrDb->setValue(cTableVdrs::fiUuid, EPG2VDRConfig.uuid);
      vdrDb->setValue(cTableVdrs::fiIp, getFirstIp());
      vdrDb->setValue(cTableVdrs::fiName, getHostName());
      vdrDb->setValue(cTableVdrs::fiDbApi, DB_API);
      vdrDb->setValue(cTableVdrs::fiVersion, v);
      vdrDb->setValue(cTableVdrs::fiState, "attached");
      vdrDb->setValue(cTableVdrs::fiMaster, "n");

      // set svdrp port if uninitialized, we cant query ther actual port from VDR

//       if (vdrDb->getIntValue(cTableVdrs::fiSvdrp) == 0)
//          vdrDb->setValue(cTableVdrs::fiSvdrp, 6419);

      vdrDb->store();
      free(v);
   }

   if (status == success)
   {
      status += epgHandler->updateExternalIdsMap(mapDb);
      checkHandlerRole();
   }

   return status;
}

int cUpdate::exitDb()
{
   // de-register me at the vdrs table

   if (vdrDb && vdrDb->isConnected())
   {
      vdrDb->setValue(cTableVdrs::fiUuid, EPG2VDRConfig.uuid);
      vdrDb->find();
      vdrDb->setValue(cTableVdrs::fiMaster, "n");
      vdrDb->setValue(cTableVdrs::fiState, "detached");
      vdrDb->store();
   }

   delete selectAllImages;      selectAllImages = 0;
   delete selectUpdEvents;      selectUpdEvents = 0;
   delete selectAllChannels;    selectAllChannels = 0;
   delete selectAllVdrChannels; selectAllVdrChannels = 0;
   delete selectComponentsOf;   selectComponentsOf = 0;
   delete selectMasterVdr;      selectMasterVdr = 0;
   delete deleteTimer;          deleteTimer = 0;
   delete selectMyTimer;        selectMyTimer = 0;

   delete eventsDb;    eventsDb = 0;
   delete compDb;     compDb = 0;
   delete fileDb;     fileDb = 0;
   delete imageDb;    imageDb = 0;
   delete imageRefDb; imageRefDb = 0;
   delete episodeDb;  episodeDb = 0;
   delete vdrDb;      vdrDb = 0;
   delete mapDb;      mapDb = 0;
   delete timerDb;    timerDb = 0;

   delete connection; connection = 0;

   return done;
}

//***************************************************************************
// Get Timer Of Event
//***************************************************************************

cTimer* cUpdate::getTimerOf(const cEvent* event) const
{
   for (cTimer* t = Timers.First(); t; t = Timers.Next(t)) 
      if (t->Event() == event)
         return t;

   return 0;
}

//***************************************************************************
// Check Connection
//***************************************************************************

int cUpdate::checkConnection(int& timeout)
{
   static int retry = 0;

   timeout = retry < 5 ? 10 : 60;

   // check connection

   if (!dbConnected(yes))
   {
      // try to connect

      tell(0, "Trying to re-connect to database!");
      retry++;

      if (initDb() != success)
      {
         tell(0, "Retry #%d failed, retrying in %d seconds!", retry, timeout);
         exitDb();

         return fail;
      }

      retry = 0;         
      tell(0, "Connection established successfull!");
   }

   return success;
}

//***************************************************************************
// Check Handler Role
//***************************************************************************

void cUpdate::checkHandlerRole()
{
   char buf[1+TB];
   int active;
   char flag = 0;

/*
  wenn no    - mich ausschalten und feritg (db 'n' und handler off)
  wenn auto  - aushandeln wie gehabt
  wenn yes   - (db 'Y' und handler on)

*/

   if (EPG2VDRConfig.masterMode == mmAuto)
   {
      tell(3, "Auto check master role");

      // select where "uuid <> ? and upper(master) = 'Y'"

      vdrDb->clear();
      vdrDb->setValue(cTableVdrs::fiUuid, EPG2VDRConfig.uuid);
      
      active = !selectMasterVdr->find();

      if (!active)
         tell(3, "Master found, uuid '%s' (%s)", 
              vdrDb->getStrValue(cTableVdrs::fiUuid), 
              vdrDb->getStrValue(cTableVdrs::fiName));
      
      selectMasterVdr->freeResult();
      
      flag = active ? 'y' : 'n';
   }
   else if (EPG2VDRConfig.masterMode == mmYes)
   {
      flag = 'Y';
      active = yes;
   }
   else
   {
      flag = 'n';
      active = no;
   }

   // write again to force update the updsp

   vdrDb->clear();
   vdrDb->setValue(cTableVdrs::fiUuid, EPG2VDRConfig.uuid);
   vdrDb->find();

   vdrDb->setValue(cTableVdrs::fiState, "attached");
   vdrDb->setValue(cTableVdrs::fiMaster, c2s(flag, buf));
   vdrDb->store();

   if (!epgdBusy && epgHandler->getActive() != active)
   {
      tell(2, "Change handler state to '%s'", active ? "active" : "standby");
      epgHandler->setActive(active);
   }
}

//***************************************************************************
// Updates Pending
//***************************************************************************

int cUpdate::updatesPending(int timeout)
{
   int pending = no;

   epgdBusy = no;
   epgdUpdating = no;

   // check epgd state

   vdrDb->clear();
   vdrDb->setValue(cTableVdrs::fiUuid, EPGDNAME);

   if (vdrDb->find())
   {
      nextEpgdUpdateAt = vdrDb->getIntValue(cTableVdrs::fiNextUpdate);
      epgdState = cEpgdState::toState(vdrDb->getStrValue(cTableVdrs::fiState));

      if (epgdState >= cEpgdState::esBusy && epgdState < cEpgdState::esBusyImages)
      {
         epgdBusy = yes;

         if (epgHandler->getActive())
            tell(2, "Change handler state to 'standby'");

         epgHandler->setActive(no);

         pending = epgdState == Es::esBusyMatch || epgdState == Es::esBusyEvents;

//         if (pending)
         {
            char buf[50+TB];
            time_t updsp = vdrDb->getIntValue(cTableVdrs::fiUpdSp);

            strftime(buf, 50, "%y.%m.%d %H:%M:%S", localtime(&updsp));
            tell(2, "Info: Updates pending but epgd '%s' since '%s', retrying in %d seconds", 
                 Es::toName(epgdState), buf, timeout);
            
            // set but don't reset here!
            
            if (epgdState == Es::esBusyMatch)
               eventRefreshOnly = yes;
            
            if (epgdState == cEpgdState::esBusyEvents)
               epgdUpdating = yes;
         }
      }

      // foce pending if ..

      if (updateTriggered || vdrDb->getIntValue(cTableVdrs::fiLastUpdate) > lastUpdateAt)
         pending = yes;
   }

   vdrDb->reset();

   return pending;
}

//***************************************************************************
// Trigger Update
//***************************************************************************

void cUpdate::triggerUpdate(int reload)
{
   if (!loopActive)
   {
      tell(0, "Thread not running, try to recover ...");
      Cancel(3);
      Start();
   }

   fullreload = reload;
   updateTriggered = yes;
   waitCondition.Broadcast();    // wakeup thread
}

//***************************************************************************
// Stop Thread
//***************************************************************************

void cUpdate::Stop()
{   
   loopActive = no;
   waitCondition.Broadcast();    // wakeup thread

   Cancel(20);                   // wait up to 20 seconds for thread was stopping
}

//***************************************************************************
// Action
//***************************************************************************

void cUpdate::Action()
{
   time_t nextAt = time(0) + 60;

   tell(0, "Update thread started (pid=%d)", getpid());
   
   mutex.Lock();         // mutex gets ONLY unlocked when sleeping
   loopActive = yes;

   while (loopActive && Running())
   {      
      int reconnectTimeout;  // set by checkConnection()
      int wait;

      // calc wait time, we sleep at most 10 seconds or (if shorter) 
      // to the next update time

      wait = nextAt - time(0);
      wait = wait > 0 && wait < 10 ? wait : 10;

      // wait time in ms

      waitCondition.TimedWait(mutex, wait*1000);

      // we pass here at least once per 10 seconds ..

      if (checkConnection(reconnectTimeout) != success)
      {
         nextAt = time(0) + reconnectTimeout;
         continue;
      }

      // update timer

      if (timerUpdateTriggered)
         updateTimer();

      // update EPG
      
      updateTriggered = updatesPending(10);
      checkHandlerRole();

      // if triggered externally or updates pending

      if (!updateTriggered)
         continue;

      if (epgdBusy)
      {
         nextAt = time(0) + 10;
         continue;
      }

      if (dbConnected(yes))
      {
         if (fullreload)
            eventRefreshOnly = no;
         
         tell(eventRefreshOnly ? 2 : 0, "--- EPG %s started ---", fullreload ? "reload" : eventRefreshOnly ? "refresh" : "update");
         
         if (eventRefreshOnly)
         {
            refreshEpg();
            lastEventsUpdateAt = time(0);
            cSchedules::Cleanup(true);   // force VDR to store of epg.data to filesystem
         }
         else
         {
            // cleanup unreferenced image-files
            
            if (cleanupPictures() != success)
               continue;
            
            if (refreshEpg() != success)
               continue;
            
            // force VDR to store of epg.data to filesystem

            cSchedules::Cleanup(true);
            
            // get pictures from database and copy to local FS
            
            if (storePicturesToFs() != success)
               continue;
            
            // update lookback information (notice) 
            
            lastUpdateAt = time(0);
            lastEventsUpdateAt = lastUpdateAt;

            vdrDb->clear();
            vdrDb->setValue(cTableVdrs::fiUuid, EPG2VDRConfig.uuid);
            vdrDb->find();
            vdrDb->setValue(cTableVdrs::fiLastUpdate, lastUpdateAt);
            vdrDb->store();
         }

         tell(eventRefreshOnly ? 2 : 0, "--- EPG %s finished ---", fullreload ? "reload" : eventRefreshOnly ? "refresh" : "update");

         if (EPG2VDRConfig.loglevel > 2)
            connection->showStat("update");

         eventRefreshOnly = no;
         fullreload = no;
         updateTriggered = no;
      }
   } 

   loopActive = no;

   tell(0, "Update thread ended (pid=%d)", getpid());
}

//***************************************************************************
// Refresh Epg
//***************************************************************************

int cUpdate::refreshEpg()
{
   const cEvent* event;
   int total = 0;
   int dels = 0;
   int channels = 0;
   uint64_t start = cTimeMs::Now();
   time_t updateSince;

   if (fullreload)
   {
      tell(1, "Removing all events from epg");

      while (!cSchedules::ClearAll())
         tell(0, "Warning: Clear EPG failed, can't get lock. Retrying ...");

      lastUpdateAt = 0;
   }

   updateSince = eventRefreshOnly  ? lastEventsUpdateAt : lastUpdateAt;

   if (updateSince)
      tell(eventRefreshOnly ? 2 : 1, "Update EPG, loading changes since %s", l2pTime(updateSince).c_str());
   else
      tell(1, "Update EPG, reloading all events");
   
   // 
   // iterate over all channels in channelmap
   // 

   mapDb->clear();
   mapDb->setValue(cTableChannelMap::fiSource, "vdr"); // only used from selectAllVdrChannels

   for (int f = selectAllChannels->find(); f; f = selectAllChannels->fetch())
   {
      int count = 0;
      cSchedulesLock* schedulesLock = new cSchedulesLock(true);
      cSchedules* ss = 0;
      cSchedule* s = 0;
      tChannelID channelId = tChannelID::FromString(mapDb->getStrValue(cTableChannelMap::fiChannelId));

      channels++;

      eventsDb->clear();
      eventsDb->setValue(cTableEvents::fiUpdSp, updateSince);
      eventsDb->setValue(cTableEvents::fiChannelId, mapDb->getStrValue(cTableChannelMap::fiChannelId));

      // lock holen

      if (!(ss = (cSchedules*)cSchedules::Schedules(*schedulesLock)))
      {
         delete schedulesLock;
         tell(0, "Error, can't get lock on schedules, aborting refresh!");
         selectAllChannels->freeResult();
         
         return fail;  // #TODO -> break!?
      }

      // get schedule (channel)
      
      if (!(s = (cSchedule*)ss->GetSchedule(channelId)))
         s = ss->AddSchedule(channelId);

      //
      // iterate over all events of this channel
      // 

      for (int found = selectUpdEvents->find(); found; found = selectUpdEvents->fetch())
      {
         cTimer* timer = 0;
         char updFlg = toupper(eventsDb->getStrValue(cTableEvents::fiUpdFlg)[0]);

         // fix missing flag

         updFlg = updFlg == 0 ? 'P' : updFlg;

         // ignore unneded event rows ..

         if (!Us::isNeeded(updFlg))
            continue;

         // get event / timer 
         
         if (event = s->GetEvent(eventsDb->getIntValue(cTableEvents::fiUseId)))
         {
            if (Us::isRemove(updFlg))
               tell(2, "Remove event %uld of channel '%s' due to updflg %c", 
                    event->EventID(),  (const char*)event->ChannelID().ToString(), updFlg);

            timer = getTimerOf(event);
            s->DelEvent((cEvent*)event);
         }

         if (!Us::isRemove(updFlg))
            event = s->AddEvent(createEventFromRow(eventsDb->getRow()));
         else if (event)
         {
            event = 0;
            dels++;
         }
         
         if (timer && event)
            timer->SetEvent(event);
         
         count++;

         if (!dbConnected())
            break;
      }

      selectUpdEvents->freeResult();

      // Kanal fertig machen ..

      s->Sort();
      ss->SetModified(s);
      
      // lock freigeben 
      
      if (schedulesLock)
         delete schedulesLock;
                
      tell(2, "Processed channel '%s' - '%s' with %d updates",
           eventsDb->getStrValue(cTableEvents::fiChannelId),
           mapDb->getStrValue(cTableChannelMap::fiChannelName),
           count);

      total += count;

      if (!dbConnected(yes))
         break;
   }

   selectAllChannels->freeResult();

   if (updateSince)
      tell(1, "Updated changes since '%s'; %d channels, %d events (%d deletions) in %s", 
           l2pTime(updateSince).c_str(),
           channels, total, dels, ms2Dur(cTimeMs::Now()-start).c_str());
   else
      tell(0, "Updated all %d channels, %d events (%d deletions) in %s", 
           channels, total, dels, ms2Dur(cTimeMs::Now()-start).c_str());
   
   return dbConnected(yes) ? success : fail;
}

//***************************************************************************
// To/From Row
//***************************************************************************

cEvent* cUpdate::createEventFromRow(const cDbRow* row)
{
   cEvent* e = new cEvent(row->getIntValue(cTableEvents::fiUseId));

   e->SetTableID(row->getIntValue(cTableEvents::fiTableId));
   e->SetVersion(row->getIntValue(cTableEvents::fiVersion));
   e->SetTitle(row->getStrValue(cTableEvents::fiTitle));
   e->SetShortText(row->getStrValue(cTableEvents::fiShortText));
   e->SetStartTime(row->getIntValue(cTableEvents::fiStartTime));
   e->SetDuration(row->getIntValue(cTableEvents::fiDuration));
   e->SetParentalRating(row->getIntValue(cTableEvents::fiParentalRating));
   e->SetVps(row->getIntValue(cTableEvents::fiVps));
   e->SetDescription(row->getStrValue(cTableEvents::fiDescription));
   e->SetComponents(0);

   // components

   if (row->hasValue(cTableEvents::fiSource, "vdr"))
   {
      cComponents* components = new cComponents;
      
      compDb->clear();
      compDb->setValue(cTableComponents::fiEventId, row->getIntValue(cTableEvents::fiEventId));
      compDb->setValue(cTableComponents::fiChannelId, row->getStrValue(cTableEvents::fiChannelId));
      
      for (int f = selectComponentsOf->find(); f; f = selectComponentsOf->fetch())
      {
         components->SetComponent(components->NumComponents(),
                                  compDb->getIntValue(cTableComponents::fiStream),
                                  compDb->getIntValue(cTableComponents::fiType),
                                  compDb->getStrValue(cTableComponents::fiLang),
                                  compDb->getStrValue(cTableComponents::fiDescription));
      }
      
      selectComponentsOf->freeResult();
      
      if (components->NumComponents())
         e->SetComponents(components);      // event take ownership of components!
      else
         delete components;
   }

   return e;
}

//***************************************************************************
// Store Pictures to local Filesystem
//***************************************************************************

int cUpdate::storePicturesToFs()
{
   int count = 0;
   int updated = 0;
   char* path = 0;
   time_t start = time(0);

   if (!EPG2VDRConfig.getepgimages)
      return done;

   asprintf(&path, "%s", epgimagedir);
   chkDir(path);
   free(path);
   asprintf(&path, "%s/images", epgimagedir);
   chkDir(path);
   free(path);

   tell(0, "Load images from database");

   imageRefDb->clear();
   imageRefDb->setValue(cTableImageRefs::fiUpdSp, lastUpdateAt);
   imageUpdSp.setValue(lastUpdateAt);
   
   for (int res = selectAllImages->find(); res; res = selectAllImages->fetch())
   {
      int eventid = masterId.getIntValue();
      const char* imageName = imageRefDb->getStrValue(cTableImageRefs::fiImgName);
      int lfn = imageRefDb->getIntValue(cTableImageRefs::fiLfn);
      char* newpath;
      char* linkdest = 0;
      char* destfile = 0;
      int forceLink = no;
      int size = imageSize.getIntValue();

      asprintf(&destfile, "%s/images/%s", epgimagedir, imageName);
      
      // check target ... image changed?

      if (!fileExists(destfile) || fileSize(destfile) != size)
      {
         // get image 
         
         imageDb->clear();
         imageDb->setValue(cTableImages::fiImgName, imageName);
         
         if (imageDb->find() && !imageDb->getRow()->getValue(cTableImages::fiImage)->isNull())
         {
            count++;

            // remove existing target
            
            if (fileExists(destfile))
            {
               updated++;
               removeFile(destfile);
            }
            
            forceLink = yes;
            tell(2, "Store image '%s' with %d bytes", destfile, size);
            
            if (FILE* fh1 = fopen(destfile, "w"))
            {
               fwrite(imageDb->getStrValue(cTableImages::fiImage), 1, size, fh1);
               fclose(fh1);
            }
            else
            {
               tell(1, "Can't write image to '%s', error was '%m'", destfile);
            }
         }

         imageDb->reset();
      }

      free(destfile);
     
      // create links ...

      asprintf(&linkdest, "./images/%s", imageName);

#ifdef _IMG_LINK
      if (!lfn)
      {
         // for lfn 0 create additional link without "_?"

         asprintf(&newpath, "%s/%d.%s", epgimagedir, eventid, imageExtension);
         createLink(newpath, linkdest, forceLink);
         free(newpath);
      }
#endif

      // create link with index

      asprintf(&newpath, "%s/%d_%d.%s", epgimagedir, eventid, lfn, imageExtension);
      createLink(newpath, linkdest, forceLink);
      free(newpath);

      // ...
      
      free(linkdest);

      if (!dbConnected())
         break;
   }

   selectAllImages->freeResult();

   tell(0, "Got %d images from database in %ld seconds (%d updates, %d new)", 
        count, time(0) - start, updated, count-updated);
 
   return dbConnected(yes) ? success : fail;
}

//***************************************************************************
// Remove Pictures
//***************************************************************************

int cUpdate::cleanupPictures()
{
   const char* ext = ".jpg";
   struct dirent* dirent;   
   DIR* dir;
   char* pdir;
   int iCount = 0;
   int lCount = 0;

   imageRefDb->countWhere("", iCount);

   if (iCount < 100)   // less than 100 image lines are suspicious ;)
   {
      tell(0, "Exit image cleanup to avoid deleting of all images on empty imagerefs table");
      return done;
   }

   // -----------------------
   // remove unused images

   tell(1, "Starting cleanup of images in '%s'", epgimagedir);
 
   // -----------------------
   // cleanup 'images' directory 

   cDbStatement* stmt = new cDbStatement(imageRefDb);
   
   stmt->build("select ");
   stmt->bind(cTableImageRefs::fiFileRef, cDBS::bndOut);
   stmt->build(" from %s where ", imageRefDb->TableName());
   stmt->bind(cTableImageRefs::fiImgName, cDBS::bndIn | cDBS::bndSet);

   if (stmt->prepare() != success)
   {
      delete stmt;
      return fail;
   }

   iCount = 0;

   // open directory

   asprintf(&pdir, "%s/images", epgimagedir);

   if (!(dir = opendir(pdir)))
   {
      tell(1, "Can't open directory '%s', '%m'", pdir);

      free(pdir);

      return done;
   }

   free(pdir);

   while (dbConnected() && (dirent = readdir(dir)))
   {
      // check extension

      if (strncmp(dirent->d_name + strlen(dirent->d_name) - strlen(ext), ext, strlen(ext)) != 0)
         continue;

      imageRefDb->clear();
      imageRefDb->setValue(cTableImageRefs::fiImgName, dirent->d_name);
     
      if (!stmt->find())
      {
         asprintf(&pdir, "%s/images/%s", epgimagedir, dirent->d_name);

         if (!removeFile(pdir))
            iCount++;
         
         free(pdir);
      }

      stmt->freeResult();
   }

   delete stmt;
   closedir(dir);

   if (!dbConnected(yes))
      return fail;

   // -----------------------
   // remove wasted symlinks
   
   if (!(dir = opendir(epgimagedir)))
   {
      tell(1, "Can't open directory '%s', '%m'", epgimagedir);
      return done;
   }

   tell(1, "Remove %s symlinks", fullreload ? "all" : "old");
   
   while ((dirent = readdir(dir)))
   {
      // check extension
      
      if (strncmp(dirent->d_name + strlen(dirent->d_name) - strlen(ext), ext, strlen(ext)) != 0)
         continue;
      
      asprintf(&pdir, "%s/%s", epgimagedir, dirent->d_name);
      
      // fileExists use access() which dereference links!
      
      if (isLink(pdir) && (fullreload || !fileExists(pdir)))
      {
         if (!removeFile(pdir))
            lCount++;
      }
      
      free(pdir);
   }
   
   closedir(dir);
   tell(1, "Cleanup finished, removed (%d) images and (%d) symlinks", iCount, lCount);
   
   return success;
}

//***************************************************************************
// Link Needed
//***************************************************************************

int cUpdate::pictureLinkNeeded(const char* linkName)
{
   int found;

   if (!dbConnected())
      return yes;

   // we don't need to patch the linkname "123456_0.jpg"
   // since atoi() stops at the first non numerical character ...

   imageRefDb->clear();
   imageRefDb->setValue(cTableImageRefs::fiLfn, 0L);
   imageRefDb->setValue(cTableImageRefs::fiEventId, atoi(linkName));
   
   found = imageRefDb->find();
   imageRefDb->reset();

   return found;
}
