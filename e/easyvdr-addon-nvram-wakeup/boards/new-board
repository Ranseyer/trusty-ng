#!/usr/bin/ruby

#
# NVRAM WakeUp - Add new board to yaml database
#
# Copyright (c) 2008 Tobias Grimm
#
# $Id$
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

require 'yaml'
require 'pp'
require 'keys'

dmi = {}
infowriter = {}

def dump_settings(infowriter)
    for key_value in @biosinfo_keys 
        if infowriter[key_value]
            puts "#{key_value} = #{infowriter[key_value]}"
        end
    end
end

for line in File.new(ARGV[0], 'r')
    if line =~ /(Mainboard|BIOS) ([^:]*):\s*"(.*)"\s*$/
        key = $2
        key = 'bios' + key if $1 == 'BIOS'
        key = 'version' if key == 'revision'
        value = $3
        dmi[key] = value
    end
    if line =~ /\s*([^ ]*)\s*=\s*([^ ]*)\s*/
        infowriter[$1]=$2.chomp
    end
end

boards = YAML.load(File.new('boards.yaml', 'r'))

for board in boards
    if board.slice(@dmi_keys) == dmi
        puts "The list of boards already contains one with the same DMI identifier"
        if board.slice(@biosinfo_keys) == infowriter
            puts "...and with the same settings"
        else
            puts "...but with different settings"
            puts "Original settings:"
            dump_settings board
            puts
            puts "New settings:"
            dump_settings infowriter
        end
        exit 1
    end
end

iw_name='AUTO'

for board in boards
    if board.slice(@biosinfo_keys) == infowriter
       iw_name = board.infowriter
       puts "Matching infowriter: #{board.infowriter}"
       break
    end
end

puts "- vendor         : '#{dmi.vendor}'"
puts "  type           : '#{dmi['type']}'"
puts "  version        : '#{dmi.version}'"
puts "  biosvendor     : '#{dmi.biosvendor}'"
puts "  biosversion    : '#{dmi.biosversion}'"
puts "  biosrelease    : '#{dmi.biosrelease}'"
puts "  infowriter     : '#{iw_name}'"

for key in @biosinfo_keys
    if infowriter[key]
        puts "  %-15s: '%s'" % [key, infowriter[key]]
    end
end
