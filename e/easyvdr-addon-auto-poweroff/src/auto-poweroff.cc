/*
 * main.cc
 * Copyright (C) 2014 Bleifuss <bleifuss2@gmx.net>
 * 
 * make-remote-mapping is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * make-remote-mapping is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


//#include <fcntl.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <stdio.h>

//#include <cstdio>

#include <termios.h>
#include <fstream>
#include <iostream>
#include <limits.h>
#include <string>

#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <getopt.h>


#include <pthread.h>


//Wichtig!! Projekt (rechtes Fenster) rechts anklicken
//Eigenschaften Flags für C- Kompiler -pthread einstellen


#define DEV_LIRCD "lircd"
#define VARRUNDIR "/var/run"
#define PACKAGE "lirc"

#define LIRCD	   VARRUNDIR "/" PACKAGE "/" DEV_LIRCD 




std::string ProgramName="auto-poweroff";

char Lircbuf[128];
bool write_input=false;
int error=0;
bool Init_Lirc=false;
bool Quiet=false;
bool UserInput=false;

//######################################################################
void clear_cin_buffer(void) {
	//Buffer leeren
	std::cin.clear();
	std::cin.ignore(INT_MAX,'\n');
}

//Auf return warten
void wait_key_return(void){
	if(Quiet!=true){
		std::cout << "Return druecken" << std::endl;
		std::cin;
		clear_cin_buffer();
	}
}
//######################################################################
void * RemoteThread(void *p) {
int i;
int LircSocket;
struct sockaddr_un addr;
	
	//Socket Typ
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, LIRCD);

	//Socket öffnen
	LircSocket = socket(AF_UNIX, SOCK_STREAM, 0);
	if (LircSocket == -1) {
		perror("socket");
		wait_key_return();		
		exit(errno);
	};

	
	if (connect(LircSocket, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
		perror("connect lirc socket");
		wait_key_return();		
		exit(errno);
	};

	//Lirc OK
	Init_Lirc=true;
	
	for (;;) {
		i = read(LircSocket, Lircbuf, 128);
		if (i == -1) {
			perror("read");
			wait_key_return();			
			exit(errno);
		};

		
		if (!i)
			exit(0);
		//Taste gedrückt		
		UserInput=true;
		
	};

	close(LircSocket);
}


//#############################################################################
void print_debug (const std::string &vdr_remote_conf, 
                  const std::string &xbmc_lircmap_xml,
                  const std::string &pchanger_lirc_conf){
	std::cout << "vdr remote.conf:"  << vdr_remote_conf << std::endl;
	std::cout << "xbmc lircmap xml:" << xbmc_lircmap_xml << std::endl;
	std::cout << "pchanger lirc.conf:" << pchanger_lirc_conf << std::endl;
}
//#############################################################################
void print_help (){
	std::cout << std::endl << std::endl;
	std::cout << "---------------------------------\n" << std::endl;
	std::cout << "Auto-Poweroff bei keiner Benutzer eingabe Parameter:" << std::endl << std::endl;
	std::cout << "-t Poweroff Zeit in Minuten" << std::endl;
	std::cout << "-c Befehl der dann ausgeführt wird" << std::endl;
	std::cout << "-l Lirc eingaben ignorieren" << std::endl;
	std::cout << "-k eingaben ignorieren" << std::endl;
	std::cout << "Beispiel:" << std::endl;
	std::cout << "auto-poweroff -t 120 -c "svdrpsend HITK Power"" << std::endl;
}


//#############################################################################
main(int argc, char *argv[]){

pthread_t thread;
int ret;	

	//Lirc Thread starten
	if(pthread_create(&thread, NULL, RemoteThread, NULL) != 0) {
		perror("pthread_create");
		wait_key_return();
		exit (EXIT_FAILURE);
	}
	sleep (1);

	//Lirc erfolgreich initialisiert?
	if(Init_Lirc!=true)
		exit (EXIT_FAILURE);

	int Menu=0;
	
	//Parameter einlesen
	int c;
	while ((c = getopt(argc, argv, "r:l:p:k:m:n:e:qh")) != -1) {
		switch (c) {
			case 'r':
				vdr_remote_conf = optarg;
			break;
			case 'l':
				xbmc_lircmap_xml = optarg;
			break;
			case 'p':
				pchanger_lirc_conf = optarg;
			break;
			case 'k':
				xbmc_keymap_xml = optarg;
			break;
			case 'n':
				RemoteName[0] = optarg;
			break;
			case 'e':
				RemoteName[1] = optarg;
			break;
			case 'm':
				Menu = atoi( optarg );
			break;
			case 'q':
				Quiet=true;
			break;
			case 'h':
				print_help(); //Hilfe aufrufen
				return 0;
			break;
			default: //Hilfe aufrufen
				print_help(); //Hilfe aufrufen
				wait_key_return();
				exit(EXIT_FAILURE);
		}
	}
	
	while (true) {

		UserInput=flase;	
		while ( UserInput!=true) { 	
	
			UserInput=flase;
		}
		//Aus an vdr senden
		ret=system("svdrpsend HITK Power");
	}

return error;
}
