#!/bin/sh

#DATE=`date +%y%m%d`
DATE=1

# Some rpm checks
RPMVERSION=`rpm --version | tr [A-Z] [a-z] | sed -e 's/[a-z]//g' -e 's/\.//g' -e 's/ //g'`

# rpm version 4 return 40
if [ `expr $RPMVERSION` -lt 100 ]; then
  RPMVERSION=`expr $RPMVERSION \* 10`
fi

if [ `expr $RPMVERSION` -lt 400 ]; then
  RPM_BA="rpm -ba -ta ./xine-ui-0.99.9.tar.bz2"
  RPM_BB="rpm -bb -ta ./xine-ui-0.99.9.tar.bz2"
elif [ `expr $RPMVERSION` -lt 420 ]; then
  RPM_BA="rpm -ta ./xine-ui-0.99.9.tar.bz2 -ba"
  RPM_BB="rpm -ta ./xine-ui-0.99.9.tar.bz2 -bb"
else
  RPM_BA="rpmbuild -ta ./xine-ui-0.99.9.tar.bz2 -ba"
  RPM_BB="rpmbuild -ta ./xine-ui-0.99.9.tar.bz2 -bb"
fi

##VERSION=".."

echo "Creating tarball..."
rm -f config.cache && ./autogen.sh && make dist
rm -rf rpms
mkdir rpms

echo "*****************************************************"
echo
echo "building rpm for xine-ui 0.99.9"
echo 
echo "current architecture:pentium"
echo "rpms will be copied to ./rpms directory"
echo
echo "*****************************************************"

export XINE_BUILD=i586-pc-linux-gnu

eval $RPM_BA

cp /usr/src/redhat/SRPMS/xine-ui-0.99.9-$DATE.src.rpm ./rpms/
mv /usr/src/redhat/RPMS/i386/xine-ui-0.99.9-$DATE.i386.rpm ./rpms/xine-ui-0.99.9-$DATE.i586.rpm
mv /usr/src/redhat/RPMS/i386/xine-ui-aa-0.99.9-$DATE.i386.rpm ./rpms/xine-ui-aa-0.99.9-$DATE.i586.rpm

echo "Done."
