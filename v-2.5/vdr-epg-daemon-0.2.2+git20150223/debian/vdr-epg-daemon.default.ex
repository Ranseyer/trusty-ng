# Defaults for vdr-epg-daemon initscript
# sourced by /etc/init.d/vdr-epg-daemon
# installed at /etc/default/vdr-epg-daemon by the maintainer scripts

#
# This is a POSIX shell fragment
#

# Additional options that are passed to the Daemon.
DAEMON_OPTS=""
