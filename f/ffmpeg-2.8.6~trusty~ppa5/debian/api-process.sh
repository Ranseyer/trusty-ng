#!/bin/bash

cat | sort | grep -v '^$' | while read line
do
    component=`echo $line | awk '{print $1}'`
    api_ver=`echo $line | awk '{print $2}' | sed 's|\.||'`

    echo -e "$component\t$api_ver"
done
