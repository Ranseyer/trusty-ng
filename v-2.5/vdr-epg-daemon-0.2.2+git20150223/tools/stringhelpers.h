#ifndef _STRINGHELPERS_H_
#define _STRINGHELPERS_H_

#include <string>
using namespace std;

string str_replace(const string& search, const string& replace, const string& subject);
string str_cut(const string& search, const string& subject);

#endif