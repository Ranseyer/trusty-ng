#!/bin/bash

# ===============================================
# VARIABLES
# ===============================================

# end users should not edit this section but use
# the installer options, run ./installer.sh -h to
# see them

_dvbtype="sat"
_tbsonly="no"
_buildonly="no"

# this is the root directory of the unpacked zip, not the v4l root dir
_drivroot=$PWD

# some systems have /usr/lib, /lib or both
# to match all these locations we use $_lib_prefix/firmware and
# $_lib_prefix/modules in this script
# the prefix will be set when the prerequisites are checked
_lib_prefix=

# ===============================================
# FUNCTIONS
# ===============================================


function check_prerequisites()
{    
    local bins_fulfilled="yes"
    local dirs_fulfilled="no"
    
    local bins=( 'make' 'tac' 'egrep' 'grep' 'head' 'wget' 'tar' 'sed' ) # 'zip' 'unzip'
    
    local possible_linux_header_dirs=(
        "/lib/modules/$(uname -r)/build"
        "/lib/modules/$(uname -r)/source"
        "/usr/lib/modules/$(uname -r)/build"
        "/usr/lib/modules/$(uname -r)/source"
    )
    
    echo "Checking the availability of the required executables:"
    echo ''
    
    for ((i=0; i < ${#bins[@]}; i++))
    do
        local command="${bins[$i]}"
        
        # print padded text
        printf "%20s" "$command: "
        
        if [ "$(command -v $command)" == "" ];
        then
            echo "no"
            bins_fulfilled="no"
        else
            echo "yes"
        fi
        
    done
   
    echo ''
    echo "Check the availability of the linux headers or sources:"
    echo ''
    
    for ((i=0; i < ${#possible_linux_header_dirs[@]}; i++))
    do
        local dir="${possible_linux_header_dirs[$i]}"
        
        # print padded text
        printf "%50s" "$dir: "
        
        if [ -d "$dir" ];
        then
            echo "yes"
            dirs_fulfilled="yes"
            _lib_prefix=$(echo "$dir" | egrep -o "^(/usr)?/lib")
        else
            echo "no"
        fi
        
    done
    
    echo ''

    
    if [ "$dirs_fulfilled" == "no" ];
    then
        echo "=> You are missing the kernel header files"
        echo "=> Please install one of the following packages"
        echo ""
        echo "    openSUSE:           kernel-devel"
        echo "    Arch Linux:         linux-headers"
        echo "    Debian (64-bit):    kernel-package linux-headers-amd64"   
        #echo "    Debian (32-bit):    kernel-package linux-headers" #(what packages are reallty needed?)
        #echo "    Ubuntu:             "
        echo ""        
    fi

    
    if [ "$bins_fulfilled" == "no" ];
    then
        echo -e "=> Be sure you have the following linux packages installed:"
        echo -e "    * \\e[04mcoreutils\\e[00m"
        echo -e "    * \\e[04mmoreutils\\e[00m"
    fi

    
    # either say yeah or nyeah
    if [[ "$bins_fulfilled" == "yes" && "$dirs_fulfilled" == "yes" ]];
    then
        echo "=> All the prerequisites are fulfilled!"
    else
        echo "=> Some prerequisites are not fullfilled. We stop here!"
        exit 1
    fi  
}


function get_amount_of_cpus()
{   
    ## if the cpuinfo doesn't exist assume one processor
    #if [ ! -f /proc/cpufinfo ];
    #then
    #    echo 1
    #    exit
    #fi
    
    # 1. print the cpuinfo in reverse order and grep the first line containing processor
    # 2. then look for a number
    # TODO: pipe error messages
    
    local amount=$(tac /proc/cpuinfo | egrep processor | head -n 1 | egrep -o "[[:digit:]]+")
    
    if [[ "$amount" == "" || "$amount" == "0" ]];
    then
        amount=1
    else
        amount=$((1+$amount))
    fi

    echo $amount
}


function print_linux_distro_message()
{
    echo "Your Linux distribution is detected as $1"
    echo "Creating a native package now ..."
}


function create_distro_package()
{    
    local distro_detected=0
    
    # try to detect the Linux distribution and create a native package
    if [ "$(command -v pacman)" != "" ];
    then
        print_linux_distro_message "Arch Linux"

        echo "Do you want to continue creating the native package?"
        
        OPTIONS="yes no" 
        select opt in $OPTIONS; do
            case $opt in
                yes)
                    distro_detected=1
                    
                    # flag the installer being run from a packager script
                    # to avoid cycles
                    export TBSINSTALLRUNFROMPACMAN=1
                    
                    cd PKGBUILD
                    #makepkg -sfi
                    makepkg -sf -p PKGBUILD_complete-v4l
                    
                    # stop the 'manual' compiling & installing
                    exit 0;
                    ;;
                no)
                    echo "Continue to scan other distributions"
                    break
                    ;;
                *)
                    echo "Bad option" 
                    exit;
            esac
        done
    fi
    
    if [ $distro_detected == 0 ];
    then
        echo "No other distributions detected. Continue the default installation process";
    fi
}


function unpack_driver_source() {
    
    cd driver
    [ -d linux-tbs-drivers ] && rm -rf linux-tbs-drivers
    tar xjvf linux-tbs-drivers.tar.bz2
    cd linux-tbs-drivers
}


function do_backup()
{
    cd "$_drivroot"
    
    local ok=
    
    if [ ! -d "${_lib_prefix}/firmware" ];
    then
        echo "The directory ${_lib_prefix}/firmware doesn't exist"
        ok="no"
    else
        if [ ! -f firmware.zip ];
        then
            zip -r firmware.zip ${_lib_prefix}/firmware
        fi
    fi

    if [ ! -d "${_lib_prefix}/modules/$(uname -r)/kernel/drivers/media" ];
    then
        echo "The directory ${_lib_prefix}/modules/$(uname -r)/kernel/drivers/media doesn't exist"
        ok="no"
    else
        if [ ! -f media.zip ];
        then
            zip -r media.zip ${_lib_prefix}/modules/$(uname -r)/kernel/drivers/media
        fi
    fi
    
    [[ $ok == "no" ]] && sleep 5
    
    cd -
}


# ===============================================
# MAIN
# ===============================================


while getopts “hbctul” OPTION
do
    case $OPTION in
        h)
            #echo "installer.sh [-h] [-b] [-c] [-t]"
            echo "installer.sh [-h] [-b]"
            echo "Options:"
            echo "  -h print the usage"
            echo "  -b build but do not install the drivers"
            #echo "  -c build the DVB-C version"
            #echo "  -t build only the TBS modules and skip other modules provided by v4l-dvb"
            exit 1
            ;;
        b)
            _buildonly="yes"
            echo "The drivers are only built but not installed"
            ;;
        #c)
        #    _dvbtype="cable"
        #    ;;
        #t)
        #    _tbsonly="yes"
        #    echo "Only the TBS drivers are built"
        #    ;;
        ?)
            : # nothing to parse
    esac
done


check_prerequisites

# if the flat is 1 it means this script was called by a packager script
# and thus create_distro_package must be skipped then
if [ "$TBSINSTALLRUNFROMPACMAN" != 1 ];
then
    # do not create a distribution package
    :
    #create_distro_package
fi

unpack_driver_source

  
_arch=$(uname -m)
case $_dvbtype in
    cable)
      case $_arch in
        i686)
          case $(uname -r) in
            2.6.*)
              ./v4l/tbs-dvbc-x86.sh
              ;;
            3.*)
              ./v4l/tbs-dvbc-x86_r3.sh
              ;;
          esac
          ;;
        x86_64)
          ./v4l/tbs-dvbc-x86_64.sh
          ;;
      esac
      ;;
    sat)
      case $_arch in
        i686)
          case $(uname -r) in
            2.6.*)
              ./v4l/tbs-x86.sh
              ;;
            3.*)
              ./v4l/tbs-x86_r3.sh
              ;;
          esac
          ;;
        x86_64)
          ./v4l/tbs-x86_64.sh
          ;;
      esac
      ;;
    esac

# build the tbs drivers only if desired
if [ "$_tbsonly" == "yes" ];
then
    cp ../../config_tbsonly v4l/.config
fi

# doesn't compile
#sed -i 's/CONFIG_DVB_TBS6982FE=m/#CONFIG_DVB_TBS6982FE=m/' driver/linux-tbs-drivers/v4l/.config

# check_prerequisites must have been called somewhere before!
#do_backup

# the build process creates (#cpus + 1) jobs
if [ "${_buildonly}" == "yes" ];
then
    make --jobs=$(get_amount_of_cpus)
else
    make --jobs=$(get_amount_of_cpus) \
        && { echo "Now installing ..."; sudo rm -rf /lib/modules/$(uname -r)/kernel/drivers/media; sudo make install; } \
        || { echo "The installation failed"; exit 1; }
    
    # do not copy the firmware if the installer is run from the package manager build system
    if [ "$TBSINSTALLRUNFROMPACMAN" != 1 ];
    then
        sudo cp ${_drivroot}/firmware/*.fw /lib/firmware
    fi
fi


# if [ -f ${_drivroot}/media.zip ] && [ -f ${_drivroot}/firmware.zip ];
#if test -f ${_drivroot}/media.zip && test -f ${_drivroot}/firmware.zip;

#if [[ -f ${_drivroot}/media.zip && -f ${_drivroot}/firmware.zip ]];
#then
#    cat <<EOM
#---------------------------------------------------------------------------------
#A backup of these directories got created before the installer copied any files:
#
#  * firmware.zip <-- ${_lib_prefix}/firmware
#  * media.zip    <-- ${_lib_prefix}/modules/$(uname -r)/kernel/drivers/media
#
#This backup is also done if you just built the drivers with './installer -b'. This
#allows you to restore the initial state no matter you installed the drivers and
#firmware manually or automatically by the installer anytime.
#
#Important: if you download the installer from the web again later and run a new
#           instance then its initial state is what happened ever before. If you
#           ran another installer previously then you backup its copied files too.
#
#In the case you need to restore the initial state unpack the zip files. As root
#copy their content back to your system and run
#
#  sudo depmod -a
#---------------------------------------------------------------------------------
#EOM
#else
#    echo "No backup could be created."
#fi
