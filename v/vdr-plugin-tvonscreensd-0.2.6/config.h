/*
 * config.h
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: config.h,v 1.7 2004/11/23 14:21:24 schmitzj Exp $
 *
 */

#ifndef TVONSCREENSD_CONFIG_H
#define TVONSCREENSD_CONFIG_H

#include <vdr/config.h>
#include <vdr/menuitems.h>

class tvonscreensdConfig
{
public:

	tvonscreensdConfig(void);
	~tvonscreensdConfig();
	bool SetupParse(const char *Name, const char *Value);
	bool ProcessArgs(int argc, char *argv[]);
	const char *CommandLineHelp(void);

//  char showChannels[];
	int showLogos;
	int XLfonts;
	int noInfoLine;
	int showChannels;
	int bwlogos;
	int colorworkaround;
	int usertime1;
	int usertime2;
	int usertime3;
	int thenshownextday;
	int showsearchinitiator;

	char *logos;
	char *vdradminfile;
};

extern tvonscreensdConfig tvonscreensdCfg;

class tvonscreensdConfigPage : public cMenuSetupPage
{
private:
	tvonscreensdConfig m_NewConfig;
	
protected:
	virtual void Store(void);

public:
	tvonscreensdConfigPage(void);
	virtual ~tvonscreensdConfigPage();
};

#endif 


