/*
 * timer.c: EPG2VDR plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#include <vdr/tools.h>

#include "update.h"

//***************************************************************************
// Update Timer
//***************************************************************************

int cUpdate::updateTimer()
{
   cMutexLock lock(&timerMutex);

   tell(1, "Updating timer");

   timerDb->deleteWhere("STATE = 'D'");

   // remove deleted timers

   timerDb->clear();
   timerDb->setValue(cTableTimers::fiVdrUuid, EPG2VDRConfig.uuid);

   for (int f = selectMyTimer->find(); f; f = selectMyTimer->fetch())
   {
      int exist = no;

      for (cTimer* t = Timers.First(); t; t = Timers.Next(t)) 
      {
         if (t->StartTime() == timerDb->getIntValue(cTableTimers::fiStartTime) &&
             strcmp(t->Channel()->GetChannelID().ToString(), timerDb->getStrValue(cTableTimers::fiChannelId)) == 0)
         {
            exist = yes;
            break;
         }
      }

      if (!exist)
      {
         timerDb->setValue(cTableTimers::fiState, "D");
         timerDb->update();
      }
   }

   // update timers

   for (cTimer* t = Timers.First(); t; t = Timers.Next(t)) 
   {
      int insert;
      
      if (!t->Event())
      {
         // tell(0, "Fatal: Found timer without event!");
         continue;
      }

      cString channelId = t->Event()->ChannelID().ToString();

      timerDb->clear();
      timerDb->setValue(cTableTimers::fiEventId, (long)t->Event()->EventID());
      timerDb->setValue(cTableTimers::fiChannelId, channelId);
      timerDb->setValue(cTableTimers::fiVdrUuid, EPG2VDRConfig.uuid);
      
      insert = !timerDb->find();
      
      timerDb->setValue(cTableTimers::fiState, "A");
      timerDb->setValue(cTableTimers::fiStartTime, t->StartTime());
      timerDb->setValue(cTableTimers::fiEndTime, t->StopTime());

      if (insert)
         timerDb->insert();
      else
         timerDb->update();

      tell(1, "%s timer for event %u", insert ? "Insert" : "Update", t->Event()->EventID());
   }

   timerUpdateTriggered = no;

   return success;
}

//***************************************************************************
// Notifications from VDRs Status Interface
//***************************************************************************
//***************************************************************************
// Timers Change Notification
//***************************************************************************

void cUpdate::TimerChange(const cTimer* Timer, eTimerChange Change)
{
   cMutexLock lock(&timerMutex);

   if (Change == tcMod)
   {
      tell(2, "Timer changed, updating");
      timerUpdateTriggered = yes;
   }
//    else if (Change == tcDel)
//    {
//       if (Timer->Event())
//       {
//          TimerId tid;
//
//          tell(2, "Timer of %u/%s deleted", Timer->Event()->EventID(), 
//               (const char*)Timer->Event()->ChannelID().ToString());
         
//          tid.eventId = Timer->Event()->EventID();
//          strcpy(tid.channelId, (const char*)Timer->Event()->ChannelID().ToString());
//          deletedTimers.push_back(tid);
//       }
//    }
}

//***************************************************************************
// Channel Switch Notification
//***************************************************************************

void cUpdate::ChannelSwitch(const cDevice *Device, int ChannelNumber, bool LiveView)
{
   // to be implemented ...
}
