#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <jansson.h>
#include "../../tools/curlfuncs.h"
#include "themoviedbscraper.h"

using namespace std;

cMovieDBScraper::cMovieDBScraper(string language) {
    apiKey = "abb01b5a277b9c2c60ec0302d83c5ee9";
    this->language = language;
    baseURL = "api.themoviedb.org/3";
    posterSize = "w500";
    backdropSize = "w1280";
    actorthumbSize = "h632";
}

cMovieDBScraper::~cMovieDBScraper() {
}

cMovieDbMovie *cMovieDBScraper::Scrap(string movieName) {
    int movieID = SearchMovie(movieName);
    if (movieID < 1) {
        return NULL;
    }
    cMovieDbMovie *movie = ReadMovie(movieID);
    if (!movie)
        return NULL;
    return movie;
}

bool cMovieDBScraper::Connect(void) {
    stringstream url;
    url << baseURL << "/configuration?api_key=" << apiKey;
    string configJSON;
    if (CurlGetUrl(url.str().c_str(), &configJSON)) {
        return parseJSON(configJSON);
    }
    return false;
}

bool cMovieDBScraper::parseJSON(string jsonString) {
    json_t *root;
    json_error_t error;

    root = json_loads(jsonString.c_str(), 0, &error);
    if (!root) {
        return false;
    }
    if(!json_is_object(root)) {
        return false;
    }
    json_t *images;
    images = json_object_get(root, "images");
    if(!json_is_object(images)) {
        return false;
    }
    
    json_t *imgUrl;
    imgUrl = json_object_get(images, "base_url");
    if(!json_is_string(imgUrl)) {
        return false;
    }
    imageUrl = json_string_value(imgUrl);
    json_decref(root);
    return true;
}

int cMovieDBScraper::SearchMovie(string movieName) {
    stringstream url;
    url << baseURL << "/search/movie?api_key=" << apiKey << "&query=" << CurlEscape(movieName.c_str()) << "&language=" << language.c_str();
    string movieJSON;
    int movieID = -1;
    if (CurlGetUrl(url.str().c_str(), &movieJSON)) {
        cMovieDbMovie movie(movieJSON);
        movieID = movie.ParseJSONForMovieId(movieName);
    }
    return movieID;
}

cMovieDbMovie *cMovieDBScraper::ReadMovie(int movieID) {
    stringstream url;
    url << baseURL << "/movie/" << movieID << "?api_key=" << apiKey << "&language=" << language.c_str();
    string movieJSON;
    cMovieDbMovie *movie = NULL;
    if (CurlGetUrl(url.str().c_str(), &movieJSON)) {
        movie = new cMovieDbMovie(movieJSON);
        movie->id = movieID;
        movie->SetBaseUrl(baseURL);
        movie->SetApiKey(apiKey);
        movie->SetPosterBaseUrl(imageUrl + posterSize);
        movie->SetBackdropBaseUrl(imageUrl + backdropSize);
        movie->SetActorBaseUrl(imageUrl + actorthumbSize);
        movie->ParseJSON();
    }
    return movie;
}