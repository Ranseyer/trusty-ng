/*
 * update.h: EPG2VDR plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#ifndef __UPDATE_H
#define __UPDATE_H

#include <mysql/mysql.h>
#include <map>
#include <vector>

#include <vdr/status.h>

#include "lib/common.h"
#include "lib/db.h"
#include "lib/tabledef.h"

#include "epg2vdr.h"

#define EPGDNAME "epgd"

class cEpg2VdrEpgHandler;

//***************************************************************************
// Update
//***************************************************************************

class cUpdate : public cThread, public cStatus
{
   public:

      enum MasterMode
      {
         mmAuto,
         mmYes,
         mmNo,
         
         mmCount
      };
      
      cUpdate(cPluginEPG2VDR* plugin);
      ~cUpdate();

      // interface

      void Stop();
      int isUpdateActive()         { return updateTriggered; }
      int isEpgdBusy()             { return epgdBusy; }
      int isEpgdUpdating()         { return epgdUpdating; }
      time_t getNextEpgdUpdateAt() { return nextEpgdUpdateAt; }
      void triggerUpdate(int reload = no);

   protected:

      virtual void TimerChange(const cTimer* Timer, eTimerChange Change);   // notification from VDRs status interface
      virtual void ChannelSwitch(const cDevice *Device, int ChannelNumber, bool LiveView);

   private:

      struct TimerId
      {
         unsigned int eventId;
         char channelId[100];
      };

      // functions

      int initDb();
      int exitDb();

      void Action(void);
      void checkHandlerRole();
      int updatesPending(int timeout);
      int dbConnected(int force = no) { return connection && (!force || connection->check() == success); }
      int checkConnection(int& timeout);
      int updateTimer();
      int refreshEpg();
      cEvent* createEventFromRow(const cDbRow* row);
      int lookupVdrEventOf(int eId, const char* cId);
      cTimer* getTimerOf(const cEvent* event) const;
      int storePicturesToFs();
      int cleanupPictures();
      int pictureLinkNeeded(const char* linkName);

      tChannelID toChanID(const char* chanIdStr)
      {
         if (isEmpty(chanIdStr))
            return tChannelID::InvalidID;
         
         return tChannelID::FromString(chanIdStr);
      }

      // data

      cDbConnection* connection;
      int loopActive;
      time_t nextEpgdUpdateAt;
      time_t lastUpdateAt;
      time_t lastEventsUpdateAt;
      char* epgimagedir;
      int withutf8;
      cCondVar waitCondition;
      cMutex mutex;
      cMutex timerMutex;
      int updateTriggered;
      int timerUpdateTriggered;
      int fullreload;
      char imageExtension[3+TB];
      int epgdBusy;
      int epgdUpdating;
      Es::State epgdState;
      int eventRefreshOnly;

      cTableEvents* eventsDb;
      cTableFileRefs* fileDb;
      cTableImages* imageDb;
      cTableImageRefs* imageRefDb;
      cTableEpisodes* episodeDb;
      cTableChannelMap* mapDb;
      cTableTimers* timerDb;
      cTableVdrs* vdrDb;
      cTableComponents* compDb;

      cDbStatement* selectMasterVdr;
      cDbStatement* selectAllImages;
      cDbStatement* selectUpdEvents;
      cDbStatement* selectAllChannels;
      cDbStatement* selectAllVdrChannels;
      cDbStatement* selectComponentsOf;
      cDbStatement* deleteTimer;
      cDbStatement* selectMyTimer;

      cDbValue vdrEvtId;
      cDbValue extEvtId;
      cDbValue vdrStartTime;
      cDbValue extChannelId;
      cDbValue imageUpdSp;
      cDbValue imageSize;
      cDbValue masterId;

      cEpg2VdrEpgHandler* epgHandler;

      vector<TimerId> deletedTimers;
};

//***************************************************************************
#endif //__UPDATE_H
