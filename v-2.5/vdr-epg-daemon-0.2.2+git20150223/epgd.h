/*
 * epgd.h
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#ifndef __EPGD_H
#define __EPGD_H

#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>
#include <libexslt/exslt.h>

#include <unistd.h>

#include <vector>

#include "lib/common.h"
#include "lib/config.h"
#include "lib/db.h"
#include "lib/tabledef.h"

#include "series.h"
#include "levenshtein.h"

#include "tvdbmanager.h"
#include "moviedbmanager.h"

#define confDirDefault "/etc/epgd"

#define VERSION      "0.2.2"
#define VERSION_DATE "01.02.2015"
#define DB_API       4

extern const char* confDir;

//***************************************************************************
// Statistic
//***************************************************************************

struct Statistic
{
   int bytes;
   int files;
   int rejected;
   int nonUpdates;
};

class cEpgd;
typedef cEpgd cUpdate;  // historical, to be removed

//***************************************************************************
// Plugin
//***************************************************************************

class Plugin
{
   public:

      Plugin()          { obj = 0; utf8 = yes; }
      virtual ~Plugin() {}

      virtual int init(cEpgd* aObject, int aUtf8)
      { 
         obj = aObject; 
         utf8 = aUtf8;
         return success; 
      }

      virtual int exit()   { return done; }
      virtual int initDb() { return done; }
      virtual int exitDb() { return done; }

      virtual int atConfigItem(const char* Name, const char* Value) = 0;      
      virtual const char* getSource() = 0;
      virtual int hasSource(const char* source) { return strcmp(getSource(), source) == 0; }

      virtual int getPicture(const char* imagename, const char* fileRef, MemoryStruct* data) = 0;
      virtual int processDay(int day, int fullupdate, Statistic* stat) = 0;

      virtual int cleanupBefore()  { return done; }
      virtual int cleanupAfter()   { return done; }

      virtual int ready() = 0;

   protected:

      cEpgd* obj;
      int utf8;
};

//***************************************************************************
// Plugin Loader
//***************************************************************************

class PluginLoader
{
   public:

      PluginLoader(const char* name, Plugin* p = 0);
      virtual ~PluginLoader();

      int load();
      Plugin* getPlugin()    { return plugin; }

   private:

      char* fileName;
      void* handle;
      Plugin* plugin;
};

//***************************************************************************
// Configuration (#TODO move to lib)
//***************************************************************************

class Configuration
{
   public:

      virtual int atConfigItem(const char* Name, const char* Value) = 0;

      virtual int readConfig()
      {
         int count = 0;
         FILE* f;
         char* line = 0;
         size_t size = 0;
         char* value;
         char* name;
         char* fileName;
         
         asprintf(&fileName, "%s/epgd.conf", confDir);
         
         if (access(fileName, F_OK) != 0)
         {
            fprintf(stderr, "Cannot access configuration file '%s'\n", fileName);
            free(fileName);
            return fail;
         }
         
         f = fopen(fileName, "r");
         
         while (getline(&line, &size, f) > 0)
         {
            char* p = strchr(line, '#');
            if (p) *p = 0;
            
            allTrim(line);
            
            if (isEmpty(line))
               continue;
            
            if (!(value = strchr(line, '=')))
               continue;
            
            *value = 0;
            value++;
            lTrim(value);
            name = line;
            allTrim(name);
            
            if (atConfigItem(name, value) != success)
            {
               fprintf(stderr, "Found unexpected parameter '%s', aborting\n", name);
               free(fileName);
               return fail;
            }
            
            count++;
         }
         
         free(line);
         fclose(f);
         
         tell(0, "Read %d option from %s", count , fileName);
         
         free(fileName);
         
         return success;
      }
};

//***************************************************************************
// EPG Deamon
//***************************************************************************

class cEpgd : public Configuration
{
   public:

      cEpgd();
      virtual ~cEpgd();

      // interface

      int init();
      int initUuid();
      void loop();
      void scheduleAutoUpdate(int wait = 0);
      int doShutDown()    { return shutdown; }
      int atConfigItem(const char* Name, const char* Value);
      int parseEvent(cDbRow* event, xmlNode* node);

      int dbConnected(int force = no) { return connection && (!force || connection->check() == success); }

      // static stuff

      static void downF(int signal)     { tell(0, "Shutdown triggered with signal %d", signal); shutdown = yes; }
      static void triggerF(int aSignal) { trigger = yes; }

      static xmlDocPtr transformXml(const char* buffer, int size, xsltStylesheetPtr stylesheet, const char* fileRef);
      static int storeToFs(MemoryStruct* data, const char* filename, const char* subPath);
      static int loadFromFs(MemoryStruct* data, const char* filename, const char* subPath);
      static int downloadFile(const char* url, int& size, MemoryStruct* data, int timeout = 30, const char* userAgent = "libcurl-agent/1.0");

      // public for plugins access

      cDbConnection* connection;

      cTableEvents* eventsDb;
      cTableComponents* compDb;
      cTableFileRefs* fileDb;
      cTableImages* imageDb;
      cTableImageRefs* imageRefDb;
      cTableEpisodes* episodeDb;
      cTableChannelMap* mapDb;
      cTableVdrs* vdrDb;
      cTableParameters* parameterDb;
      cTableRecordings* recordingsDb;

      cDbStatement* chkDoubleMap;
      cDbStatement* selectAllMap;
      cDbStatement* selectByCompTitle;
      cDbStatement* selectMaxUpdSp;
      cDbStatement* selectDistCompname;
      cDbStatement* selectByCompName;
      cDbStatement* selectByCompNames;
      cDbStatement* updateEpisodeAtEvents;
      cDbStatement* selectMaxMapOrd;
      cDbStatement* countDvbChanges;
      cDbStatement* selectNewRecordings;
      cDbStatement* countNewRecordings;
      cDbStatement* selectRecordingEvent;
      cDbStatement* selectRecOtherClient;
      cDbStatement* selectActiveVdrs;

      cDbValue changeCount;
      cDbValue newRecCount;

      cDbProcedure* procMergeEpg;
      cDbProcedure* procUser;

   private:

      // functions

      int initDb();
      int exitDb();
      int checkProcedure(const char* name, cDBS::ProcType type, cDbProcedure* fp = 0);
      int checkView(const char* name, const char* file);
      int registerMe();

      int loadPlugins();
      int initPlugins();
      int initPluginDb();
      int exitPluginDb();

      void setState(cEpgdState::State state, time_t lastUpdate = 0, int silent = no);

      int doTrigger()                    { return trigger; }
      int triggerVdrs(const char* trg);

      int loadChannelmap();
      int getMaxChannelOrder();
      int updateMapRow(char* extid, const char* source, const char* chan, int merge, int vps);
      int applyChannelmapChanges();
      int checkConnection();

      int getParameter(const char* name, char* value);
      int getParameter(const char* name, long int& value, long int def = 0);
      int setParameter(const char* name, const char* value);
      int setParameter(const char* name, long int  value);

      // work

      int update();
      int cleanupEvents();
      int cleanupPictures();
      int getPictures();
      int storeImageRefs(long evtId, const char* source, const char* images, 
                         const char* ext, const char* fileref);
      int scrapNewEvents();
      int cleanupSeriesAndMovies();
      void scrapNewRecordings(int count);

      // plugin interface

      int cleanupBefore();
      int cleanupAfter();
      int getPicture(const char* source, const char* imagename, 
                     const char* fileRef, MemoryStruct* data);
      int processDay(int day, int fullupdate, Statistic* stat);

      // series

      int evaluateEpisodes();
      int downloadEpisodes();

      // scraper stuff
      
      int initScrapers();
      void exitScrapers();
      bool checkEventsForRecording(int eventId, string channelId, int &seriesId, int &episodeId, int &movieId);
      bool checkRecOtherClients(string uuid, string recPath, int recStart);

      // data

      int withutf8;
      time_t nextUpdateAt;
      time_t lastUpdateAt;
      time_t lastMergeAt;

      int fullupdate;
      int fullreload;

      static int shutdown;
      static int trigger;

      std::vector<PluginLoader*> plugins;

      cTVDBManager* tvdbManager;
      cMovieDBManager* movieDbManager;
};

//***************************************************************************
#endif //__EPGD_H
