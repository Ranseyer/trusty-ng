/*
 * handler.h: EPG2VDR plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 */

#ifndef __HANDLER_H
#define __HANDLER_H

#include "update.h"

//***************************************************************************
// Mutex Try
//***************************************************************************

class cMutexTry
{
   public:

      cMutexTry()
      {
         locked = 0;

         pthread_mutexattr_t attr;
         pthread_mutexattr_init(&attr);
         pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
         pthread_mutex_init(&mutex, &attr);
      }
      
      ~cMutexTry()
      { 
         pthread_mutex_destroy(&mutex); 
      }

      int tryLock()
      {
         if (pthread_mutex_trylock(&mutex) == 0)
         {
            locked++;
            // tell(0, "[%d] got lock (%d) [%p]", cThread::ThreadId(), locked, this);

            return yes;
         }

         // tell(0, "[%d] don't got lock (%d) [%p]", cThread::ThreadId(), locked, this);

         return no;
      }

      void unlock()
      {
         if (locked)
         {
            // tell(0, "[%d] unlock (%d) [%p]", cThread::ThreadId(), locked, this);
            locked--;
            pthread_mutex_unlock(&mutex);
         }
      }
      
   private:

      pthread_mutex_t mutex;
      int locked;
};

//***************************************************************************
// EPG Handler
//***************************************************************************

class cEpgHandlerInstance
{
   public:
      
      cEpgHandlerInstance()
      { 
         initialized = no;
         connection = 0;
         eventsDb = 0;
         compDb = 0;
         mapDb = 0;
         vdrDb = 0;
         endTime = 0;
         updateDelFlg = 0;
         selectDelFlg = 0;
         delCompOf = 0;
         selectMergeSp = 0;

         tell(0, "Init handler instance for thread %d", cThread::ThreadId());
      }

      virtual ~cEpgHandlerInstance() { exitDb(); }

      virtual int dbConnected(int force = no) 
      { 
         return initialized 
            && connection 
            && (!force || connection->check() == success); 
      }

      int checkConnection()
      {
         static int retry = 0;
         
         // check connection
         
         if (!dbConnected(yes))
         {
            // try to connect
            
            tell(0, "Trying to re-connect to database!");
            retry++;
            
            if (initDb() != success)
            {
               exitDb();
               
               return fail;
            }
            
            retry = 0;         
            tell(0, "Connection established successfull!");
         }
         
         return success;
      }

      int isEpgdBusy()
      {
         int busy = no;
         
         if (!dbConnected())
            return true;

         vdrDb->clear();
         vdrDb->setValue(cTableVdrs::fiUuid, EPGDNAME);
         
         if (vdrDb->find())
         {
            if (strcasecmp(vdrDb->getStrValue(cTableVdrs::fiState), "busy (events)") == 0)
               busy = yes;
         }
         
         vdrDb->reset();
         
         return busy;
      }

      int initDb()
      {
         int status = success;

         exitDb();

         connection = new cDbConnection();

         vdrDb = new cTableVdrs(connection);
         if (vdrDb->open() != success) return fail;

         mapDb = new cTableChannelMap(connection);
         if (mapDb->open() != success) return fail;

         eventsDb = new cTableEvents(connection);
         if (eventsDb->open() != success) return fail;
         
         compDb = new cTableComponents(connection);
         if (compDb->open() != success) return fail;

         // prepare statement to get mapsp

         // select 
         //   mergesp from channelmap 
         // where 
         //   source != 'vdr' 
         //   and channelid = ? limit 1

         selectMergeSp = new cDbStatement(mapDb);
         
         selectMergeSp->build("select ");
         selectMergeSp->bind(cTableChannelMap::fiMergeSp, cDBS::bndOut);
         selectMergeSp->build(" from %s where source != 'vdr'", mapDb->TableName());
         selectMergeSp->bind(cTableChannelMap::fiChannelId, cDBS::bndIn |cDBS::bndSet, " and ");
         selectMergeSp->build(" limit 1");

         status += selectMergeSp->prepare();

         // prepare statement to mark wasted DVB events

         // update events set delflg = ?, updsp = ? 
         //   where channelid = ? and source = ? 
         //      and starttime+duration > ? 
         //      and starttime < ? 
         //      and (tableid > ? or (tableid = ? and version <> ?))

         endTime = new cDbValue("starttime+duration", cDBS::ffInt, 10);
         updateDelFlg = new cDbStatement(eventsDb);
         
         updateDelFlg->build("update %s set ", eventsDb->TableName());
         updateDelFlg->bind(cTableEvents::fiDelFlg, cDBS::bndIn |cDBS::bndSet);
         updateDelFlg->bind(cTableEvents::fiUpdFlg, cDBS::bndIn |cDBS::bndSet, ", ");
         updateDelFlg->bind(cTableEvents::fiUpdSp, cDBS::bndIn | cDBS::bndSet, ", ");
         updateDelFlg->build(" where ");
         updateDelFlg->bind(cTableEvents::fiChannelId, cDBS::bndIn | cDBS::bndSet);
         updateDelFlg->bind(cTableEvents::fiSource, cDBS::bndIn | cDBS::bndSet, " and ");
         updateDelFlg->bindCmp(0, endTime, ">" , " and ");         
         updateDelFlg->bindCmp(0, cTableEvents::fiStartTime, 0, "<" ,  " and ");
         updateDelFlg->bindCmp(0, cTableEvents::fiTableId,   0, ">" ,  " and (");
         updateDelFlg->bindCmp(0, cTableEvents::fiTableId,   0, "=" ,  " or (");
         updateDelFlg->bindCmp(0, cTableEvents::fiVersion,   0, "<>" , " and ");
         updateDelFlg->build("));");
         
         status += updateDelFlg->prepare();

         // we need the same as select :(

         selectDelFlg = new cDbStatement(eventsDb);
         
         selectDelFlg->build("select ");
         selectDelFlg->bind(cTableComponents::fiEventId, cDBS::bndOut);
         selectDelFlg->build(" from %s where ", eventsDb->TableName());
         selectDelFlg->bind(cTableEvents::fiChannelId, cDBS::bndIn | cDBS::bndSet);
         selectDelFlg->bind(cTableEvents::fiSource, cDBS::bndIn | cDBS::bndSet, " and ");
         selectDelFlg->bindCmp(0, endTime, ">" , " and ");
         selectDelFlg->bindCmp(0, cTableEvents::fiStartTime, 0, "<" ,  " and ");
         selectDelFlg->bindCmp(0, cTableEvents::fiTableId,   0, ">" ,  " and (");
         selectDelFlg->bindCmp(0, cTableEvents::fiTableId,   0, "=" ,  " or (");
         selectDelFlg->bindCmp(0, cTableEvents::fiVersion,   0, "<>" , " and ");
         selectDelFlg->build("));");
         
         status += selectDelFlg->prepare();

         // -----------------
         // delete from components where eventid = ?;

         delCompOf = new cDbStatement(compDb);
         
         delCompOf->build("delete from %s where ", compDb->TableName());
         delCompOf->bind(cTableComponents::fiEventId, cDBS::bndIn |cDBS::bndSet);
         delCompOf->build(";");         

         status += delCompOf->prepare();

         if (status == success)
         {
            status += updateMemList();
            status += updateExternalIdsMap();
         }

         initialized = yes;
         return status;
      }

      int exitDb()
      {
         initialized = no;

         if (connection)
         {
            delete endTime;       endTime = 0;
            delete updateDelFlg;  updateDelFlg = 0;
            delete selectDelFlg;  selectDelFlg = 0;
            delete delCompOf;     delCompOf = 0;

            delete vdrDb;         vdrDb = 0;
            delete eventsDb;      eventsDb = 0;
            delete compDb;        compDb = 0;
            delete mapDb;         mapDb = 0;
            delete selectMergeSp; selectMergeSp = 0;

            delete connection;    connection = 0;
         }

         return done;
      }

      int updateMemList()
      {
         time_t start = time(0);

         evtMemList.clear();
         
         // select eventid, channelid, version, tableid, delflg
         //   from events where source = 'vdr'
         
         cDbStatement* selectAllVdr = new cDbStatement(eventsDb);
         
         selectAllVdr->build("select ");
         selectAllVdr->bind(cTableEvents::fiEventId, cDBS::bndOut);
         selectAllVdr->bind(cTableEvents::fiChannelId, cDBS::bndOut, ", ");
         selectAllVdr->bind(cTableEvents::fiVersion, cDBS::bndOut, ", ");
         selectAllVdr->bind(cTableEvents::fiTableId, cDBS::bndOut, ", ");
         selectAllVdr->bind(cTableEvents::fiDelFlg, cDBS::bndOut, ", ");
         selectAllVdr->build(" from %s where source = 'vdr'", eventsDb->TableName());
         
         if (selectAllVdr->prepare() != success)
         {
            tell(0, "Aborted reading hashes from db due to prepare error");
            delete selectAllVdr;
            return fail;
         }
         
         tell(1, "Start reading hashes from db");

         eventsDb->clear();
         
         for (int f = selectAllVdr->find(); f; f = selectAllVdr->fetch())
         {
            char evtKey[100];
            
            if (eventsDb->hasValue(cTableEvents::fiDelFlg, "Y"))
               continue;
            
            sprintf(evtKey, "%ld:%s", 
                    eventsDb->getIntValue(cTableEvents::fiEventId),
                    eventsDb->getStrValue(cTableEvents::fiChannelId));
            
            evtMemList[evtKey].version = eventsDb->getIntValue(cTableEvents::fiVersion);
            evtMemList[evtKey].tableid = eventsDb->getIntValue(cTableEvents::fiTableId);
         }
         
         selectAllVdr->freeResult();
         delete selectAllVdr;
         
         tell(1, "Finished reading hashes from db, got %d hashes (in %ld seconds)", 
              (int)evtMemList.size(), time(0)-start);
         
         return success;
      }

      //***************************************************************************
      // NOEPG feature - so we don't need the noepg plugin
      //***************************************************************************

      virtual bool IgnoreChannel(const cChannel* Channel)
      { 
         static time_t nextRetryAt = time(0);
         LogDuration l("IgnoreChannel", 5);

         // if this method is called the channel is 
         // for the db an we have the active role!
         // (already checked by cEpg2VdrEpgHandler)

         // check connection

         if (!dbConnected() && time(0) < nextRetryAt)
            return true;
         
         if (checkConnection() != success || externIdMap.size() < 1)
         {
            nextRetryAt = time(0) + 60;
            return true;
         }

         return false;
      }

      //***************************************************************************
      // Transaction Stuff
      //***************************************************************************

      virtual bool BeginSegmentTransfer(const cChannel *Channel, bool OnlyRunningStatus)
      {
         // inital die channelid setzen

         channleId = Channel->GetChannelID();

         // start transaction
         
         // if (!OnlyRunningStatus && dbConnected() && getExternalIdOfChannel(&channleId) != "")
         //   connection->startTransaction();

         return false; 
      }

      //***************************************************************************
      // Handled Externally
      //   hier wird festgelegt ob das Event via VDR im EPG landen soll
      //***************************************************************************

      virtual bool HandledExternally(const cChannel* Channel)
      {
         LogDuration l("HandledExternally", 5);

         if (dbConnected() && getExternalIdOfChannel(&channleId) != "")
            return true;
         
         return false;
      }

      virtual bool EndSegmentTransfer(bool Modified, bool OnlyRunningStatus)
      {
         if (OnlyRunningStatus || !dbConnected() || !connection->inTransaction())
            return false;

         if (Modified) 
            connection->commit();
         else
            connection->rollback();

         if (EPG2VDRConfig.loglevel > 2)
            connection->showStat("handler");

         return false; 
      }

      //***************************************************************************
      // Is Update
      //***************************************************************************

      virtual bool IsUpdate(tEventID EventID, time_t StartTime, uchar TableID, uchar Version) 
      {
         char evtKey[100];
         LogDuration l("IsUpdate", 5);

         if (!dbConnected())
            return false;

         if (!isZero(getExternalIdOfChannel(&channleId).c_str()) && StartTime > time(0) + 4 * tmeSecondsPerDay)
            return false;

         sprintf(evtKey, "%ld:%s", (long)EventID, (const char*)channleId.ToString());

         if (evtMemList.find(evtKey) == evtMemList.end())
         {
            tell(4, "Handle insert of event %d for channel '%s'", 
                 EventID, (const char*)channleId.ToString());

            if (!connection->inTransaction())
               connection->startTransaction();

            return true;
         }

         uchar oldTableId = ::max(uchar(evtMemList[evtKey].tableid), uchar(0x4E)); 
         
         // skip if old tid is already lower
         
         if (oldTableId < TableID)
         {
            tell(4, "Ignoring update with old tableid for event '%s'", evtKey);
            return false;
         }
         
         // skip if version an tid identical

         if (oldTableId == TableID && evtMemList[evtKey].version == Version)
         {
            tell(4, "Ignoring 'non' update for event '%s'", evtKey);
            return false;
         }
         
         if (EPG2VDRConfig.loglevel > 3)
            tell(4, "Handle update of event %d [%s] %d/%d - %d/%d", EventID, evtKey, 
                 Version, TableID, 
                 evtMemList[evtKey].version, evtMemList[evtKey].tableid);

         if (!connection->inTransaction())
            connection->startTransaction();

         return true;
      }

      //***************************************************************************
      // Handle Event
      //***************************************************************************

      virtual bool HandleEvent(cEvent* event)
      {
         if (!dbConnected() || !event || !channleId.Valid())
            return false;

         LogDuration l("HandleEvent", 5);

         // External-ID:
         //     na  -> Kanal nicht konfiguriert -> wird ignoriert
         //    = 0  -> wird vom Sender genommen -> und in der DB abgelegt
         //    > 0  -> echte externe ID -> wird von extern ins epg übertragen, 
         //            das Sender EPG wird nur (zusätzlich) in der DB gehalten

         // Events der Kanaele welche nicht in der map zu finden sind ignorieren

         if (getExternalIdOfChannel(&channleId) == "")
            return false;

         if (!connection->inTransaction())
         {
            tell(0, "Error missing tact in HandleEvent");
            return false;
         }

         string comp;

         eventsDb->clear();
         eventsDb->setValue(cTableEvents::fiEventId, (long)event->EventID());
         eventsDb->setValue(cTableEvents::fiChannelId, channleId.ToString());
         int insert = !eventsDb->find();

         // reinstate ??

         if (eventsDb->hasValue(cTableEvents::fiDelFlg, "Y"))
         {
            char updFlg = Us::usPassthrough;

            mapDb->clear();
            mapDb->setValue(cTableChannelMap::fiChannelId, channleId.ToString());

            if (selectMergeSp->find()) 
            {
               time_t mergesp = mapDb->getIntValue(cTableChannelMap::fiMergeSp);
               long masterid = eventsDb->getIntValue(cTableEvents::fiMasterId);
               long useid = eventsDb->getIntValue(cTableEvents::fiUseId);
               
               if (event->StartTime() > mergesp)
                  updFlg = Us::usRemove;
               else if (event->StartTime() <= mergesp && masterid == useid)
                  updFlg = Us::usActive;
               else if (event->StartTime() <= mergesp && masterid != useid)
                  updFlg = Us::usLink;
            }

            eventsDb->setCharValue(cTableEvents::fiUpdFlg, updFlg); 
            eventsDb->setValue(cTableEvents::fiDelFlg, 0, 0);        // set to NULL
         }

         if (!insert && abs(event->StartTime() - eventsDb->getIntValue(cTableEvents::fiStartTime)) > 6*tmeSecondsPerHour)
         {
            tell(3, "Info: Start time of %d/%s - '%s' moved %ld hours from %s to %s - '%s'",
                 event->EventID(), (const char*)channleId.ToString(),
                 eventsDb->getStrValue(cTableEvents::fiTitle),
                 (event->StartTime() - eventsDb->getIntValue(cTableEvents::fiStartTime)) / tmeSecondsPerHour,
                 l2pTime(eventsDb->getIntValue(cTableEvents::fiStartTime)).c_str(),
                 l2pTime(event->StartTime()).c_str(), 
                 event->Title());
         }

         time_t end = event->StartTime() + event->Duration();

         if (!insert 
             && end < time(0) - 2*tmeSecondsPerHour 
             && eventsDb->getIntValue(cTableEvents::fiStartTime) >  time(0)
             && event->StartTime() < eventsDb->getIntValue(cTableEvents::fiStartTime))
         {
            tell(0, "Got update of %d/%s with startime more than 2h in past "
                 "(%s/%d) before (%s), ignoring update, set delflg instead",
                 event->EventID(), (const char*)channleId.ToString(),
                 l2pTime(event->StartTime()).c_str(), event->Duration(),
                 l2pTime(eventsDb->getIntValue(cTableEvents::fiStartTime)).c_str());

            eventsDb->setValue(cTableEvents::fiDelFlg, "Y");
            eventsDb->setCharValue(cTableEvents::fiUpdFlg, Us::usDelete);
         }
         else
         {
            eventsDb->setValue(cTableEvents::fiStartTime, event->StartTime());
         }

         eventsDb->setValue(cTableEvents::fiSource, "vdr");
         eventsDb->setValue(cTableEvents::fiTableId, event->TableID());
         eventsDb->setValue(cTableEvents::fiVersion, event->Version());
         eventsDb->setValue(cTableEvents::fiTitle, event->Title());
         eventsDb->setValue(cTableEvents::fiLongDescription, event->Description());
         eventsDb->setValue(cTableEvents::fiDuration, event->Duration());
         eventsDb->setValue(cTableEvents::fiParentalRating, event->ParentalRating());
         eventsDb->setValue(cTableEvents::fiVps, event->Vps());

         if (!isEmpty(event->ShortText()))
            eventsDb->setValue(cTableEvents::fiShortText, event->ShortText());

         // components ..

         compDb->clear();
         compDb->setValue(cTableComponents::fiEventId, (long)event->EventID());
         delCompOf->execute();

         if (event->Components()) 
         {
            for (int i = 0; i < event->Components()->NumComponents(); i++) 
            {
               tComponent* p = event->Components()->Component(i);

               compDb->clear();
               compDb->setValue(cTableComponents::fiEventId, (long)event->EventID());
               compDb->setValue(cTableComponents::fiChannelId, channleId.ToString());
               compDb->setValue(cTableComponents::fiStream, p->stream);
               compDb->setValue(cTableComponents::fiType, p->type);
               compDb->setValue(cTableComponents::fiLang, p->language);
               compDb->setValue(cTableComponents::fiDescription, p->description ? p->description : "");
               compDb->store();
            }
         }

         // compressed ..

         if (event->Title())
         {
            comp = event->Title();
            prepareCompressed(comp);
            eventsDb->setValue(cTableEvents::fiCompTitle, comp.c_str());
         }

         if (!isEmpty(event->ShortText()))
         {
            comp = event->ShortText();
            prepareCompressed(comp);
            eventsDb->setValue(cTableEvents::fiCompShortText, comp.c_str());
         }

         if (insert)
         {
            eventsDb->setValue(cTableEvents::fiUseId, 0L);
            eventsDb->setCharValue(cTableEvents::fiUpdFlg, Us::usPassthrough); // default (vdr:000 events)

            mapDb->clear();
            mapDb->setValue(cTableChannelMap::fiChannelId, channleId.ToString());

            if (selectMergeSp->find())
            {
               // vdr event for merge with external event

               time_t mergesp = mapDb->getIntValue(cTableChannelMap::fiMergeSp);
               eventsDb->setCharValue(cTableEvents::fiUpdFlg, 
                                      event->StartTime() > mergesp ? Us::usInactive : Us::usActive);
            }

            eventsDb->insert();
         }
         else
         {
            eventsDb->update();
         }

         // update hash map

         char evtKey[100];

         sprintf(evtKey, "%ld:%s", (long)event->EventID(), (const char*)channleId.ToString());

         evtMemList[evtKey].version = event->Version();
         evtMemList[evtKey].tableid = event->TableID();

         return true;
      }

      //***************************************************************************
      // Drop Outdated
      //***************************************************************************

      virtual bool DropOutdated(cSchedule* Schedule, time_t SegmentStart, 
                                time_t SegmentEnd, uchar TableID, uchar Version) 
      {
         // we handle only vdr events here (provided by DVB)

         if (!dbConnected() || getExternalIdOfChannel(&channleId) == "")
            return false;

         if (!connection->inTransaction())
         {
            tell(0, "Error missing tact DropOutdated");
            return false;
         }

         if (SegmentStart <= 0 || SegmentEnd <= 0)
            return false;

         LogDuration l("DropOutdated", 5);

         eventsDb->clear();
         eventsDb->setValue(cTableEvents::fiChannelId, Schedule->ChannelID().ToString());
         eventsDb->setValue(cTableEvents::fiSource, "vdr");
         eventsDb->setValue(cTableEvents::fiUpdSp, time(0));
         eventsDb->setValue(cTableEvents::fiStartTime, SegmentEnd);
         eventsDb->setValue(cTableEvents::fiTableId, TableID);
         eventsDb->setValue(cTableEvents::fiVersion, Version);
         endTime->setValue(SegmentStart);

         // remove segment from cache

         for (int f = selectDelFlg->find(); f; f = selectDelFlg->fetch())
         {
            char evtKey[100];

            sprintf(evtKey, "%ld:%s", 
                    eventsDb->getIntValue(cTableEvents::fiEventId),
                    eventsDb->getStrValue(cTableEvents::fiChannelId));

            evtMemList.erase(evtKey);
         }

         selectDelFlg->freeResult();

         eventsDb->setValue(cTableEvents::fiDelFlg, "Y");
         eventsDb->setCharValue(cTableEvents::fiUpdFlg, Us::usDelete);

         // mark segment as deleted

         updateDelFlg->execute();

         return true;
      }

   private:
      
      string getExternalIdOfChannel(tChannelID* channelId)
      {
         char* id = 0;
         string extid = "";

         if (externIdMap.size() < 1)
            return "";

         id = strdup(channelId->ToString());

         if (externIdMap.find(id) != externIdMap.end())
            extid = externIdMap[id];

         free(id);

         return extid;
      }

      int updateExternalIdsMap()
      {
         externIdMap.clear();
         tell(1, "Start reading external ids from db");
         mapDb->clear();

         // select extid, channelid
         //   from channlemap
         
         cDbStatement* selectAll = new cDbStatement(mapDb);
         
         selectAll->build("select ");
         selectAll->bind(cTableChannelMap::fiExternalId, cDBS::bndOut);
         selectAll->bind(cTableChannelMap::fiChannelId, cDBS::bndOut, ", ");
         selectAll->build(" from %s", mapDb->TableName());

         if (selectAll->prepare() != success)
         {
            tell(0, "Reading external id's from db aborted due to prepare error");
            delete selectAll;
            return fail;
         }
         
         for (int f = selectAll->find(); f; f = selectAll->fetch())
         {
            const char* extid = mapDb->getStrValue(cTableChannelMap::fiExternalId);
            const char* chan = mapDb->getStrValue(cTableChannelMap::fiChannelId);

            externIdMap[chan] = extid;
         }

         tell(1, "Finished reading external id's from db, got %d id's", 
              (int)externIdMap.size());

         selectAll->freeResult();
         delete selectAll;

         return success;
      }

      struct MemMap
      {
         int version;
         int tableid;
      };

      map<string,MemMap> evtMemList;
      map<string,string> externIdMap;
      tChannelID channleId;

      int initialized;
      cDbConnection* connection;

      cTableEvents* eventsDb;
      cTableChannelMap* mapDb;
      cTableVdrs* vdrDb;
      cTableComponents* compDb;

      cDbValue* endTime;
      cDbStatement* updateDelFlg;
      cDbStatement* selectDelFlg;
      cDbStatement* delCompOf;
      cDbStatement* selectMergeSp;
      cUpdate* update;
};

//***************************************************************************
// cEpg2VdrEpgHandler
//***************************************************************************

class cEpg2VdrEpgHandler : public cEpgHandler
{
   public:
      
      cEpg2VdrEpgHandler(cUpdate* aUpdate) : cEpgHandler() { active = no; }

      ~cEpg2VdrEpgHandler()
      {
         map<tThreadId,cEpgHandlerInstance*>::iterator it;

         for (it = handler.begin(); it != handler.end(); ++it)
         {
            delete handler[it->first];
            handler[it->first] = 0;
         }
      }

      int getActive()           { return active; }
      void setActive(int state) { active = state; }

      //***************************************************************************
      // Ignore Channel
      //***************************************************************************

      virtual bool IgnoreChannel(const cChannel* Channel)
      {
         // IgnoreChannel ist der erste Anlaufpunkt des EIT handlers, 
         // wird hier ignoriert bricht die Verarbeitung des Kanals direkt ab

         tChannelID tmp = Channel->GetChannelID();

         if (externIdMap.size() < 1)
            return true;

         // Kanäle welche nicht in der map konfiguriert sind (na) werden nicht in der DB verwaltet 
         // abhängig von blacklist durchgelassen oder ignoriert

         if (getExternalIdOfChannel(&tmp) == "")
            return EPG2VDRConfig.blacklist;

         // ingnore - wenn nicht aktiv

         if (!active)
            return true;

         // vom handler ignoriert (wegen db Problemen etc.)?

         if (getHandler()->IgnoreChannel(Channel))
            return true;

         // solange der handler beschäftigt (wir den lock nicht bekommen) ist erst mal ignorieren

         if (!handlerMutex.tryLock())
            return true;

         return false;
      }

      virtual bool HandledExternally(const cChannel* Channel)
      { 
         cEpgHandlerInstance* h = getHandler();
         return h->HandledExternally(Channel);
      }

      virtual bool BeginSegmentTransfer(const cChannel *Channel, bool OnlyRunningStatus)
      { 
         cEpgHandlerInstance* h = getHandler();
         return h->BeginSegmentTransfer(Channel, OnlyRunningStatus);
      }

      virtual bool EndSegmentTransfer(bool Modified, bool OnlyRunningStatus)
      { 
         handlerMutex.unlock();

         cEpgHandlerInstance* h = getHandler();
         return h->EndSegmentTransfer(Modified, OnlyRunningStatus);
      }

      virtual bool IsUpdate(tEventID EventID, time_t StartTime, uchar TableID, uchar Version) 
      {

         cEpgHandlerInstance* h = getHandler();
         return h->IsUpdate(EventID, StartTime, TableID, Version);
      }

      virtual bool HandleEvent(cEvent* event)
      {
         cEpgHandlerInstance* h = getHandler();
         return h->HandleEvent(event);
      }

      virtual bool DropOutdated(cSchedule* Schedule, time_t SegmentStart, 
                                time_t SegmentEnd, uchar TableID, uchar Version) 
      { 
         cEpgHandlerInstance* h = getHandler();
         return h->DropOutdated(Schedule, SegmentStart, SegmentEnd, TableID, Version);
      }

      int updateExternalIdsMap(cTableChannelMap* mapDb)
      {
         cMutexLock lock(&mapMutex);

         externIdMap.clear();
         tell(1, "Start reading external ids from db");
         mapDb->clear();

         // select extid, channelid
         //   from channlemap
         
         cDbStatement* selectAll = new cDbStatement(mapDb);
         
         selectAll->build("select ");
         selectAll->bind(cTableChannelMap::fiExternalId, cDBS::bndOut);
         selectAll->bind(cTableChannelMap::fiChannelId, cDBS::bndOut, ", ");
         selectAll->bind(cTableChannelMap::fiSource, cDBS::bndOut, ", ");
         selectAll->bind(cTableChannelMap::fiChannelName, cDBS::bndOut, ", ");
         selectAll->bind(cTableChannelMap::fiMerge, cDBS::bndOut, ", ");
         selectAll->build(" from %s", mapDb->TableName());

         if (selectAll->prepare() != success)
         {
            tell(0, "Reading external id's from db aborted due to prepare error");
            delete selectAll;
            return fail;
         }
         
         for (int f = selectAll->find(); f; f = selectAll->fetch())
         {
            string extid = mapDb->getStrValue(cTableChannelMap::fiExternalId);
            int merge = mapDb->getIntValue(cTableChannelMap::fiMerge);

            const char* strChannelId = mapDb->getStrValue(cTableChannelMap::fiChannelId);
            cChannel* channel = Channels.GetByChannelID(tChannelID::FromString(strChannelId));

            // update channelname in channelmap
            
            if (channel && !mapDb->hasValue(cTableChannelMap::fiChannelName, channel->Name()))
            {
               mapDb->find();              // get all fields from table (needed for update)!
               mapDb->setValue(cTableChannelMap::fiChannelName, channel->Name());
               mapDb->update();
               mapDb->reset();
            }

            // we should get the merge > 1 channels already via a merge 1 entry of the channelmap!

            if (merge > 1)
               continue;

            // if extid > 0 (no vdr:000 channel) and map not configured disable channel via 'na'

            if (!isZero(extid.c_str()) && !merge)
            {
               tell(1, "Handler ignore channel '%s' due to merge = 0", strChannelId);
               extid = "";
            }

            // insert into map

            externIdMap[strChannelId] = extid;
         }

         tell(1, "Finished reading external id's from db, got %d id's", 
              (int)externIdMap.size());

         selectAll->freeResult();
         delete selectAll;

         return success;
      }

   private:

      string getExternalIdOfChannel(tChannelID* channelId)
      {
         char* id = 0;
         string extid = "";

         cMutexLock lock(&mapMutex);

         if (externIdMap.size() < 1)
            return "";

         id = strdup(channelId->ToString());

         if (externIdMap.find(id) != externIdMap.end())
            extid = externIdMap[id];

         free(id);

         return extid;
      }

      cEpgHandlerInstance* getHandler()
      {
         if (handler.find(cThread::ThreadId()) == handler.end())
            handler[cThread::ThreadId()] = new cEpgHandlerInstance();
         
         return handler[cThread::ThreadId()];
      }
      
      map<string,string> externIdMap;
      map<tThreadId,cEpgHandlerInstance*> handler;
      int active;
      cMutex mapMutex;
      cMutexTry handlerMutex;
};

//***************************************************************************
#endif // __HANDLER_H
