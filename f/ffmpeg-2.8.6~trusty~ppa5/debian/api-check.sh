#!/bin/bash

api_upstream=`mktemp /tmp/XXXXXXXXXX`
api_package=`mktemp /tmp/XXXXXXXXXX`

./api-process.sh > $api_upstream
./api-list.sh > $api_package

st=`diff -u $api_package $api_upstream`
if [ -z "$st" ]; then
    echo "No changes"
else
    echo "There is changes:"
    st=`diff -u $api_package $api_upstream`
fi

rm -f $api_package $api_upstream

