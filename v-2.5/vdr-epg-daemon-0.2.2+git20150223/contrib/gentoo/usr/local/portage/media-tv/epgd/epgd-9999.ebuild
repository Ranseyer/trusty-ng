# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils git-2

DESCRIPTION="a EPG daemon which fetch the EPG and additional data from various sources"
HOMEPAGE="http://projects.vdr-developer.org/projects/vdr-epg-daemon"
# EGIT_REPO_URI="git://projects.vdr-developer.org/vdr-epg-daemon.git"
: ${EGIT_REPO_URI:=${EPGD_GIT_REPO_URI:-git://projects.vdr-developer.org/vdr-epg-daemon.git}}
: ${EGIT_BRANCH:=${EPGD_GIT_BRANCH:-master}}

SRC_URI=""


LICENSE=""
SLOT="0"
KEYWORDS=""

IUSE="-debug"

DEPEND="dev-vcs/git
        app-arch/libarchive
        net-misc/curl
        dev-libs/libxslt
        dev-libs/libxml2
        >=dev-db/mysql-5.1.70
        dev-libs/libzip"        
       

RDEPEND="${DEPEND}"

src_unpack() {

	git-2_src_unpack || default

}

src_prepare() {
  
       cd "${WORKDIR}/${P}"

	sed -i Make.config -e "s/\/local//"

	if use debug; then
	  sed -i Make.config -e "s/# DEBUG/DEBUG/"
	fi

       EPATCH_OPTS="-p1" 
	for LOCALPATCH in ${EPGD_LOCAL_PATCHES_DIR}/*.{diff,patch}; do
	  test -f "${LOCALPATCH}" && epatch "${LOCALPATCH}"
	done

}

src_install() {

	cd "${WORKDIR}/${P}"
	dobin epgd
	insinto /usr/lib/epgd/plugins || die
	doins $(find -name libepgd-*.so)
	insinto $(mysql_config --plugindir) || die
	doins $(find -name mysql*.so)
       dodir /etc/epgd || die
	fperms 0755 /etc/epgd || die
	newinitd "${FILESDIR}"/epgd.initd epgd || die
	newconfd "${FILESDIR}"/epgd.confd epgd || die
       mkdir -p configs2
       cat configs/channelmap.conf > configs2/channelmap.conf
	find PLUGINS -name channelmap.con* -exec cat {} \; >> configs2/channelmap.conf
	cat configs/epgd.conf > configs2/epgd.conf
       find PLUGINS -name epgd.conf -exec cat {} \; >> configs2/epgd.conf
	insinto /etc/epgd || die 
       doins configs2/* || die
       doins $(find -name *.??l)
	exeinto /usr/bin || die
	doexe "${WORKDIR}/${P}"/scripts/epgd-tool || die

}

pkg_postinst() {
	einfo "Please reffer to the wiki for assistance with the setup"
	einfo "located at http://projects.vdr-developer.org/projects/vdr-epg-daemon/wiki"
	einfo "You can use \"epgd-tool\" for installing the MySQL Database"

}		
