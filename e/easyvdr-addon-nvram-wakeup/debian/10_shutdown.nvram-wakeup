#
# VDR shutdown hook for nvram-wakeup - Tobias Grimm <tg@e-tobi.net>
# ----------------------------------
#
# This shutdown hook sets the wakeup time for the next timer using
# nvram-wakeup. If necessary the shutdown command is modified to
# use a special shutdown strategy.
#

NVRAMCMD="/usr/sbin/nvram-wakeup"
GRUB2_CNF="/boot/grub/grub.cfg"

# read arguments for nvram-wakeup from conf-file
. /etc/vdr/easyvdr-addon-nvram-wakeup.conf

# Defaults:
[ -z "$ENABLED" ]           && export ENABLED="no"
[ -z "$COMMANDLINE" ]       && export COMMANDLINE=""
[ -z "$SPECIALSHUTDOWN" ]   && export SPECIALSHUTDOWN=""
[ -z "$WAKEUP_REGULAR_DAYS" ]      && export WAKEUP_REGULAR_DAYS=0
[ -z "$WAKEUP_REGULAR_TIME" ]      && export WAKEUP_REGULAR_TIME=0
[ -z "$FORCE_REBOOT" ]      && export FORCE_REBOOT="no"

LOG="logger -t easyvdr-addon-nvram-wakeup"

if [ $ENABLED = "no" ] ; then
	$LOG "easyvdr-addon-nvram-wakeup functionality is disabled"
	exit 0
fi

request_reboot() {
	if [ -z "$SPECIALSHUTDOWN" ] ; then
		$LOG "easyvdr-addon-nvram-wakeup: A special shutdown strategy is required but not configured."
		echo "ABORT_MESSAGE=\"no special shutdown configured\""
		exit 1
	else
		# REBOOT_LINE=`grep -s ^menuentry "$GRUB2_CNF" | grep -i -n poweroffhalt | { IFS=: read a b ; echo $a ; }`
		# grub-reboot $((REBOOT_LINE-1))
		grub-reboot easyVDRPowerOff
		echo "SHUTDOWNCMD=\"$SPECIALSHUTDOWN\""
		exit 0
	fi
}

# calculate, at what time the machine should be powered on:

TIMER=$1

if [ $WAKEUP_REGULAR_DAYS -gt 0 ]; then
	REGULAR_TIMER=$((`date -d "$WAKEUP_REGULAR_TIME" +%s` + $WAKEUP_REGULAR_DAYS * 24 * 60 * 60))

	# when no vdr timer is set or vdr timer starts later than regular timer:
	if [ $TIMER -eq 0 ] || [ $TIMER -gt 0 -a $REGULAR_TIMER -lt $TIMER ] ; then
		TIMER=$REGULAR_TIMER
	fi
fi

# set wakeup time and check nvram-wakeup and check result:

$LOG "$NVRAMCMD -ls $TIMER $COMMANDLINE"

$NVRAMCMD -ls $TIMER $COMMANDLINE

case $? in
	0)	# all went ok - new date and time set
		$LOG "easyvdr-addon-nvram-wakeup: everything ok"

		if [ $FORCE_REBOOT = "yes" ] ; then
			$LOG "easyvdr-addon-nvram-wakeup: reboot not needed but forced"
			request_reboot
		fi

		exit 0
	;;
	1)	# all went ok - new date and time set.
		#
		# *** but we need to reboot. ***
		#
		# for some boards this is needed after every change.
		#
		# for some other boards, we only need this after changing the
		# status flag, i.e. from enabled to disabled or the other way.


		# For plan A - (Plan A is not supported anymore---see README)
		#
		# For plan B - (don't forget to install the modified kernel image first)
		#

		$LOG "easyvdr-addon-nvram-wakeup: everything ok - need to reboot first"
		request_reboot
	;;
	*)	# something went wrong
		# don't do anything - just exit with status 1

		$LOG "easyvdr-addon-nvram-wakeup: could not set time, shutdown will be aborted"
		echo "ABORT_MESSAGE=\"nvram-wakeup could not set time\""
		exit 1
	;;
esac
