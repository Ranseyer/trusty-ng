#include "moviedbmanager.h"

extern const char* confDir;

using namespace std;

cMovieDBManager::cMovieDBManager(void) {
    connection = NULL;
    tMovies = NULL;
    tMoviesActor = NULL;
    tMoviesActors = NULL;
    tMovieMedia = NULL;
    tEvents = NULL;
    tRecordings = NULL;
    movieDbScraper = NULL;
    withutf8 = no;
    bytesDownloaded = 0;
    exsltRegisterAll();
    setlocale(LC_CTYPE, "");
    char* lang;
    lang = setlocale(LC_CTYPE, 0);
    if (lang) {
        //tell(0, "Set locale to '%s'", lang);
        if ((strcasestr(lang, "UTF-8") != 0) || (strcasestr(lang, "UTF8") != 0)){
            //tell(0, "detected UTF-8");
            withutf8 = yes;
        }
    } else {
        //tell(0, "Reseting locale for LC_CTYPE failed.");
    }
    string loc = lang;
    size_t index = loc.find_first_of("_");
    string langISO = "";
    if (index > 0) {
        langISO = loc.substr(0, index);
    }
    if (langISO.size() == 2) {
        language = langISO.c_str();
    } else {
        language = "en";
    }
    //tell(0, "using scrapping language %s", language.c_str());
}

cMovieDBManager::~cMovieDBManager() {
    if (movieDbScraper)
        delete movieDbScraper;
    if (tMovies)
        delete tMovies;
    if (tMoviesActor)
        delete tMoviesActor;
    if (tMoviesActors)
        delete tMoviesActors;
    if (tMovieMedia)
        delete tMovieMedia;
    if (tEvents)
        delete tEvents;
    if (tRecordings)
        delete tRecordings;
    FreeCurlLibrary();
}

bool cMovieDBManager::ConnectDatabase(cDbConnection *conn) {
    connection = conn;
    if (!connection)
        return false;
    tMovies = new cTableMovies(connection);
    if (tMovies->open() != success) 
        return false;
    tMoviesActor = new cTableMovieActor(connection);
    if (tMoviesActor->open() != success) 
        return false;
    tMoviesActors = new cTableMovieActors(connection);
    if (tMoviesActors->open() != success) 
        return false;
    tMovieMedia = new cTableMovieMedia(connection);
    if (tMovieMedia->open() != success) 
        return false;
    tEvents = new cTableEvents(connection);
    if (tEvents->open() != success) 
        return false;
    tRecordings = new cTableRecordings(connection);
    if (tRecordings->open() != success) 
        return false;
    return true;
}

bool cMovieDBManager::ConnectScraper(void) {
    movieDbScraper = new cMovieDBScraper(language);
    bool ok = movieDbScraper->Connect();
    return ok;
}

bool cMovieDBManager::GetMoviesFromEPG(vector<sMovieResult> *result) {
    int status = success;
    cDbStatement *selectMovies = new cDbStatement(tEvents);
    selectMovies->build("select ");
    selectMovies->bind(cTableEvents::fiEventId, cDBS::bndOut);
    selectMovies->bind(cTableEvents::fiTitle, cDBS::bndOut, ", ");
    selectMovies->bind(cTableEvents::fiScrSp, cDBS::bndOut, ", ");
    selectMovies->build(" from %s where ", tEvents->TableName());
    selectMovies->build(" category = 'Spielfilm'");
    selectMovies->build(" and %s is null", tEvents->getField(cTableEvents::fiScrSp)->name);
    selectMovies->build(" and updflg in('A','T','C','P')");
    status += selectMovies->prepare();
    if (status != success) {
        delete selectMovies;
        return false;
    }
    for (int found = selectMovies->find(); found; found = selectMovies->fetch()) {
        sMovieResult res;
        res.eventId = tEvents->getIntValue(cTableEvents::fiEventId);
        res.title = tEvents->getStrValue(cTableEvents::fiTitle);
        res.lastScraped = tEvents->getIntValue(cTableEvents::fiScrSp);
        result->push_back(res);
    }
    selectMovies->freeResult();
    delete selectMovies;
    return true;
}

void cMovieDBManager::ProcessMovie(sMovieResult mov) {
    //tell(0, "Checking eventID: %d, Title: %s", mov.eventId, mov.title.c_str());
    
    map<string, int>::iterator hit = alreadyScraped.find(mov.title);
    int movieID = 0;
    if (hit != alreadyScraped.end()) {
        movieID = (int)hit->second;
        if (movieID == 0) {
            //tell(0, "movie %s already scraped and nothing found", mov.title.c_str());
            return;
        } else {
            //tell(0, "found movie %s in cache, id %d", mov.title.c_str(), movieID);
        }
    }
    if (movieID == 0) {
        //check if movie is in database
        movieID = LoadMovieFromDB(mov.title);
        if (movieID != 0) {
            //tell(0, "movie %s already in db, id %d", mov.title.c_str(), movieID);
        } else {
            //scrap movie
            cMovieDbMovie *movie = movieDbScraper->Scrap(mov.title);
            if (movie) {
                movieID = movie->id;
                movie->ReadActors();
                //tell(0, "movie %s successfully scraped, id %d", mov.title.c_str(), movieID);
                SaveMovie(movie);
                delete movie;
            } else {
                //tell(0, "movie %s not found at themoviedb.com", mov.title.c_str());
            }
        }
    }
    alreadyScraped.insert(pair<string, int>(mov.title, movieID));
    //updating event with movie data
    UpdateEvent(mov.eventId, movieID);
}

int cMovieDBManager::LoadMovieFromDB(string title) {
    int status = success;
    int movieID = 0;
    tMovies->clear();
    tMovies->setValue(cTableMovies::fiTitle, title.c_str());
    cDbStatement *select = new cDbStatement(tMovies);
    select->build("select ");
    select->bind(cTableMovies::fiMovieId, cDBS::bndOut);
    select->build(" from %s where ", tMovies->TableName());
    select->bind(cTableMovies::fiTitle, cDBS::bndIn | cDBS::bndSet);
    status += select->prepare();
    if (status != success) {
        delete select;
        return movieID;
    }
    int res = select->find();
    if (res) {
        movieID = tMovies->getIntValue(cTableMovies::fiMovieId); 
    }
    select->freeResult();
    delete select;
    return movieID;
}

void cMovieDBManager::UpdateEvent(int eventID, int movieID) {
    stringstream upd;
    upd << "update " << tEvents->TableName();
    upd << " set scrsp = " << time(0);
    upd << ", scrmovieid = " << movieID;
    upd << " where eventid = " << eventID;
    cDbStatement updStmt(connection, upd.str().c_str());
    updStmt.prepare();
    updStmt.execute();
}

void cMovieDBManager::SaveMovie(cMovieDbMovie *movie) {
    SaveMovieBasics(movie);
    SaveMovieMedia(movie);
    SaveMovieActors(movie);  
}

void cMovieDBManager::SaveMovieBasics(cMovieDbMovie *movie) {
    tMovies->clear();
    tMovies->setValue(cTableMovies::fiMovieId, movie->id);
    tMovies->setValue(cTableMovies::fiTitle, movie->title.c_str());
    tMovies->setValue(cTableMovies::fiOriginalTitle, movie->originalTitle.c_str());
    tMovies->setValue(cTableMovies::fiTagline, movie->tagline.c_str());
    tMovies->setValue(cTableMovies::fiOverview, movie->overview.c_str());
    tMovies->setValue(cTableMovies::fiIsAdult, movie->adult?1:0);
    tMovies->setValue(cTableMovies::fiCollectionId, movie->collectionID);
    tMovies->setValue(cTableMovies::fiCollectionName, movie->collectionName.c_str());
    tMovies->setValue(cTableMovies::fiBudget, movie->budget);
    tMovies->setValue(cTableMovies::fiRevenue, movie->revenue);
    tMovies->setValue(cTableMovies::fiGenres, movie->genres.c_str());
    tMovies->setValue(cTableMovies::fiHomepage, movie->homepage.c_str());
    tMovies->setValue(cTableMovies::fiReleaaseDate, movie->releaseDate.c_str());
    tMovies->setValue(cTableMovies::fiRuntime, movie->runtime);
    tMovies->setValue(cTableMovies::fiPopularity, movie->popularity);
    tMovies->setValue(cTableMovies::fiVoteAverage, movie->voteAverage);
    tMovies->store();
}

void cMovieDBManager::SaveMovieMedia(cMovieDbMovie *movie) {
    MemoryStruct data;
    if (movie->posterPath.size() > 10) {
        tMovieMedia->clear();
        tMovieMedia->setValue(cTableMovieMedia::fiMovieId, movie->id);
        tMovieMedia->setValue(cTableMovieMedia::fiActorId, 0);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaType, mtPoster);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaUrl, movie->posterPath.c_str());
        tMovieMedia->setValue(cTableMovieMedia::fiMediaWidth, movie->posterWidth);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaHeight, movie->posterHeight);
        if (GetPicture(movie->posterPath.c_str(), &data) == success) {
            tMovieMedia->setValue(cTableMovieMedia::fiMediaContent, data.memory, data.size);
            tMovieMedia->store();
        } 
    }
    if (movie->backdropPath.size() > 10) {
        tMovieMedia->clear();
        tMovieMedia->setValue(cTableMovieMedia::fiMovieId, movie->id);
        tMovieMedia->setValue(cTableMovieMedia::fiActorId, 0);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaType, mtFanart);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaUrl, movie->backdropPath.c_str());
        tMovieMedia->setValue(cTableMovieMedia::fiMediaWidth, movie->backdropWidth);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaHeight, movie->backdropHeight);
        if (GetPicture(movie->backdropPath.c_str(), &data) == success) {
            tMovieMedia->setValue(cTableMovieMedia::fiMediaContent, data.memory, data.size);
            tMovieMedia->store();
        } 
    }
    if (movie->collectionPosterPath.size() > 10) {
        tMovieMedia->clear();
        tMovieMedia->setValue(cTableMovieMedia::fiMovieId, movie->id);
        tMovieMedia->setValue(cTableMovieMedia::fiActorId, 0);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaType, mtCollectionPoster);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaUrl, movie->collectionPosterPath.c_str());
        tMovieMedia->setValue(cTableMovieMedia::fiMediaWidth, movie->posterWidth);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaHeight, movie->posterHeight);
        if (GetPicture(movie->collectionPosterPath.c_str(), &data) == success) {
            tMovieMedia->setValue(cTableMovieMedia::fiMediaContent, data.memory, data.size);
            tMovieMedia->store();
        } 
    }
    if (movie->collectionBackdropPath.size() > 10) {
        tMovieMedia->clear();
        tMovieMedia->setValue(cTableMovieMedia::fiMovieId, movie->id);
        tMovieMedia->setValue(cTableMovieMedia::fiActorId, 0);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaType, mtCollectionFanart);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaUrl, movie->collectionBackdropPath.c_str());
        tMovieMedia->setValue(cTableMovieMedia::fiMediaWidth, movie->backdropWidth);
        tMovieMedia->setValue(cTableMovieMedia::fiMediaHeight, movie->backdropHeight);
        if (GetPicture(movie->collectionBackdropPath.c_str(), &data) == success) {
            tMovieMedia->setValue(cTableMovieMedia::fiMediaContent, data.memory, data.size);
            tMovieMedia->store();
        } 
    }
}

void cMovieDBManager::SaveMovieActors(cMovieDbMovie *movie) {
    cMovieDBActor *actor = NULL;
    while(actor = movie->GetActor()) {
        tMoviesActor->clear();
        tMoviesActor->setValue(cTableMovieActor::fiActorId, actor->id);
        tMoviesActor->setValue(cTableMovieActor::fiActorName, actor->name.c_str());
        tMoviesActor->store();
        tMoviesActors->clear();
        tMoviesActors->setValue(cTableMovieActors::fiMovieId, movie->id);
        tMoviesActors->setValue(cTableMovieActors::fiActorId, actor->id);
        tMoviesActors->setValue(cTableMovieActors::fiRole, actor->role.c_str());
        tMoviesActors->store();
        bool mediaExists = LoadMedia(0, actor->id, mtActorThumb);
        if (!mediaExists) {
            tMovieMedia->clear();
            tMovieMedia->setValue(cTableMovieMedia::fiMovieId, 0);
            tMovieMedia->setValue(cTableMovieMedia::fiActorId, actor->id);
            tMovieMedia->setValue(cTableMovieMedia::fiMediaType, mtActorThumb);
            tMovieMedia->setValue(cTableMovieMedia::fiMediaUrl, actor->thumbUrl.c_str());
            tMovieMedia->setValue(cTableMovieMedia::fiMediaWidth, actor->width);
            tMovieMedia->setValue(cTableMovieMedia::fiMediaHeight, actor->height);
            MemoryStruct data;
            if (GetPicture(actor->thumbUrl.c_str(), &data) == success) {
                tMovieMedia->setValue(cTableMovieMedia::fiMediaContent, data.memory, data.size);
                tMovieMedia->store();
            }
        }
    }
}

bool cMovieDBManager::LoadMedia(int movieId, int actorId, int mediaType) {
    tMovieMedia->clear();
    tMovieMedia->setValue(cTableMovieMedia::fiMovieId, movieId);
    tMovieMedia->setValue(cTableMovieMedia::fiActorId, actorId);
    tMovieMedia->setValue(cTableMovieMedia::fiMediaType, mediaType);
    int found = tMovieMedia->find();
    if (found == yes)
        return true;
    return false;
}

int cMovieDBManager::GetPicture(const char* url, MemoryStruct* data) {
    int maxSize = tMovieMedia->getField(cTableMovieMedia::fiMediaContent)->size;
    data->clear();
    int fileSize = 0;
    if (CurlGetUrlMem(url, fileSize, data) == success) {
        bytesDownloaded += fileSize;
        if (fileSize < maxSize)
            return success;
    } 
    return fail;
}

int cMovieDBManager::CleanupMovies(void) {
    int numDelete = 0;
    set<int> activeMovieIds;
    int status = success;
    //fetching movieIds from current events
    cDbStatement *selectMovieIds = new cDbStatement(tEvents);
    selectMovieIds->build("select distinct ");
    selectMovieIds->bind(cTableEvents::fiScrMovieId, cDBS::bndOut);
    selectMovieIds->build(" from %s where ", tEvents->TableName());
    selectMovieIds->build(" %s is not null ", tEvents->getField(cTableEvents::fiScrMovieId)->name);
    selectMovieIds->build(" and %s > 0 ", tEvents->getField(cTableEvents::fiScrMovieId)->name);
    status += selectMovieIds->prepare();
    if (status != success) {
        delete selectMovieIds;
        return numDelete;
    }
    tEvents->clear();

    for (int res = selectMovieIds->find(); res; res = selectMovieIds->fetch()) {
        activeMovieIds.insert(tEvents->getIntValue(cTableEvents::fiScrMovieId));
    }

    selectMovieIds->freeResult();
    delete selectMovieIds;
    //fetching movieIds from recordings
    cDbStatement *selectMovieIdsRec = new cDbStatement(tRecordings);
    selectMovieIdsRec->build("select distinct ");
    selectMovieIdsRec->bind(cTableRecordings::fiMovieId, cDBS::bndOut);
    selectMovieIdsRec->build(" from %s where ", tRecordings->TableName());
    selectMovieIdsRec->build(" %s is not null ", tRecordings->getField(cTableRecordings::fiMovieId)->name);
    selectMovieIdsRec->build(" and %s > 0 ", tRecordings->getField(cTableRecordings::fiMovieId)->name);
    status += selectMovieIdsRec->prepare();
    if (status != success) {
        delete selectMovieIdsRec;
        return numDelete;
    }
    tRecordings->clear();
    for (int res = selectMovieIdsRec->find(); res; res = selectMovieIdsRec->fetch()) {
        activeMovieIds.insert(tRecordings->getIntValue(cTableRecordings::fiMovieId));
    }
    selectMovieIdsRec->freeResult();
    delete selectMovieIdsRec;

    //fetching all movieIds from movie table
    vector<int> storedMovieIds;
    cDbStatement *selectStoredMovieIds = new cDbStatement(tMovies);
    selectStoredMovieIds->build("select ");
    selectStoredMovieIds->bind(cTableMovies::fiMovieId, cDBS::bndOut);
    selectStoredMovieIds->build(" from %s where ", tMovies->TableName());
    selectStoredMovieIds->build(" %s is not null ", tMovies->getField(cTableMovies::fiMovieId)->name);
    selectStoredMovieIds->build(" and %s > 0 ", tMovies->getField(cTableMovies::fiMovieId)->name);
    status += selectStoredMovieIds->prepare();
    if (status != success) {
        delete selectStoredMovieIds;
        return numDelete;
    }
    tMovies->clear();

    for (int res = selectStoredMovieIds->find(); res; res = selectStoredMovieIds->fetch()) {
        storedMovieIds.push_back(tMovies->getIntValue(cTableMovies::fiMovieId));
    }
    selectStoredMovieIds->freeResult();
    delete selectStoredMovieIds;

    numDelete = storedMovieIds.size() - activeMovieIds.size();
    if (numDelete < 1)
        return numDelete;
    for (vector<int>::iterator mId = storedMovieIds.begin(); mId != storedMovieIds.end(); mId++) {
        set<int>::iterator hit = activeMovieIds.find(*mId); 
        if (hit == activeMovieIds.end()) {
            DeleteMovie(*mId);            
        }
    }
    return numDelete;
}

void cMovieDBManager::DeleteMovie(int movieId) {
    if (movieId < 1)
        return;
    stringstream delMovieActors;
    delMovieActors << "delete from " << tMoviesActors->TableName();
    delMovieActors << " where " << tMoviesActors->getField(cTableMovieActors::fiMovieId)->name;
    delMovieActors << " = " << movieId;

    cDbStatement delActors(connection, delMovieActors.str().c_str());
    delActors.prepare();
    delActors.execute();

    stringstream delMovieMedia;
    delMovieMedia << "delete from " << tMovieMedia->TableName();
    delMovieMedia << " where " << tMovieMedia->getField(cTableMovieMedia::fiMovieId)->name;
    delMovieMedia << " = " << movieId;

    cDbStatement delMedia(connection, delMovieMedia.str().c_str());
    delMedia.prepare();
    delMedia.execute();

    stringstream delMovie;
    delMovie << "delete from " << tMovies->TableName();
    delMovie << " where " << tMovies->getField(cTableMovies::fiMovieId)->name;
    delMovie << " = " << movieId;

    cDbStatement delMov(connection, delMovie.str().c_str());
    delMov.prepare();
    delMov.execute();
}

bool cMovieDBManager::SearchRecordingDB(string name, int &movieId) {
    int status = success;
    cDbStatement *select = new cDbStatement(tMovies);
    select->build("select ");
    select->bind(cTableMovies::fiMovieId, cDBS::bndOut);
    select->build(" from %s where ", tMovies->TableName());
    select->bind(cTableMovies::fiTitle, cDBS::bndIn | cDBS::bndSet);
    status += select->prepare();
    if (status != success) {
        delete select;
        return false;
    }
    tMovies->clear();
    tMovies->setValue(cTableMovies::fiTitle, name.c_str());
    int found = select->find();
    movieId = tMovies->getIntValue(cTableMovies::fiMovieId);
    select->freeResult();
    delete select;
    if (found) {
        return true;
    }
    return false;
}

bool cMovieDBManager::SearchRecordingOnline(string name, int &movieId) {
    cMovieDbMovie *movieRec = movieDbScraper->Scrap(name);
    if (!movieRec) {
        movieRec = SearchRecordingSophisticated(name);
    }
    if (movieRec) {
        movieRec->ReadActors();
        SaveMovie(movieRec);
        movieId = movieRec->id;
        delete movieRec;
        return true;
    }
    return false;
}

cMovieDbMovie *cMovieDBManager::SearchRecordingSophisticated(string name) {
    cMovieDbMovie *movieFound = NULL;
    size_t posHyphen  = name.find_first_of("-");
    size_t posBracket = name.find_first_of("(");
    bool hasHyphen  = (posHyphen  != string::npos)?true:false;
    bool hasBracket = (posBracket != string::npos)?true:false;
    string nameMod;
    //first remove all "-" 
    if (hasBracket) {
        nameMod = str_replace("-", " ", name);
        movieFound = movieDbScraper->Scrap(nameMod);
        if (movieFound)
            return movieFound;
    }
    //if both hyphens and brackets found, check what comes first
    if (hasHyphen && hasBracket) {
        //if bracket comes after hyphen, remove bracket first
        if (posBracket > posHyphen) {
            nameMod = str_cut("(", name);
            if (nameMod.size() > 3) {
                movieFound = movieDbScraper->Scrap(nameMod);
                if (movieFound)
                    return movieFound;
            }
            nameMod = str_cut("-", name);
            if (nameMod.size() > 3) {
                movieFound = movieDbScraper->Scrap(nameMod);
            }
        } else {
            nameMod = str_cut("-", name);
            if (nameMod.size() > 3) {
                movieFound = movieDbScraper->Scrap(nameMod);
                if (movieFound)
                    return movieFound;
            }
            nameMod = str_cut("(", name);
            if (nameMod.size() > 3) {
                movieFound = movieDbScraper->Scrap(nameMod);
            }
        }
    } else if (hasHyphen) {
        nameMod = str_cut("-", name);
        if (nameMod.size() > 3) {
            movieFound = movieDbScraper->Scrap(nameMod);
        }
    } else if (hasBracket) {
        nameMod = str_cut("(", name);
        if (nameMod.size() > 3) {
            movieFound = movieDbScraper->Scrap(nameMod);
        }
    }
    return movieFound;
}

bool cMovieDBManager::CheckScrapInfoDB(int scrapMovieId) {
    //check if movie is in db
    tMovies->clear();
    tMovies->setValue(cTableMovies::fiMovieId, scrapMovieId);
    int found = tMovies->find();
    if (!found)
        return false;
    return true;
}

bool cMovieDBManager::CheckScrapInfoOnline(int scrapMovieId) {
    cMovieDbMovie *movieRec = movieDbScraper->ReadMovie(scrapMovieId);
    if (!movieRec)
        return false;
    movieRec->ReadActors();
    SaveMovie(movieRec);
    delete movieRec;
    return true;
}

