CREATE PROCEDURE mergeepg ()
BEGIN
/*
* configure merge
*/
set @epi ='a';
set @img ='b';
set @sht ='';
/*
* fix useid = 0
*/
update events set useid = masterid where useid = 0;
/*
* cleanup deleted Links
*/
update
 events e
set
 e.useid = e.masterid,
 e.updsp = unix_timestamp()
where
 e.source = 'vdr' and
 e.updflg in ('D','R') and
 e.useid != e.masterid and
 e.starttime + e.duration >= unix_timestamp();
/*
* cleanup broken Links
*/
update
 events t
 inner join
(
  select
   v.masterid,
   e.starttime,
   e.comptitle,
   e.compshorttext,
   e.updflg
  from
   events v
   left join
   events e
   on e.masterid = v.useid  and v.masterid != e.masterid and e.updflg in ('T','C') and e.delflg is Null
  where
   v.updflg in('L') and
   v.starttime + v.duration >= unix_timestamp()
) s
on t.masterid=s.masterid
set
 t.useid = t.masterid,
 t.updflg = case when t.delflg = 'Y' then 'D' else 'A' end,
 t.updsp = unix_timestamp()
where
 s.updflg is Null or
 abs(s.starttime - t.starttime) > 21600  or
 (abs(s.starttime - t.starttime) > 3600 and
  getlvrmin(0,0,0,0,
   getcrosslvr(s.comptitle,t.comptitle),
   getcrosslvr(s.comptitle,t.compshorttext),
   getcrosslvr(s.compshorttext,t.comptitle),
   getcrosslvr(s.compshorttext,t.compshorttext)) >= 60);
/*
* cleanup broken Targets
*/
update
 events t
 inner join
(
  select
   e.masterid,
   v.updflg
  from
   events e
   left join
   events v
   on e.masterid = v.useid  and v.masterid != e.masterid and v.updflg = 'L'
  where
   e.updflg in('C','T') and
   e.starttime + e.duration >= unix_timestamp()
) s
on t.masterid=s.masterid
set
 t.useid = t.masterid,
 t.updflg = case when t.delflg = 'Y' then 'D' else 'R' end,
 t.updsp = unix_timestamp()
where
 s.updflg is Null;
/*
* reset tables
*/
truncate snapshot;
truncate analyse;
/*
* prepare snapshot
*/
insert into snapshot
select
 distinct ev.channelid, ev.source, masterid, ev.eventid, useid, starttime, duration, title, comptitle, shorttext, compshorttext, ev.updsp,
  case when episode is Null then Null else 'X' end episode,
  case when ev.source = 'vdr' then Null else cm.merge end merge,
  case when ev.imagecount >0 then 'X' else Null end images
from
 events ev
 inner join channelmap cm on ( ev.channelid = cm.channelid )
where
 ( ev.source = cm.source or ev.source = 'vdr' )
 AND
 ev.updflg in ('I','A','R','S')
 AND
 ev.starttime between unix_timestamp() - ev.duration and unix_timestamp() + 259200
 AND
 cm.merge in(1,2);
/*
* do the magic
*/
set @pk1 ='';
set @rn1 =10000;
set @lvmin ='';
insert ignore into analyse
SELECT  channelid, vdr_masterid, vdr_eventid, vdr_starttime, vdr_duration, vdr_title, vdr_shorttext, ext_masterid, ext_eventid, ext_starttime, ext_duration, ext_title, ext_shorttext, ext_episode, ext_merge, ext_images, lvmin,
          epi+img+sht + ranklv as rank
FROM
(
  SELECT  channelid, vdr_masterid, vdr_eventid, vdr_starttime, vdr_duration, vdr_title, vdr_shorttext, ext_masterid, ext_eventid, ext_starttime, ext_duration, ext_title, ext_shorttext, ext_episode, ext_merge, ext_images, lvmin,
            case when ext_episode is not Null then 0 else (case @epi when 'a' then 1000 when 'b' then 100 when 'c' then 10 else 0 end) end epi,
            case when ext_images is not Null then 0 else (case @img when 'a' then 1000 when 'b' then 100 when 'c' then 10 else 0 end) end img,
            case when ext_shorttext is not Null then 0 else (case @sht when 'a' then 1000 when 'b' then 100 when 'c' then 10 else 0 end) end sht,
          @rn1 := if(@pk1=concat(channelid,vdr_eventid,ext_source), if(@lv=lvmin, @rn1, @rn1+10000),10000+ext_merge) as ranklv,
          @pk1 := concat(channelid,vdr_eventid,ext_source),
          @lv := lvmin
  FROM
  (
    select
     vdr.channelid,
     vdr.masterid vdr_masterid, vdr.eventid vdr_eventid, vdr.starttime vdr_starttime, vdr.duration vdr_duration, vdr.title vdr_title, vdr.shorttext vdr_shorttext,
     ext.masterid ext_masterid, ext.eventid ext_eventid, ext.starttime ext_starttime, ext.duration ext_duration, ext.title ext_title, ext.shorttext ext_shorttext,
     ext.source ext_source,ext.episode ext_episode, ext.merge ext_merge, ext.images ext_images,
     getlvrmin(vdr.starttime,ext.starttime,vdr.duration,ext.duration,
       getcrosslvr(vdr.comptitle,ext.comptitle),
       getcrosslvr(vdr.comptitle,ext.compshorttext),
       getcrosslvr(vdr.compshorttext,ext.comptitle),
       getcrosslvr(vdr.compshorttext,ext.compshorttext)
     ) lvmin
    from
     snapshot vdr
     inner join
     snapshot ext
     on vdr.channelid = ext.channelid and
     vdr.starttime - ext.starttime between -1200 and +1200 and
     vdr.duration / ext.duration * 100 between 50 and 200
    where
     vdr.source = 'vdr'
     AND
     vdr.source <> ext.source
    ORDER BY channelid,vdr_eventid,ext_merge desc,lvmin
  ) A
  where lvmin <= 60
) B
order by channelid,vdr_eventid,rank;
/*
* update mergesp
*/
update
 channelmap cm,
 (select channelid,max(vdr_starttime) merge_sp from analyse group by channelid) an
set
 cm.mergesp = an.merge_sp
where
 an.channelid = cm.channelid and
 an.merge_sp > cm.mergesp;
/*
* update useid on vdr events
*/
update
 events ev,
 analyse an
set
 ev.useid = an.ext_masterid
where
 ev.masterid = vdr_masterid and
 ev.channelid = an.channelid;
/*
* update useid on ext events
*/
update
 events ext,
 events vdr
set
 ext.useid = case when ext.updflg in('I','R') then vdr.masterid else ext.useid end,
 ext.updflg = case when ext.updflg in('I','R') then 'C' else 'T' end,
 ext.updsp = case when ext.updflg in('I','R') then unix_timestamp() else ext.updsp end
where
 ext.masterid = vdr.useid and
 ext.channelid = vdr.channelid and
 ext.updflg in('I','A','R','S') and
 ext.source <> vdr.source and
 vdr.source = 'vdr';
/*
* update all other relevant updflg and updsp
*/
update
 events ev,
 (select distinct channelid,mergesp from channelmap where source != 'vdr') cm
set
 ev.updflg = case when ev.source = 'vdr' and ev.masterid != ev.useid then 'L'
                  when ev.source = 'vdr' and ev.masterid = ev.useid then 'A'
                  when ev.source != 'vdr' and ev.updflg = 'S' then 'I'
                  when ev.source != 'vdr' then 'R'
             end,
 ev.updsp = case when source = 'vdr' and ev.masterid = ev.useid and ev.updflg = 'A' then ev.updsp
                 else unix_timestamp()
            end
where
 ev.channelid = cm.channelid and
 ev.starttime < cm.mergesp + ev.duration/5 and
 ( ev.source = 'vdr' and ev.updflg in ('I','A') or
  ev.source != 'vdr' and ev.updflg in ('A','S') );
/*
* reinitialize prior removed dvb events
*/
update
 events ev,
 (select distinct channelid,mergesp from channelmap where source != 'vdr') cm
set
 ev.updflg = 'A',
 ev.useid = ev.masterid,
 ev.updsp = unix_timestamp()
where
 ev.channelid = cm.channelid and
 ev.starttime < cm.mergesp + ev.duration/5 and
 ev.source = 'vdr' and
 ev.updflg in ('R');
END
