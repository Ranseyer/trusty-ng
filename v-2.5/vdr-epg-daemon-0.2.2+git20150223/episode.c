/*
 * update.c
 *
 * See the README file for copyright information
 *
 */

#include <unistd.h>
#include <locale.h>
#include <zlib.h>

#include "epgd.h"

//***************************************************************************
// 
//***************************************************************************

int getSpecial(const char* line, const char* item, char* content, int max)
{
   const char* p;
   
   unsigned int off = strlen(item) + 1;

   if ((p = strstr(line, item)) && strlen(line) > off)
   {
      snprintf(content, max, "%s", p+off);

      return success;
   }

   return fail;
}

//***************************************************************************
// Store To Table
//***************************************************************************

int cEpisodeFile::storeToTable(cDbTable* episodeDb, const cList<cLine>* linkLines)
{
   unsigned int pos;
   string compName;
   string lang = "de";
   string ename = name;
   unsigned short seasons[100];

   char shortName[100]; *shortName = 0;
   char col1Name[100]; *col1Name = 0;
   char col2Name[100]; *col2Name = 0;
   char col3Name[100]; *col3Name = 0;

   const cList<cLine>* theLines = linkLines ? linkLines : lines;
   
   memset(seasons, 0, sizeof(seasons));

   if (!theLines)
      return done;

   if (isLink())
      ename = link;

   // detect language special

   if ((pos = ename.rfind('.')) != string::npos)
   {
      lang = ename.substr(pos+1);

      if (lang == "de" || lang == "en" || lang == "de-en" || lang == "en-de" || lang == "at")
         ename.erase(pos);
      else
         lang = "de";
   }

   // build compressed name for episode lookup
  
   compName = ename;
   prepareCompressed(compName);

   if (!compName.length())
      return done;

   // delete old entry of this episode
   
   string delStmt = "compname = '" + compName + "' and lang = '" + lang + "'";

   episodeDb->deleteWhere(delStmt.c_str());

   char* line = 0;

   for (cLine* l = theLines->First(); l; l = theLines->Next(l))
   {
      free(line);
      line = strdup(l->Text());

      if (l->Length() < 4)
         continue;

      // printf("-> '%s'\n", l->Text());

      if (line[0] == '#')
      {
         getSpecial(line, "# SHORT", shortName, 100);
         getSpecial(line, "# EXTRACOL1", col1Name, 100);
         getSpecial(line, "# EXTRACOL2", col2Name, 100);
         getSpecial(line, "# EXTRACOL3", col3Name, 100);
         
         if (isdigit(line[2]))
         {
            int s, v, t;

            if (sscanf(line, "# %d\t%d\t%d", &s, &v, &t) == 3)
               seasons[s] = t-(v-1);
         }

         continue;     
      }

      if (!isdigit(line[0]) || !isdigit(line[1]) || line[2] != '\t')
         continue;
      
      // found episode line ...

      string partNameComp;
      char partName[200]; *partName = 0;
      char* comment = 0;
      char ex1[250]; *ex1 = 0;
      char ex2[250]; *ex2 = 0;
      char ex3[250]; *ex3 = 0;
      char ex[250]; *ex = 0;

      int se;
      int ep;
      int no;

      // get lines like: 
      //    "01<tab>1<tab>1<tab>Schatten der Vergangenheit<tab>[extcol1<tab>[extcol2<tab>[extcol3]]]<tab>#comment"
      
      comment = strchr(line, '#');
      
      if (comment)
      {
         *comment = 0;
         comment++;

         while (*comment && *comment == ' ')
            comment++;
      }

      if (sscanf(line, "%d\t%d\t%d\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]", 
                 &se, &ep, &no, partName, ex1, ex2, ex3) < 4)
         continue;

      partNameComp = partName;
      prepareCompressed(partNameComp);

      if (!partNameComp.length() || strcmp(partName, "n.n.") == 0)
         continue;

      episodeDb->clear();

      episodeDb->setValue(cTableEpisodes::fiCompName, compName.c_str());
      episodeDb->setValue(cTableEpisodes::fiCompPartName, partNameComp.c_str());
      episodeDb->setValue(cTableEpisodes::fiLang, lang.c_str());

      episodeDb->setValue(cTableEpisodes::fiEpisodeName, ename.c_str());
      episodeDb->setValue(cTableEpisodes::fiLink, isLink());
      episodeDb->setValue(cTableEpisodes::fiPartName, partName);
      episodeDb->setValue(cTableEpisodes::fiSeason, se);
      episodeDb->setValue(cTableEpisodes::fiPart, ep);
      episodeDb->setValue(cTableEpisodes::fiParts, se < 100 ? seasons[se] : 0);
      episodeDb->setValue(cTableEpisodes::fiNumber, no);

      if (!isEmpty(shortName))
         episodeDb->setValue(cTableEpisodes::fiShortName, shortName);

      if (!isEmpty(comment))
         episodeDb->setValue(cTableEpisodes::fiComment, comment);

      if (!isEmpty(ex1) && !isEmpty(col1Name))
      {
         snprintf(ex, 250, "%s: %s", col1Name, ex1);
         episodeDb->setValue(cTableEpisodes::fiExtraCol1, ex);
      }

      if (!isEmpty(ex2) && !isEmpty(col2Name)) 
      {
         snprintf(ex, 250, "%s: %s", col2Name, ex2);
         episodeDb->setValue(cTableEpisodes::fiExtraCol2, ex);
      }

      if (!isEmpty(ex3) && !isEmpty(col3Name)) 
      {
         snprintf(ex, 250, "%s: %s", col3Name, ex3);
         episodeDb->setValue(cTableEpisodes::fiExtraCol3, ex);
      }

      if (episodeDb->store() != success)
         tell(0, "Error: Can't store part '%s'/%d of serie '%s' in database", partName, ep, ename.c_str());
   }

   free(line);
   
   return 0;
}
