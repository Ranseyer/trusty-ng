��            )   �      �     �     �     �  )   �          &  '   3     [     g  &   t     �  g   �  -     A   C     �     �     �     �     �     �               %  &   B     i  !   z  &   �     �     �  �  �  (   �     �  !   �     �          !  !   4     V     h  (   {     �  �   �  2   G	  C   z	     �	     �	     �	     
  "   '
     J
     h
      �
  &   �
  E   �
       (   '  :   P  $   �     �                                                                                             	                              
              ${SYSTEM3} is now without GRUB. Advanced options Boot successfully repaired. Close this window when you have finished. GRUB location GRUB options GRUB reinstallation has been cancelled. MBR options Main options No OS has been found on this computer. Operation aborted Please use the file browser that just opened to delete unused files (or transfer them to another disk). Please use this software in a 64bits session. Please use this software in a live-session (live-CD or live-USB). Please wait few seconds... Recommended repair Reinstall GRUB Repair the boot of the computer Restore MBR Restore the MBR of: Scanning systems The OS now in use This can prevent to start it Ubuntu installed in Windows (via Wubi) Unhide boot menu You can now reboot your computer. e.g. you may get a Power Manager error repairs most frequent problems seconds Project-Id-Version: boot-repair
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-12-08 01:03+0100
PO-Revision-Date: 2012-11-29 21:21+0000
Last-Translator: YannUbuntu <yannubuntu@gmail.com>
Language-Team: Tagalog <tl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-12-23 14:41+0000
X-Generator: Launchpad (build 16877)
 ${SYSTEM3} ay kasalukuyang wala ng GRUB. Advance na Opsyon Ang boot ay matagumpay na naayos. Paki sara ito pag tapos ka na. Kinalalagyan ng GRUB Mga opsyon sa GRUB Ang pagtalaga ng GRUB ay itinigil Mga opsyon sa MBR Pangunahing opsyon Walang OS na nakita sa kompyuter na ito. Itinigil ang operasyon Paki gamit ng huling file browser na binuksan para mabura ang mga files na hindi na ginagamit (o kaya ilipat mo ang mga ito sa ibang disk). Paki gamit ng software na ito sa 64-bit na sesyon. Paki gamit ng software na ito sa live-session (live-CD o live-USB). Pakihintay ng ilang segundo Inirerekumendang repair Italaga ulit ang GRUB Paki-ayos ng boot ng kompyuter Ibalik ang dating kalagayan ng MBR Ibalik ang dating ang MBR ng: Sinasaliksik pa ang system Ang OS ay kasalukuyang ginagamit Maaari itong pumigil sa pag-umpisa nun Ang Ubuntu ay nakatalaga sa loob ng Windows (sa pamamagitan ng Wubi). Ipakita ang boot menu Pwede mo ng i-restart ang iyong komputer halimbawa, maaari kang magkaroon ng Power Manager na error pag-aayos pinaka-madalas na problema mga segundo 