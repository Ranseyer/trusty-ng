/*
 * tvonscreensd.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: tvonscreensd.c,v 1.15 2006/06/18 13:59:36 schmitzj Exp $
 *
 */

#include <vdr/plugin.h>
#include "magazine.h"
#include "config.h"
//#include "i18n.h"

static const char *VERSION        = "1.0.141.SD-FF";
static const char *DESCRIPTION    = trNOOP("Shows the EPG info in form of a typical TV magazine");
static const char *MAINMENUENTRY  = trNOOP("TV-OnScreensd");

class cPluginTvOnscreensd : public cPlugin {
private:

public:
  cPluginTvOnscreensd(void);
  virtual ~cPluginTvOnscreensd();
  virtual const char *Version(void) { return VERSION; }
  virtual const char *Description(void) { return tr(DESCRIPTION); }
  virtual const char *CommandLineHelp(void);
  virtual bool ProcessArgs(int argc, char *argv[]);
  virtual bool Initialize(void);
  virtual bool Start(void);
  virtual void Housekeeping(void);
  virtual const char *MainMenuEntry(void) { return tr(MAINMENUENTRY); }
  virtual cOsdObject *MainMenuAction(void);
  virtual cMenuSetupPage *SetupMenu(void);
  virtual bool SetupParse(const char *Name, const char *Value);
  };

cPluginTvOnscreensd::cPluginTvOnscreensd(void)
{
  // Initialize any member variables here.
  // DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
  // VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!
}

cPluginTvOnscreensd::~cPluginTvOnscreensd()
{
  // Clean up after yourself!
}

const char *cPluginTvOnscreensd::CommandLineHelp(void)
{
  // Return a string that describes all known command line options.
	return tvonscreensdCfg.CommandLineHelp();
}

bool cPluginTvOnscreensd::ProcessArgs(int argc, char *argv[])
{
	return tvonscreensdCfg.ProcessArgs(argc,argv);
}

bool cPluginTvOnscreensd::Initialize(void)
{
  // Initialize any background activities the plugin shall perform.
  	return true;
}

bool cPluginTvOnscreensd::Start(void)
{
  // Start any background activities the plugin shall perform.
  return true;
}

void cPluginTvOnscreensd::Housekeeping(void)
{
  // Perform any cleanup or other regular tasks.
}

cOsdObject *cPluginTvOnscreensd::MainMenuAction(void)
{
  // Perform the action when selected from the main VDR menu.

	return new magazine(this);
}

cMenuSetupPage *cPluginTvOnscreensd::SetupMenu(void)
{
  // Return a setup menu in case the plugin supports one.
  return new tvonscreensdConfigPage();
}

bool cPluginTvOnscreensd::SetupParse(const char *Name, const char *Value)
{
  // Parse your own setup parameters and store their values.
	return tvonscreensdCfg.SetupParse(Name,Value);
}

VDRPLUGINCREATOR(cPluginTvOnscreensd); // Don't touch this!
