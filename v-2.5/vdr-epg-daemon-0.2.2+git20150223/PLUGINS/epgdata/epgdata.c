/*
 * epgdata.c
 *
 * See the README file for copyright information
 *
 */

#include <dirent.h>
#include <unistd.h>

#include "epgdata.h"

//***************************************************************************
// Epgdata
//***************************************************************************

Epgdata::Epgdata()
   : Plugin()
{
   pxsltStylesheet = 0;
   stmtByFileRef = 0;
   stmtCleanDouble = 0;
   stmtSetDelByFileref = 0;
   selectByDate = 0;
   selectId = 0;

   pin = 0;
   timeout = 3 * tmeSecondsPerMinute;
   baseurl = strdup("http://www.epgdata.com");
}

Epgdata::~Epgdata()
{
   if (pxsltStylesheet)
      xsltFreeStylesheet(pxsltStylesheet);

   delete stmtByFileRef;
   delete selectByDate;
   delete stmtCleanDouble;
   delete stmtSetDelByFileref;

   free(baseurl);
   free(pin);
}

int Epgdata::init(cEpgd* aObject, int aUtf8)
{
   Plugin::init(aObject, aUtf8);

   pxsltStylesheet = loadXSLT(getSource(), confDir, utf8);

   return done;
}

int Epgdata::initDb()
{
   int status = success;

   // --------
   // by fileref (for pictures)
   // select name from fileref 
   //     where source = ? and fileref = ?

   stmtByFileRef = new cDbStatement(obj->fileDb);

   stmtByFileRef->build("select ");
   stmtByFileRef->bind(cTableFileRefs::fiName, cDBS::bndOut);
   stmtByFileRef->build(" from %s where ", obj->fileDb->TableName());
   stmtByFileRef->bind(cTableFileRefs::fiSource, cDBS::bndIn | cDBS::bndSet);
   stmtByFileRef->bind(cTableFileRefs::fiFileRef, cDBS::bndIn | cDBS::bndSet, " and ");
   
   status += stmtByFileRef->prepare();

   // ---------------
   // (for cleanup double)
   // select name from fileref 
   //     where source = ? order by name desc

   stmtCleanDouble = new cDbStatement(obj->fileDb);

   stmtCleanDouble->build("select ");
   stmtCleanDouble->bind(cTableFileRefs::fiName, cDBS::bndOut);
   stmtCleanDouble->build(" from %s where ", obj->fileDb->TableName());
   stmtCleanDouble->bind(cTableFileRefs::fiSource, cDBS::bndIn | cDBS::bndSet);
   stmtCleanDouble->build(" order by name desc");
   
   status += stmtCleanDouble->prepare();

   // ---------
   // select channelid, merge, mergesp from channelmap 
   //     where source = ? and extid = ?

   selectId = new cDbStatement(obj->mapDb);

   selectId->build("select ");
   selectId->bind(cTableChannelMap::fiChannelId, cDBS::bndOut);
   selectId->bind(cTableChannelMap::fiMergeSp, cDBS::bndOut, ", ");
   selectId->bind(cTableChannelMap::fiMerge, cDBS::bndOut, ", ");
   selectId->build(" from %s where ", obj->mapDb->TableName());
   selectId->bind(cTableChannelMap::fiSource, cDBS::bndIn | cDBS::bndSet);
   selectId->bind(cTableChannelMap::fiExternalId, cDBS::bndIn | cDBS::bndSet, " and ");
   
   status += selectId->prepare();

   // ---------
   // select name, tag from filerf
   //  where source = 'epgdata' 
   //    and name like ?

   valueName.setField(obj->fileDb->getField(cTableFileRefs::fiName));
   valueNameLike.setField(obj->fileDb->getField(cTableFileRefs::fiName));

   selectByDate = new cDbStatement(obj->fileDb);

   selectByDate->build("select ");
   selectByDate->bind(&valueName, cDBS::bndOut);
   selectByDate->bind(cTableFileRefs::fiTag, cDBS::bndOut, ", ");
   selectByDate->build(" from %s where source = '%s' and ", 
                       obj->fileDb->TableName(), getSource());
   selectByDate->bindCmp(0, &valueNameLike, "like");

   status += selectByDate->prepare();

   // --------
   // update events set delflg = ?, updflg = ?, fileref = ?, updsp = ?
   //    where fileref = ? 
   //      and source = ?;
   //      and updflg in (....)

   valueFileRef.setField(obj->eventsDb->getField(cTableEvents::fiFileRef));
   stmtSetDelByFileref = new cDbStatement(obj->eventsDb);
   
   stmtSetDelByFileref->build("update %s set ", obj->eventsDb->TableName());
   stmtSetDelByFileref->bind(cTableEvents::fiDelFlg, cDbService::bndIn |cDbService:: bndSet);
   stmtSetDelByFileref->bind(cTableEvents::fiUpdFlg, cDbService::bndIn |cDbService:: bndSet, ", ");
   stmtSetDelByFileref->bind(cTableEvents::fiFileRef, cDbService::bndIn | cDbService::bndSet, ", ");
   stmtSetDelByFileref->bind(cTableEvents::fiUpdSp, cDbService::bndIn | cDbService::bndSet, ", ");
   stmtSetDelByFileref->build( " where ");
   stmtSetDelByFileref->bind(&valueFileRef, cDbService::bndIn |cDbService:: bndSet);
   stmtSetDelByFileref->bind(cTableEvents::fiSource, cDbService::bndIn | cDbService::bndSet, " and ");
   stmtSetDelByFileref->build(" and updflg in (%s)", Us::getDeletable());

   status += stmtSetDelByFileref->prepare();

   // ----------
   // if no epgdata entry in fileref read files from FS to table

   int count = 0;
   obj->fileDb->countWhere("source = 'epgdata'", count);

   if (!count)
   {
      char* path = 0;
      DIR* dir;
      dirent* dp;

      asprintf(&path, "%s/%s", EPG2VDRConfig.cachePath, getSource());
      chkDir(path);

      if (!(dir = opendir(path)))
      {
         tell(0, "Error: Opening cache path '%s' failed, %s", path, strerror(errno));
         free(path);
         return fail;
      }

      while (dp = readdir(dir))
      {
         char* fileRef = 0;
         char* file = 0;
         char* tag = 0;
         struct stat sb;

         if (!strstr(dp->d_name, "_de_qy.zip"))
            continue;

         asprintf(&file, "%s/%s", path, dp->d_name);
         stat(file, &sb);
         free(file);

         asprintf(&tag, "%ld", sb.st_size);
         asprintf(&fileRef, "%s-%s", dp->d_name, tag);
         
         // store file and let tag NULL to indicate that processing is needed

         obj->fileDb->clear();
         obj->fileDb->setValue(cTableFileRefs::fiName, dp->d_name);
         obj->fileDb->setValue(cTableFileRefs::fiSource, getSource());
         obj->fileDb->setValue(cTableFileRefs::fiExternalId, "0");
         obj->fileDb->setValue(cTableFileRefs::fiFileRef, fileRef);
         obj->fileDb->store();
         
         tell(1, "Added '%s' to table fileref", dp->d_name);
         free(fileRef);
         free(tag);
      }

      free(path);
      closedir(dir);
   }

   return success;
}

int Epgdata::exitDb()
{
   delete stmtByFileRef;       stmtByFileRef = 0;
   delete stmtCleanDouble;     stmtCleanDouble = 0;
   delete selectByDate;        selectByDate = 0;
   delete selectId;            selectId = 0;
   delete stmtSetDelByFileref; stmtSetDelByFileref = 0;

   return success;
}

//***************************************************************************
// At Config Item
//***************************************************************************

int Epgdata::atConfigItem(const char* Name, const char* Value)
{
   if      (!strcasecmp(Name, "Url"))      { free(baseurl); baseurl = strdup(Value); }
   else if (!strcasecmp(Name, "Pin"))      { free(pin); pin = strdup(Value); }
   else if (!strcasecmp(Name, "Timeout"))  { timeout = atoi(Value); }

   else return fail;

   return success;
}

//***************************************************************************
// Ready
//***************************************************************************

int Epgdata::ready() 
{ 
   static int count = na;

   if (isEmpty(pin))
      return no;

   if (count == na)
   {
      char* where;

      asprintf(&where, "source = '%s'", getSource());

      if (obj->mapDb->countWhere(where, count) != success)
         count = na;

      free(where);
   }

   return count > 0;
}

//***************************************************************************
// Process Day
//***************************************************************************

int Epgdata::processDay(int day, int fullupdate, Statistic* statistic)
{
   char* directory = 0;
   char* fspath = 0;
   char* path = 0;           // name of the zip file including the path
   int haveOneForThisDay = no;
   MemoryStruct data;
   int fileSize = 0;
   char* fileRef = 0;
   char* url = 0;
   char* logurl = 0;
   int load = yes;
   char* like = 0;
   int bSize = 0;
   char entryName[200+TB];
   MemoryStruct uzdata;
   char* oldFileRef = 0;

   int status;

   // path to zip files, url, ..

   asprintf(&directory, "%s/%s", EPG2VDRConfig.cachePath, getSource());
   chkDir(directory);
   asprintf(&url, "%s/index.php?action=sendPackage&iOEM=VDR&pin=%s&dayOffset=%d&dataType=xml", baseurl, pin, day);
   asprintf(&logurl, "%s/index.php?action=sendPackage&iOEM=VDR&pin=%s&dayOffset=%d&dataType=xml", baseurl, "insert-your-pin-here", day);

   // first get the http header

   data.clear();
   data.headerOnly = yes;

   status = obj->downloadFile(url, fileSize, &data);

   if (status != success || isEmpty(data.name))
   {
      tell(1, "Download header for day (%d) at '%s' failed, aborting, got name '%s', status was %d", 
           day, logurl, data.name ? data.name : "", status);
      status = fail;
      goto Exit;
   }

   tell(2, "Got info for day (%d), file '%s' with tag '%s'", day, data.name, data.tag);

   asprintf(&fileRef, "%s-%s", data.name, data.tag);
   asprintf(&path, "%s/%s", directory, data.name);

   // lookup file
   
   obj->fileDb->clear();
   obj->fileDb->setValue(cTableFileRefs::fiName, data.name);
   obj->fileDb->setValue(cTableFileRefs::fiSource, getSource());

   // 1:1 match ?

   obj->fileDb->find();

   asprintf(&like, "%.8s_%%", data.name);
   valueNameLike.setValue(like);
   free(like);
   
   // check for update 

   if (selectByDate->find())
   {
      haveOneForThisDay = yes;
      oldFileRef = strdup(obj->fileDb->getStrValue(cTableFileRefs::fiFileRef));
   }

   if (haveOneForThisDay && day >= EPG2VDRConfig.upddays)
   {
      // don't check for update of existing files more than 'upddays' in the future
      
      tell(2, "Skipping update check of file '%s' for day %d", data.name, day);

      statistic->nonUpdates++;
      status = success;
      load = no;
   }
   else if (haveOneForThisDay && obj->fileDb->hasValue(cTableFileRefs::fiFileRef, fileRef))
   {
      tell(2, "Skipping download of day (%d) due to non-update", day);
      statistic->nonUpdates++;
      status = success;
      load = no;
   }

   if (!load && !obj->fileDb->getRow()->getValue(cTableFileRefs::fiTag)->isNull())
      goto Exit;

   // not exist, update or not processed -> work

   // first check if we have it already on fs

   asprintf(&fspath, "%s/%s", directory, valueName.getStrValue());

   if (!load && fileExists(fspath))
   {
      tell(1, "File '%s' exist, loading from filesystem", fspath);
      
      obj->loadFromFs(&data, valueName.getStrValue(), getSource());

      free(fileRef);
      free(path);
      path = strdup(fspath);
      asprintf(&fileRef, "%s-%s", valueName.getStrValue(), data.tag);

      obj->fileDb->clear();
      obj->fileDb->setValue(cTableFileRefs::fiName, valueName.getStrValue());
      obj->fileDb->setValue(cTableFileRefs::fiSource, getSource());
   }

   free(fspath);

   if (load)
   {
      tell(0, "Download file: '%s' to '%s", logurl, data.name);
      
      data.clear();
      
      if (obj->downloadFile(url, fileSize, &data, timeout) != success)
      {
         tell(0, "Download of day (%d) from '%s' failed", day, logurl);
         status = fail;
         goto Exit;
      }

      statistic->bytes += data.size;
      statistic->files++;

      // store zip to FS
      
      obj->storeToFs(&data, data.name, getSource());   
   }

   if (data.isEmpty())
      goto Exit;

   // unzip ...
   
   uzdata.clear();
   
   if (unzip(path, ".xml", uzdata.memory, bSize, entryName) == success)
   {
      tell(0, "Processing file '%s' for day %d (%d/%d)", 
           fileRef, day, haveOneForThisDay, load);

      uzdata.size = bSize;
             
      // store ?
      
      if (EPG2VDRConfig.storeXmlToFs)
         obj->storeToFs(&uzdata, entryName, getSource());
      
      // process file ..
      
      obj->connection->startTransaction();
      
      if ((status = processFile(uzdata.memory, uzdata.size, fileRef)) != success)
         statistic->rejected++;
      
      if (!obj->dbConnected())
      {
         status = fail;
         goto Exit;
      }
      
      if (haveOneForThisDay && load && strcmp(oldFileRef, fileRef) != 0)
      {
         // mark 'old' entrys in events table as deleted
         // and 'fake' fileref to new to avoid deletion at cleanup
         
         tell(0, "Removing events of fileref '%s' for day %d", oldFileRef, day);
         
         obj->eventsDb->clear();
         obj->eventsDb->setValue(cTableEvents::fiDelFlg, "Y");
         obj->eventsDb->setValue(cTableEvents::fiUpdFlg, "D");
         obj->eventsDb->setValue(cTableEvents::fiFileRef, fileRef);     // fake to new fileref
         obj->eventsDb->setValue(cTableEvents::fiUpdSp, time(0));
         obj->eventsDb->setValue(cTableEvents::fiSource, getSource());
         valueFileRef.setValue(oldFileRef);                             // old fileref
         stmtSetDelByFileref->execute();
      }
      
      // Confirm processing of file
      
      obj->fileDb->setValue(cTableFileRefs::fiExternalId, "0");
      obj->fileDb->setValue(cTableFileRefs::fiTag, data.tag);
      obj->fileDb->setValue(cTableFileRefs::fiFileRef, fileRef);
      obj->fileDb->store();
      
      obj->connection->commit();
   }

  Exit:
   
   free(oldFileRef);
   obj->fileDb->reset();
   selectByDate->freeResult();

   uzdata.clear();
   free(url);
   free(logurl);
   free(fileRef);
   free(directory);
   free(path);

   return status;
}

//***************************************************************************
// Process File
//***************************************************************************

int Epgdata::processFile(const char* data, int size, const char* fileRef)
{
   xmlDocPtr transformedDoc;
   xmlNodePtr xmlRoot;
   int count = 0;

   if ((transformedDoc = obj->transformXml(data, size, pxsltStylesheet, fileRef)) == 0)
   {
      tell(0, "XSLT transformation for '%s' failed, ignoring", fileRef);
      return fail;
   }

   if (!(xmlRoot = xmlDocGetRootElement(transformedDoc)))
   {
      tell(0, "Invalid xml document returned from xslt for '%s', ignoring", fileRef);
      return fail;
   }
   
   // DEBUG: xmlSaveFile("/tmp/test.xml", transformedDoc);

   for (xmlNodePtr node = xmlRoot->xmlChildrenNode; node; node = node->next)
   {
      char* prop = 0;
      int eventid;
      char* extid = 0;
      string comp;
      
      // skip all unexpected elements
      
      if (node->type != XML_ELEMENT_NODE || strcmp((char*)node->name, "event") != 0)
         continue;
      
      // get/check eventid
      
      if (!(prop = (char*)xmlGetProp(node, (xmlChar*)"id")) || !*prop || !(eventid = atoi(prop)))
      {
         xmlFree(prop);
         tell(0, "Missing event id, ignoring!\n");
         continue;
      }
      
      xmlFree(prop);

      // get/check provider id
      
      if (!(prop = (char*)xmlGetProp(node, (xmlChar*)"provid")) || !*prop || !atoi(prop))
      {
         xmlFree(prop);
         tell(0, "Missing provider id, ignoring!\n");
         continue;
      }

      extid = strdup(prop);
      xmlFree(prop);

      obj->mapDb->clear();
      obj->mapDb->setValue(cTableChannelMap::fiExternalId, extid);
      obj->mapDb->setValue(cTableChannelMap::fiSource, getSource());
      free(extid);

      for (int f = selectId->find(); f; f = selectId->fetch())
      {
         int insert;
         const char* channelId = obj->mapDb->getStrValue(cTableChannelMap::fiChannelId);
         
         // create event ..
         
         obj->eventsDb->clear();
         obj->eventsDb->setValue(cTableEvents::fiEventId, eventid);
         obj->eventsDb->setValue(cTableEvents::fiChannelId, channelId);

         insert = !obj->eventsDb->find();

         obj->eventsDb->setValue(cTableEvents::fiSource, getSource());
         obj->eventsDb->setValue(cTableEvents::fiFileRef, fileRef);

         // auto parse ans set other fields

         obj->parseEvent(obj->eventsDb->getRow(), node);
         
         // compressed ..
         
         if (!obj->eventsDb->isNull(cTableEvents::fiTitle))
         {
            comp = obj->eventsDb->getStrValue(cTableEvents::fiTitle);
            prepareCompressed(comp);
            obj->eventsDb->setValue(cTableEvents::fiCompTitle, comp.c_str());
         }
         
         if (!obj->eventsDb->isNull(cTableEvents::fiShortText))
         {
            comp = obj->eventsDb->getStrValue(cTableEvents::fiShortText);
            prepareCompressed(comp);
            obj->eventsDb->setValue(cTableEvents::fiCompShortText, comp.c_str());
         }
         
         // store ..

         if (insert)
         {
            // handle insert

            time_t mergesp = obj->mapDb->getIntValue(cTableChannelMap::fiMergeSp);
            long starttime = obj->eventsDb->getIntValue(cTableEvents::fiStartTime);
            int merge = obj->mapDb->getIntValue(cTableChannelMap::fiMerge);

            obj->eventsDb->setValue(cTableEvents::fiVersion, 0xFF);
            obj->eventsDb->setValue(cTableEvents::fiTableId, 0L);
            obj->eventsDb->setValue(cTableEvents::fiUseId, 0L);

            if (starttime <= mergesp)
               obj->eventsDb->setValue(cTableEvents::fiUpdFlg, "I");
            else
               obj->eventsDb->setValue(cTableEvents::fiUpdFlg, merge > 1 ? "S" : "A");

            obj->eventsDb->insert();
         }
         else
         {
            obj->eventsDb->update();
         }

         obj->eventsDb->reset();
         count++;

         if (!obj->dbConnected())
            break;
      }
   }

   selectId->freeResult();

   xmlFreeDoc(transformedDoc);

   tell(2, "XML File '%s' processed, updated %d events", fileRef, count);

   return success;
}

//***************************************************************************
// Get Picture
//***************************************************************************

int Epgdata::getPicture(const char* imagename, const char* fileRef, MemoryStruct* data)
{
   int fileSize = 0;
   char* path = 0;
   char entryName[200+TB];

   data->clear();

   // lookup file information

   obj->fileDb->clear();
   obj->fileDb->setValue(cTableFileRefs::fiFileRef, fileRef);
   obj->fileDb->setValue(cTableFileRefs::fiSource, getSource());

   if (stmtByFileRef->find())
      asprintf(&path, "%s/epgdata/%s", EPG2VDRConfig.cachePath, 
               obj->fileDb->getStrValue(cTableFileRefs::fiName));

   stmtByFileRef->freeResult();

   if (!path)
   {
      tell(0, "Error: No entry with fileref '%s' to lookup image '%s' found",
           fileRef, imagename);
      return 0;
   }

   if (unzip(path, imagename, data->memory, fileSize, entryName) == success)
   {
      data->size = fileSize;
      tell(2, "Unzip of image '%s' succeeded", imagename);
   }

   free(path);

   return fileSize;
}

int Epgdata::cleanupAfter()
{
   // cleanup *.zip in FS cache

   const char* ext = ".zip";
   struct dirent* dirent;   
   DIR* dir;
   char* pdir;
   int count = 0;
   char* last = 0;

   // remove old versions for each day
 
   obj->fileDb->clear();
   obj->fileDb->setValue(cTableFileRefs::fiSource, getSource());

   for (int f = stmtCleanDouble->find(); f; f = stmtCleanDouble->fetch())
   {
      const char* name = obj->fileDb->getStrValue(cTableFileRefs::fiName);

      if (last && strncmp(name, last, 8) == 0)
      {
         char* where;
         tell(1, "Remove old epgdata file '%s' from table", name);
         asprintf(&where, "name = '%s'", name);
         obj->fileDb->deleteWhere(where);
         free(where);
      }
       
      free(last);
      last = strdup(name);
   }
   
   free(last);
   stmtCleanDouble->freeResult();

   // cleanup filesystem, remove files which not referenced in table

   asprintf(&pdir, "%s/%s", EPG2VDRConfig.cachePath, getSource());

   if (!(dir = opendir(pdir)))
   {
      tell(1, "Can't open directory '%s', '%s'", pdir, strerror(errno));
      free(pdir);

      return done;
   }

   tell(1, "Starting cleanup of epgdata zip's in '%s'", pdir);

   free(pdir);
 
   while ((dirent = readdir(dir)))
   {
      // check extension

      if (strncmp(dirent->d_name + strlen(dirent->d_name) - strlen(ext), ext, strlen(ext)) != 0)
         continue;

      // lookup file
   
      obj->fileDb->clear();
      obj->fileDb->setValue(cTableFileRefs::fiName, dirent->d_name);
      obj->fileDb->setValue(cTableFileRefs::fiSource, getSource());

      if (!obj->fileDb->find())
      {
         asprintf(&pdir, "%s/%s/%s", EPG2VDRConfig.cachePath, getSource(), dirent->d_name);

         if (!removeFile(pdir))
            count++;
         
         free(pdir);
      }

      obj->fileDb->reset();
   }

   closedir(dir);

   tell(1, "Cleanup finished, removed (%d) epgdata files", count);

   return success;
}

//***************************************************************************

extern "C" void* EPGPluginCreator() { return new Epgdata(); }
