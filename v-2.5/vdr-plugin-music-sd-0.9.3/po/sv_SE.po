# VDR music language source file.
# Copyright (C) 2008 Stefan Franz <djoimania@web.de>
# This file is distributed under the same license as the VDR package.
# Tomas Prybil <tomas@prybil.se>, 2002
# Jan Ekholm <chakie@infa.abo.fi>, 2003
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.5.7\n"
"Report-Msgid-Bugs-To: <djoimania@web.de>\n"
"POT-Creation-Date: 2013-02-19 02:16+0100\n"
"PO-Revision-Date: 2008-05-06 22:17+0200\n"
"Last-Translator: Tomas Prybil <tomas@prybil.se>\n"
"Language-Team: <vdr@linuxtv.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Music-SD: Update webstreams"
msgstr ""

msgid "Parent"
msgstr "Tillbaka"

msgid "Music-SD: Appearance"
msgstr ""

msgid "ERROR: Havent found any themes !"
msgstr ""

msgid "ERROR:"
msgstr ""

msgid "ERROR: Could not store/load new themefile !"
msgstr ""

msgid "Music-SD: Visualizations"
msgstr ""

msgid "Disable visualization"
msgstr ""

msgid "ERROR: Cant find any visualization!"
msgstr ""

msgid "ERROR: Could not load new visualization !"
msgstr ""

msgid "Music-SD: User"
msgstr ""

msgid "Change user (Music-SD will be stopped) ?"
msgstr ""

msgid "ERROR: Could not change user !"
msgstr ""

msgid "Remove track from playlist"
msgstr ""

msgid "Remove all from playlist"
msgstr ""

msgid "Save active playlist"
msgstr ""

msgid "Delete selected track from medium"
msgstr ""

msgid "Delete all tracks in playlist from medium"
msgstr ""

msgid "ERROR: Could not remove track !"
msgstr ""

msgid "Remove track from playlist ?"
msgstr ""

msgid "Are you sure?"
msgstr "�r du s�ker?"

msgid "Remove all tracks from playlist ?"
msgstr ""

msgid "Remove all tracks from playlist..."
msgstr ""

msgid "Save playlist..."
msgstr ""

msgid "Save playlist"
msgstr ""

msgid "Playlist saved as burnlist.m3u !"
msgstr ""

msgid "ERROR: Could not save playlist burnlist.m3u !"
msgstr ""

msgid "Overwrite last playlist ?"
msgstr ""

msgid "Delete file from medium ?"
msgstr ""

msgid "ERROR: Could not remove tracks !"
msgstr ""

msgid "Delete all in playlist from medium ?"
msgstr ""

msgid "Delete all tracks from medium..."
msgstr ""

msgid "Delete"
msgstr ""

msgid "Tracks terminated !"
msgstr ""

msgid "ERROR: Could not delete tracks !"
msgstr ""

msgid "Music-SD: Commands"
msgstr ""

msgid "Edit playlist..."
msgstr ""

msgid "Search for songs..."
msgstr ""

msgid "Show ID3 information of song"
msgstr ""

msgid "Edit ID3 information"
msgstr ""

msgid "Quick settings..."
msgstr ""

msgid "Search for lyric..."
msgstr ""

msgid "Download coverpicture..."
msgstr ""

msgid "Update webstreams..."
msgstr ""

msgid "Start imageviewer"
msgstr ""

msgid "Start Recording"
msgstr ""

msgid "Stop Recording"
msgstr ""

msgid "Change Appearance..."
msgstr ""

msgid "Visualizations..."
msgstr ""

msgid "Change user..."
msgstr ""

msgid "ERROR: No track(s) loaded !"
msgstr ""

msgid "ERROR: Could not get songinfo !"
msgstr ""

msgid "ERROR: no songinfo or not allowed !"
msgstr ""

msgid "Recording stopped !"
msgstr ""

msgid "Recording started !"
msgstr ""

msgid "What you want to record ?!"
msgstr ""

msgid "Stop recording ?"
msgstr ""

msgid "Start recording ?"
msgstr ""

msgid "Music-SD: Coverpicture"
msgstr ""

msgid "Reset query"
msgstr ""

msgid "Execute query"
msgstr ""

msgid "Artist"
msgstr "Artist"

msgid "Album"
msgstr "Album"

msgid "Search for cover"
msgstr ""

msgid "Searching"
msgstr ""

msgid "ERROR"
msgstr ""

msgid "ERROR: Could not open Coverviewer or not installed.. !"
msgstr ""

msgid "ERROR: While getting cover. Watch logs.. !"
msgstr ""

msgid "ERROR: Field 'Artist' empty.. !"
msgstr ""

msgid "Unknown"
msgstr ""

msgid "No comment"
msgstr ""

msgid "Music-SD: Local lyrics"
msgstr ""

msgid "Track-"
msgstr ""

msgid "Track+"
msgstr ""

msgid "No lyrics found !"
msgstr ""

msgid "Music-SD: Extern lyrics"
msgstr ""

msgid "Save lyrics ?"
msgstr ""

msgid "Save"
msgstr ""

msgid "Try to get lyrics..."
msgstr ""

msgid "Operation denied !"
msgstr ""

msgid "Music-SD: Lyrics"
msgstr ""

msgid "Load lyrics from local source"
msgstr ""

msgid "Load lyrics from external source"
msgstr ""

msgid "Music-SD: ID3 Information"
msgstr ""

msgid "Filename"
msgstr "Filnamn"

msgid "Length"
msgstr "L�ngd"

msgid "Title"
msgstr "Tittel"

msgid "Rating"
msgstr ""

msgid "Year"
msgstr "�r"

msgid "Genre"
msgstr ""

msgid "Samplerate"
msgstr "Samplingshastighet"

msgid "Bitrate"
msgstr "Bithastighet"

msgid "Channels"
msgstr ""

msgid "Comment"
msgstr ""

msgid "Directory browser"
msgstr "Katalogbl�ddrare"

msgid "Search"
msgstr ""

msgid "Play now"
msgstr ""

msgid "ID3 info"
msgstr "ID3 info"

msgid "Error building playlist!"
msgstr "Fel uppstod d� spellistan skapades!"

msgid "Item is not a file"
msgstr ""

msgid "Playlist editor"
msgstr "Spellisteditor"

msgid "Add"
msgstr "L�gg till"

msgid "Filenames"
msgstr "Filnamn"

msgid "ID3 names"
msgstr "ID3 namn"

msgid "Remove"
msgstr "Radera"

msgid "Scanning directory..."
msgstr "S�ker igenom katalog..."

msgid "Add recursivly?"
msgstr ""

msgid "Empty directory!"
msgstr "Tom katalog!"

msgid "Error scanning directory!"
msgstr "Kunde inte l�sa katalogen!"

msgid "Remove entry?"
msgstr "Radera post?"

msgid "Add all"
msgstr "L�gg till alla"

msgid "Rename playlist"
msgstr "D�p om spellistan"

msgid "Old name:"
msgstr "Gammalt namn:"

msgid "New name"
msgstr "Nytt namn"

#, fuzzy
msgid "Music-SD"
msgstr "MPlayer"

msgid "Source"
msgstr "K�lla"

msgid "Browse"
msgstr "Bl�ddra"

msgid "Convert"
msgstr ""

msgid "Rename"
msgstr "D�p om"

msgid "Scanning playlists..."
msgstr "S�ker spellistor..."

msgid "Error scanning playlists!"
msgstr "Fel uppstod d� spellistorna l�stes!"

msgid "Delete playlist?"
msgstr "Radera spellista?"

msgid "Error deleting playlist!"
msgstr "Fel uppstod d� spellistan raderades!"

msgid "unnamed"
msgstr "namnl�s"

msgid "Error creating playlist!"
msgstr "Fel uppstod d� spellistan skapades!"

msgid "Convert playlist?"
msgstr ""

msgid "Error renaming playlist!"
msgstr "Fel uppstod d� spellistan d�ptes om!"

msgid "Error loading playlist!"
msgstr "Fel uppstod d� spellistan l�stes in!"

msgid "Can't edit a WinAmp playlist!"
msgstr "Kan inte redigera en WinAmp-spellista!"

msgid "Loading playlist..."
msgstr "L�ser in spellista..."

msgid "MP3 source"
msgstr "MP3 k�lla"

msgid "Building playlist..."
msgstr "Skapar en spellista..."

msgid "Select"
msgstr "V�lj"

msgid "Mount"
msgstr "Montera"

msgid "Unmount"
msgstr "Avmontera"

msgid "Eject"
msgstr ""

msgid "Selected source is not mounted!"
msgstr "Den valda k�llan �r inte monterad!"

msgid "Mount failed!"
msgstr "Monteringen misslyckades!"

msgid "Mount succeeded"
msgstr "Monteringen lyckades"

msgid "Unmount succeeded"
msgstr "Avmonteringen lyckades"

msgid "Unmount failed!"
msgstr "Avmonteringen misslyckades!"

msgid "Eject failed!"
msgstr "Mata ut!"

msgid "Commands"
msgstr ""

msgid "More.."
msgstr ""

msgid "Cover"
msgstr ""

msgid "Jump"
msgstr ""

msgid "Clear"
msgstr ""

msgid "Copy"
msgstr ""

msgid "<<"
msgstr ""

msgid ">>"
msgstr ""

msgid "Min/Sec"
msgstr ""

msgid "of"
msgstr ""

msgid "Connecting to stream server ..."
msgstr "Kontaktar stream-server..."

msgid "Remote CDDB lookup..."
msgstr "Fj�rrf�rfr�gan till CDDB..."

msgid "Please add some tracks..."
msgstr ""

msgid "End of playlist ============================"
msgstr ""

msgid "SCANNING"
msgstr ""

msgid "STOPPED"
msgstr ""

msgid "PLAYING"
msgstr ""

msgid "PAUSED"
msgstr ""

msgid "Jump: "
msgstr ""

msgid "Be patient..."
msgstr ""

msgid "TRACKS become straight scanned"
msgstr ""

msgid "Playlist time"
msgstr ""

msgid "Playlist tracks"
msgstr ""

msgid "Not rated"
msgstr ""

msgid "Song can be deleted"
msgstr ""

msgid "Makes me nervous"
msgstr ""

msgid "Bad song"
msgstr ""

msgid "Useful for certain cause"
msgstr ""

msgid "I hear it from time to time"
msgstr ""

msgid "New song, don't know"
msgstr ""

msgid "Not bad...not good"
msgstr ""

msgid "Good song"
msgstr ""

msgid "Very good song"
msgstr ""

msgid "I love this song"
msgstr ""

msgid "One of my favourites"
msgstr ""

msgid "Music-SD: ID3tag editor"
msgstr ""

msgid "Clear tags"
msgstr ""

msgid "Save tags"
msgstr ""

msgid "Execute"
msgstr ""

msgid "Update id3-tag"
msgstr ""

msgid "MPlayer"
msgstr "MPlayer"

msgid "Setup.MPlayer$Control mode"
msgstr "Kontroll�ge"

msgid "Traditional"
msgstr "Traditionell"

msgid "Slave"
msgstr "Slav"

msgid "Setup.MPlayer$OSD position"
msgstr ""

msgid "disabled"
msgstr "avst�ngd"

msgid "global only"
msgstr ""

msgid "local first"
msgstr ""

msgid "Setup.MPlayer$Resume mode"
msgstr ""

msgid "Hide mainmenu entry"
msgstr ""

msgid "Setup.MPlayer$Slave command key"
msgstr ""

msgid "MPlayer Audio ID"
msgstr ""

msgid "Audiostream ID"
msgstr ""

msgid "MPlayer browser"
msgstr "MPlayer-bl�ddrare"

msgid "Play"
msgstr ""

msgid "Rewind"
msgstr ""

msgid "MPlayer source"
msgstr "MPlayer k�lla"

msgid "Summary"
msgstr ""

msgid "Media replay via MPlayer"
msgstr ""

#, fuzzy
msgid "MP3-/Audioplayer"
msgstr "MPlayer"

#, fuzzy
msgid "Musicplayer SD-FF"
msgstr "MPlayer"

msgid "DVB"
msgstr "DVB"

msgid "OSS"
msgstr "OSS"

msgid "MP3"
msgstr "MP3"

msgid "Setup.MP3$Artist-Title in tracklist"
msgstr ""

msgid "User"
msgstr ""

msgid "Admin"
msgstr ""

msgid "Superadmin"
msgstr ""

msgid "Setup.MP3$Userlevel"
msgstr ""

msgid "Setup.MP3$Show statusmessages"
msgstr ""

msgid "Setup.MP3$Jump interval (FFW/FREW)"
msgstr ""

msgid "Setup.MP3$Where to copy tracks"
msgstr ""

msgid "Setup.MP3$Where to record streams"
msgstr ""

msgid "Setup.MP3$Options for recording"
msgstr ""

msgid "Setup.MP3$Directory for cover of artists"
msgstr ""

msgid "Setup.MP3$OSD Offset X"
msgstr ""

msgid "Setup.MP3$OSD Offset Y"
msgstr ""

msgid "normal"
msgstr ""

msgid "double"
msgstr ""

msgid "4:3"
msgstr ""

msgid "fullscreen"
msgstr ""

msgid "Setup.MP3$Size of coverdisplay"
msgstr ""

msgid "Setup.MP3$Max. cache in MB for cover"
msgstr ""

msgid "Setup.MP3$Image quality >=slower"
msgstr ""

msgid "Setup.MP3$Use Dithering for cover"
msgstr ""

msgid "Setup.MP3$Max. download of cover"
msgstr ""

msgid "Setup.MP3$Enable visualization"
msgstr ""

msgid "Setup.MP3$Visualization bar falloff"
msgstr ""

msgid "Setup.MP3$Enable rating"
msgstr ""

msgid "Setup.MP3$Email for rating"
msgstr ""

msgid "Setup.MP3$Store rating in file"
msgstr ""

msgid "Setup.MP3$Rating as first red key"
msgstr ""

msgid "Setup.MP3$Audio output mode"
msgstr ""

msgid "Setup.MP3$Audio mode"
msgstr "Audiol�ge"

msgid "Round"
msgstr "Avrunda"

msgid "Dither"
msgstr ""

msgid "Setup.MP3$Use 48kHz mode only"
msgstr "Anv�nd endast 48Khz-l�ge"

msgid "Setup.MP3$Live picture in background"
msgstr ""

msgid "Setup.MP3$Use DeviceStillPicture"
msgstr ""

msgid "Setup.MP3$Abort player at end of list"
msgstr ""

msgid "ID3 only"
msgstr ""

msgid "ID3 & Level"
msgstr ""

msgid "Setup.MP3$Background scan"
msgstr ""

msgid "Setup.MP3$Editor display mode"
msgstr "Redigerarens visuella l�ge"

msgid "Setup.MP3$Mainmenu mode"
msgstr "Huvudmenyl�ge"

msgid "Playlists"
msgstr "Spellistor"

msgid "Browser"
msgstr "Bl�ddra"

msgid "Setup.MP3$Replace string in winamp playlist"
msgstr ""

msgid "Setup.MP3$Keep selection menu"
msgstr ""

msgid "Setup.MP3$Exit stop playback"
msgstr ""

msgid "Replay instantly"
msgstr ""

msgid "Setup.MP3$Normalizer level"
msgstr "Normaliseringsniv�"

msgid "Setup.MP3$Limiter level"
msgstr "Begr�nsningsniv�"

msgid "Setup.MP3$Connection timeout (s)"
msgstr ""

msgid "Setup.MP3$Use HTTP proxy"
msgstr "Anv�nd en HTTP-proxy"

msgid "Setup.MP3$HTTP proxy host"
msgstr "HTTP-proxyns namn"

msgid "Setup.MP3$HTTP proxy port"
msgstr "HTTP-proxyns port"

msgid "local only"
msgstr ""

msgid "local&remote"
msgstr ""

msgid "Setup.MP3$CDDB for CD-Audio"
msgstr "CDDB f�r CD-audio"

msgid "Setup.MP3$CDDB server"
msgstr "CDDB server"

msgid "Setup.MP3$CDDB port"
msgstr "CDDB port"

msgid "Music-SD: Timer Shutdown"
msgstr ""

msgid "Minutes before shutdown"
msgstr ""

msgid "Disable"
msgstr ""

msgid "Enable"
msgstr ""

msgid "Music-SD: Change time to skip"
msgstr ""

msgid "Seconds to skip"
msgstr ""

msgid "Music-SD: Add new target directory"
msgstr ""

msgid "Directory to add"
msgstr ""

msgid "Music-SD: Goal listing"
msgstr ""

msgid "Current goal listing: "
msgstr ""

msgid "Remove ?"
msgstr ""

msgid "New goal listing ?"
msgstr ""

msgid "Error:"
msgstr ""

msgid "ERROR: Could not remove entry !"
msgstr ""

msgid "Music-SD: Quick settings"
msgstr ""

msgid "Disable system shutdown after player stopped"
msgstr ""

msgid "Enable system shutdown after player stopped"
msgstr ""

msgid "Timer Shutdown"
msgstr ""

msgid "Activate Shufflemode"
msgstr ""

msgid "Deactivate Shufflemode"
msgstr ""

msgid "Activate Loopmode"
msgstr ""

msgid "Deactivate Loopmode"
msgstr ""

msgid "Change directory where to copy tracks"
msgstr ""

msgid "Change time to skip"
msgstr ""

msgid "Empty ID3 Cache"
msgstr ""

msgid "Disable automatic shutdown ?"
msgstr ""

msgid "Music-SD: Automatic shutdown"
msgstr ""

msgid "Automatic shutdown disabled !"
msgstr ""

msgid "Enable automatic shutdown ?"
msgstr ""

msgid "VDR will shutdown after player stopped !"
msgstr ""

msgid "Always start in shuffle mode ?"
msgstr ""

msgid "Disable shuffle mode only for this session ?"
msgstr ""

msgid "Always start in loop mode ?"
msgstr ""

msgid "Disable loop mode only for this session ?"
msgstr ""

msgid "Empty ID3 cache ?"
msgstr ""

msgid "Music-SD: Rating"
msgstr ""

msgid "Clear rating"
msgstr ""

msgid "Error while opening @Suchergebnis.m3u !"
msgstr ""

msgid "Sort"
msgstr ""

msgid "Add all tracks to playlist..."
msgstr ""

msgid "Rating to"
msgstr ""

msgid "No value"
msgstr ""

msgid "Rating from"
msgstr ""

msgid "Music-SD: Search"
msgstr ""

msgid "Load previous query"
msgstr ""

msgid "Year from"
msgstr ""

msgid "Year to"
msgstr ""

msgid "Only available songs"
msgstr ""

msgid "no"
msgstr ""

msgid "yes"
msgstr ""

msgid "Searching..."
msgstr ""

msgid "Found:"
msgstr ""

msgid "ERROR: Could not find anything !"
msgstr ""

msgid "Save tracklist as"
msgstr ""

msgid "ERROR: Could not save playlist"
msgstr ""

msgid "No title"
msgstr ""

msgid "Music-SD: Playlist"
msgstr ""

msgid "ID3info"
msgstr ""

msgid "Edit ID3"
msgstr ""

msgid "Mark"
msgstr ""

msgid "Empty"
msgstr ""

msgid "Save PL"
msgstr ""

msgid "Save BL"
msgstr ""

msgid "Number"
msgstr ""

msgid "Error while sorting"
msgstr ""

msgid "Sortmode:"
msgstr ""

msgid "Total:"
msgstr ""

msgid "Save playlist as burnlist ?"
msgstr ""

msgid "Save tracklist as playlist ?"
msgstr ""

msgid "Copy track to goal listing ?"
msgstr ""

msgid "Copy..."
msgstr ""

msgid "Empty playlist ?"
msgstr ""

msgid "Delete this track from playlist ?"
msgstr ""

msgid "Sorry... but first change to sortmode = number !"
msgstr ""

#, fuzzy
#~ msgid "Setup.MP3$Use HD theme"
#~ msgstr "Anv�nd endast 48Khz-l�ge"

#~ msgid "Setup.MP3$Initial loop mode"
#~ msgstr "Normalt upprepningsl�ge"

#~ msgid "Setup.MP3$Initial shuffle mode"
#~ msgstr "Normalt blandl�ge"

#~ msgid "Setup.MP3$Display mode"
#~ msgstr "Visuellt l�ge"
