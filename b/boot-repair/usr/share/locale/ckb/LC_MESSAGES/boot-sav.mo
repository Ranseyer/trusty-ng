��    !      $  /   ,      �     �  $   �          +  )   G     q     y  #   �     �     �     �     �     �  -        :     U  !   d     �     �     �     �  .   �     
     "     2  #   O  	   s  &   }     �     �  !   �     �  �  �  A   �  B   �       6   <  N   s     �  2   �  L   	  !   X	      z	  !   �	  >   �	  C   �	  V   @
  8   �
  0   �
  /     0   1  ;   b     �  #   �  O   �  0   .     _  J     L   �       W   (  )   �     �  Y   �                              
                                                                                  !                                         	           Advanced options An error occurred during the repair. Boot Repair Boot successfully repaired. Close this window when you have finished. Credits Do you want to continue? Do you want to update the software? GRUB options MBR options Other options Please backup your data now! Please try again. Please use this software in a 64bits session. Please wait few seconds... Reinstall GRUB Removing ${OS_TO_DELETE_NAME} ... Repair file systems Repair the boot of the computer Scanning systems Software Center The ${THISPARTITION} partition is nearly full. Then close this window. Then try again. This can prevent to start it This may require several minutes... Translate Ubuntu installed in Windows (via Wubi) Update Manager Updating You can now reboot your computer. seconds Project-Id-Version: boot-repair
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-12-08 01:03+0100
PO-Revision-Date: 2012-11-09 16:38+0000
Last-Translator: jwtear nariman <jwtiyar@gmail.com>
Language-Team: Kurdish (Sorani) <ckb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-12-23 14:41+0000
X-Generator: Launchpad (build 16877)
 هه‌ڵبژاردنه‌ په‌ره‌سه‌ندوه‌کان هەڵەیەک ڕویدا لە کاتی چارەسەرکردندا چاککردنەوەی بووت بووت بەسەرکەتووی چارەسەر کرا. پەنجەرەکە دابخەوە دووای ئەوەی تەواو بوویت. متمانه‌كان ئایا ئەتەوێت بەردەوام بیت ؟ ئایا ئەتەوێت نەرمەکاڵاکانت نوێبکەیتەوە ؟ هەڵبژاردنەکانی GRUB هەڵبژاردنەکانی MBR هەڵبژاردنی جیاواز تکایە ئێستا پاڵپشتی داتاکانت بکە ! تکایه‌ جارێکی تر هه‌وڵ بده‌ره‌وه‌ تکایە ئەم نەرمەکاڵایە بە سیستەمی 64بیت ئیشپێبکە تکایە چەند چرکەیەک چاوەڕێبکە... دووبارە دامەزراندنەوەی GRUB ${OS_TO_DELETE_NAME} دەسڕدرێتەوە ... چاککردنی پەڕگەکانی سیستەم بووتی کۆمپیوتەرەکەت چاک بکەرەوە پشکنینی سیستەم بنکەی نەرمەکاڵاکان ئەم پارچەیە ${THISPARTITION} بە نزیکەی وا پڕ دەبێت . دواتر ئەم پەنجەرەیە دابخە. دواتر هەوڵ بدەوە. ئەمە دەتوانرێ پشتگوێبخرێت بۆ دەستپێکردن لەوانەیە ئەمە پێویستی بە چەند خولەکێک بێت وەرگێڕان ئووبونتۆ لەسەر ویندۆز دەمەزرا (لەلایەن Wubi)ـیەوە بەڕێوەبەری نوێکردنەوە نوێئەبێتەوە ئێستا دەتوانی دووبارە کۆمپیوتەرەکەت پێبکەیتەوە. چرکه‌ 