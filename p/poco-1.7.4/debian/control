Source: poco
Priority: optional
Maintainer: Josh Blum <josh@pothosware.com>
Uploaders:Wolfgang Mangold <vdr@gmx.de>
Build-Depends: cmake, debhelper (>= 7.4.15), dpkg-dev (>= 1.15.5), libexpat1-dev, libmysqlclient-dev, libpcre3-dev (>= 7.8), libsqlite3-dev (>= 3.6.13), libssl-dev (>= 0.9.8), unixodbc-dev, zlib1g-dev
Standards-Version: 3.9.5
Section: libs
Homepage: http://pocoproject.org/
Vcs-Browser: https://github.com/pocoproject/poco/
Vcs-Git: https://github.com/pocoproject/poco.git

Package: libpoco-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libpococrypto44 (= ${binary:Version}), libpocodata44 (= ${binary:Version}), libpocofoundation44 (= ${binary:Version}), libpocodatamysql44 (= ${binary:Version}), libpoconet44 (= ${binary:Version}), libpoconetssl44 (= ${binary:Version}), libpocodataodbc44 (= ${binary:Version}), libpocodatasqlite44 (= ${binary:Version}), libpocomongodb44 (= ${binary:Version}), libpocoutil44 (= ${binary:Version}), libpocoxml44 (= ${binary:Version}), libpocojson44 (= ${binary:Version}), libpocozip44 (= ${binary:Version})
Provides: libpoco9-dev
Conflicts: libpoco9-dev, libpoco-dev-min
Replaces: libpoco9-dev
Description: C++ Portable Components (POCO) Development files
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 POCO consists of four core libraries, and a number of add-on libraries. The
 core libraries are Foundation, XML, Util and Net. Two of the add-on libraries
 are NetSSL, providing SSL support for the network classes in the Net library,
 and Data, a library for uniformly accessing different SQL databases.

Package: libpoco-dev-min
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libpocofoundation44 (= ${binary:Version}), libpoconet44 (= ${binary:Version}), libpocoutil44 (= ${binary:Version}), libpocoxml44 (= ${binary:Version}), libpocojson44 (= ${binary:Version}), libpocozip44 (= ${binary:Version})
Provides: libpoco9-dev
Conflicts: libpoco9-dev, libpoco-dev
Replaces: libpoco9-dev
Description: C++ Portable Components (POCO) Development files (minimal)
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 POCO consists of four core libraries, and a number of add-on libraries. The
 core libraries are Foundation, XML, Util and Net. Two of the add-on libraries
 are NetSSL, providing SSL support for the network classes in the Net library,
 and Data, a library for uniformly accessing different SQL databases.

Package: libpococrypto44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Crypto library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Crypto library.

Package: libpocodata44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Data library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Data library.

Package: libpocodatamysql44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), libpocodata44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Data MySQL library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Data MySQL library.

Package: libpocodataodbc44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), libpocodata44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) ODBC library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Data ODBC library. In Debian, it is linked
 against unixODBC but also iODBC can be used instead.

Package: libpocodatasqlite44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), libpocodata44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Data SQLite library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Data SQLite library.

Package: libpocomongodb44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), libpoconet44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Mongo DB library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Mongo DB library.

Package: libpocofoundation44
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Foundation library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Foundation library.

Package: libpoconet44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Network library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Net library.

Package: libpoconetssl44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), libpococrypto44 (= ${binary:Version}), libpoconet44 (= ${binary:Version}), libpocoutil44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Network library with SSL
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Network SSL library.

Package: libpocoutil44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), libpocoxml44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Util library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides POCO Util library.

Package: libpocoxml44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) XML library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO XML library.

Package: libpocozip44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Zip library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Zip library.

Package: libpocojson44
Architecture: any
Depends: libpocofoundation44 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) JSON library
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO JSON library.

Package: poco-pagecompiler
Architecture: any
Depends:
    libpocofoundation44 (= ${binary:Version}),
    libpoconet44 (= ${binary:Version}),
    libpocoxml44 (= ${binary:Version}),
    libpocojson44 (= ${binary:Version}),
    libpocoutil44 (= ${binary:Version}),
    ${shlibs:Depends}, ${misc:Depends}
Description: C++ Portable Components (POCO) Page compiler
 The POCO C++ Libraries are a collection of open source C++ class libraries
 that simplify and accelerate the development of network-centric, portable
 applications in C++. The libraries integrate perfectly with the C++ Standard
 Library and fill many of the functional gaps left open by it.
 .
 POCO is built strictly using standard ANSI/ISO C++, including the standard
 library. The contributors attempt to find a good balance between using advanced
 C++ features and keeping the classes comprehensible and the code clean,
 consistent and easy to maintain.
 .
 This package provides the POCO Page compiler utilities.
